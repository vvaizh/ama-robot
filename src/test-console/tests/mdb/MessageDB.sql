Create Table EfrsbMessage
(
	INN            VARCHAR(20),
    SNILS          VARCHAR(20),
    OGRN           VARCHAR(20),
	PublishDate 			DateTime,
	Revision 			Double,
	Type 				SmallInt,
	XmlText 			LONGTEXT
);