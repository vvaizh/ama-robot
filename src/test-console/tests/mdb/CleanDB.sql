DELETE * FROM Task;
DELETE * FROM MailIn;
DELETE * FROM MailOut;
DELETE * FROM EFRSBMessage;
DELETE * FROM ManagerProcedure;
DELETE * FROM CreditorRegistry;
DELETE * FROM CreditorClaim;
DELETE * FROM Assembly;