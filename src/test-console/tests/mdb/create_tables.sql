--Create EFRSB table
CREATE TABLE EfirMessages(
    message_id                  AutoIncrement       NOT NULL, 
    debtor_id                   Guid                NOT NULL,
    revision                    INT                 NOT NULL,
    body                        MEMO                NOT NULL,
    publish_date                DATETIME            NOT NULL,
    message_type                Text(50)            NOT NULL,  
    message_number              Text(50)            NOT NULL,
    CONSTRAINT EfirMessage_PK PRIMARY KEY (message_id)
);
-- Create Task teable 
CREATE TABLE Task (
	manproc_id		 								Guid,
	id_task												Guid,
	json_task			 								String(250) NOT NULL, 
	TaskResult											INT,
	DateResolve										Date
);
CREATE TABLE [Appointments] (
    [appointment_id]            Guid                NOT NULL,
    [author_id]                 Guid,
    [debtor_id]                 Guid,
    [manproc_id]                Guid,
    [appointment_date]          DateTime,
    [short_description]         Memo,
    [description]               Memo,
    [law_text]                  Memo,
    [notes]                     Memo,
    [done]                      BIT NOT NULL,
    [type]                      INTEGER,
    [ref_id]                    Guid,
    [debtor_name]               Memo,
    [priority]                  INTEGER,
    [tag]                       INTEGER,
    [google_event_id]		Text(255),
    [send_to_google_calendar]	Bit,
    [appointment_end_date]	DateTime,
    [updated_date]		DateTime,
    [is_unread]			Bit,	
    [is_manager_task]		Bit,							
    CONSTRAINT Appointments_PK PRIMARY KEY (appointment_id)
);


ALTER TABLE EfirMessage
    ADD CONSTRAINT EfirMessage_FK_ManagerProcedure 
        FOREIGN KEY (debtor_id) REFERENCES Deptor(deptor_id) ON UPDATE CASCADE ON DELETE CASCADE;

CREATE INDEX ManagerTasks_manproc_id            ON Task           ([manproc_id]);
CREATE INDEX EfirMessage_manproc_id_Revision            ON EfirMessage           ([debtor_id],[revision]);