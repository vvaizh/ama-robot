﻿using AMA.Robot.Properties;
using System;
using System.IO;
using System.Text;
namespace AMA.Robot
{
    class Program
    {
        private class Options
        {
            public bool EchoInput           { get; set; }
            public bool UseTestTime         { get; set; }
            public bool UseTestTimeFile     { get; set; }
            public bool ReadKey             { get; set; }
            public string InputFile         { get; set; }
            public string Encoding          { get; set; }
            public string Database          { get; set; }
            public string Datadir           { get; set; }
        }

        private static cmd.Executor executor;
        private static Options options;

        static void Main(string[] args)
        {
            if (!ParseCommandLineArguments(args, out options))
                return;

            if (!string.IsNullOrEmpty(options.Encoding))
                Console.OutputEncoding = Encoding.GetEncoding(options.Encoding);

            try
            {
                task.Постановщик factory = new task.Постановщик();
                ITimeProvider time = options.UseTestTime ? (ITimeProvider)new TestTimeProvider()
                    : options.UseTestTimeFile ? (ITimeProvider)new TestFileTimeProvider(Settings.Default.FileTime)
                    : (ITimeProvider)new ActualTimeProvider();
                IGuidProvider guids = options.UseTestTime ? (IGuidProvider)new TestGuidProvider() : (IGuidProvider)new ActualGuidProvider();
                
                using (IДела_в_работе дело = (options.Database != null) ? (IДела_в_работе)new mdb.Дело(guids, options.Database, time) : (IДела_в_работе)new mock.Дело(guids, time))
                {
                    task.Планировщик постановщик = new task.Планировщик(factory, дело, дело, time);
                    Денежные_срества.IРасчёты operations = new Расчет();
                    Материалы материалы = new Материалы { дела = дело, регистратор = дело, time = time,
                        постановщик = постановщик, guids = guids, operations = operations};
                    executor = new cmd.Executor(материалы, options.Datadir);
                    if (options.InputFile == null || options.InputFile.Trim().Length == 0)
                        ExecuteCommandsFromStandardInput();
                    else
                        ExecuteCommandsFromFile(options.InputFile);
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
                if (options.ReadKey)
                    Console.ReadKey();
            }
        }

        static void prompt()
        {
            executor.ExecuteCommand("time", Console.Out);
            Console.Write("> ");
        }

        private static void ExecuteCommandsFromReader(TextReader reader)
        {
            prompt();
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (!(line == null || line.Trim().Length == 0))
                {
                    if (options.EchoInput)
                        Console.WriteLine("Введено \"{0}\"", line.Trim());
                    executor.ExecuteCommand(line, Console.Out);
                    Console.WriteLine();
                    
                    prompt();
                }
            }
        }

        private static void ExecuteCommandsFromStandardInput()
        {
            ExecuteCommandsFromReader(Console.In);
        }

        private static void ExecuteCommandsFromFile(string path)
        {
            using (StreamReader file = new StreamReader(path))
            {
                Directory.SetCurrentDirectory(Path.GetDirectoryName(path));
                ExecuteCommandsFromReader(file);
            }
        }

        /// <summary>
        /// Parse command line arguments into 'options'
        /// </summary>
        /// <param name="args">Command line arguments</param>
        /// <param name="op">Object to put parsed arguments into</param>
        /// <returns>Returns true if args are parsed successfully</returns>
        private static bool ParseCommandLineArguments(string[] args, out Options op)
        {
            op = new Options();
            foreach(string arg in args)
            {
                string pr;
                if      (arg.StartsWith(pr = "--in=")) { op.InputFile = arg.Substring(pr.Length); }
                else if (arg.StartsWith(pr = "--cp=")) { op.Encoding = arg.Substring(pr.Length); }
                else if (arg.StartsWith(pr = "--db=")) { op.Database = arg.Substring(pr.Length); }
                else if (arg.StartsWith(pr = "--datadir=")) { op.Datadir = arg.Substring(pr.Length); }
                else
                {
                    switch (arg)
                    {
                        case "-h":
                        case "--help": ShowHelp(); return false;
                        case "-t":  op.UseTestTime      = true;  break;
                        case "-e":  op.EchoInput        = true;  break;
                        case "-tf": op.UseTestTimeFile  = true;  break;
                        case "-rk": op.ReadKey          = true;  break;
                        default:
                            Console.Error.WriteLine("Error: Option \"{0}\" is not recognized.", arg);
                            ShowHelp();
                            return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Print application command line arguments to the output
        /// </summary>
        private static void ShowHelp()
        {
            Console.WriteLine(
@"Usage: TasksApp [Options]
Options:
  -h, --help           Show this message
  -t,                   Use test time
  -e,                   Echo entered commands
  -rk,                  Read key
  --in=input_file       Execute commands from file input_file
  --cp=codepage         Set output codepage
  --db=/path/to/ama.mdb Set path to AMA database
  --datadir=path        Set path to json files
"
            );
        }
    }
}
