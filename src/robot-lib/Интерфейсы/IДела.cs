﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AMA.Robot
{
    public interface IДела : IDisposable
    {
        IЗадача Задача_решена                               (IЗадача задача);
        List<Процедура> ВсеПроцедуры                        ();
        List<IЗадача> ВсеРешенныеЗадачи                     (string procedureID);
        List<IЗадача> ВсеПропущенныеЗадачи                  (string procedureID);
        Процедура Процедура                                 (string manproc_id);
        Исходящее Исходящее                                 (string mail_id);
        IEnumerable<Исходящее> ВсеИсходящие                 (string procedureID, string type = null);
        Ответ ОтветНа                                       (string requestID);
        IEnumerable<Ответ> ВсеВходящие                      (string procedureID, string type = null);
        IEnumerable<Публикация> ВсеПубликации               (string procedureID);
        Кредитор Кредитор                                   (string registry_id);
        Требование Требование                               (string claim_id);
        IEnumerable<Требование> ВсеТребования               (string procedureID, bool? included = null);
        IEnumerable<Собрание> ВсеСобрания                   (string procedureID);
        Собрание ПоследнееСобрание                          (string procedureID);
        IEnumerable<Исходящее> ВсеУведомления               (string procedure, string type = null);
        IEnumerable<Счет> ВсеСчета                          (string procedureID);
        IEnumerable<Торги> ВсеТорги                         (string procedureID);
        IEnumerable<Анализ> ВсеАнализы                      (string procedureID);
        IEnumerable<Инвентаризация> ВсеИнвентаризации       (string procedureID);
        int ПолучитьПоследнююРевизииюЕфрсбСообщения         (string debtor_id);
        IEnumerable<Efrsb_message_record> ВсеЕфрсбСообщения (string debtorId);
        Efrsb_message_record ПолучитьЕфрсбСообщение         (string debtorId, string type);
        Контрагент КонтрагентПоИНН                          (string inn);
        Контрагент КонтрагентByContactId                    (string contact_id);
        IEnumerable<Денежные_срества.Учётная_группа> ВсеУчетныеГруппы               (string procedureID);
        IEnumerable<Денежные_срества.Движение>       ДвиженияДенежныхСредств        (string procedureID);
    }

    public interface IРегистратор_изменений
    {
        void ЗарегистрироватьКонтрагента                    (Контрагент контрагент);
        void ОбновитьТребования                             (string claim_id, decimal sum);
        void ОбновитьТребования                             (string claim_id, DateTime date);
        void ПропуститьЗадачу                               (IЗадача задача);
        string ЗарегистрироватьОценкуСчетов                 (Инвентаризация inv);
        void IncludeClaim                                   (string id);
        string ЗарегистрироватьПроцедуру                    (Процедура proc);
        string ЗарегистрироватьИсходящее                    (Исходящее msg);
        string ЗарегистрироватьВходящее                     (Ответ msg);
        void ДобавитьНовоеОбъявлениеЕФРСБ                   (string debtorId, Efrsb_message_record msg);
        string ЗарегистрироватьПубликацию                   (Публикация pub);
        string ЗарегистрироватьКредитора                    (Кредитор creditor);
        string ЗарегистрироватьТребованиеКредитора          (Требование claim);
        string ЗарегистрироватьСобрание                     (Собрание meeting);
        string ЗарегистрироватьСчёт                         (Счет account);
        string ЗарегистрироватьФинанализ                    (Анализ analysis);
        string ЗарегистрироватьИнвентаризацию               (Инвентаризация inv);
        string ЗарегистрироватьТорги                        (Торги sale);
        void ЗакрытьТорги                                   (Торги sale);
        void ОбновитьСчёт                                   (Счет account, decimal sum);
        void Зарегистрировать_решение                       (IЗадача задача, string TaskResult);
        void Восстановить_задачу                            ();
        void ПровестиСобрание                               (Собрание собрание);

        void ЗарегистрироватьДенежнуюОперацию               (string procedureID, Денежные_срества.Движение движение, int type=0);
        void ЗарегистрироватьНачальноеСальдо                (string procedureID, Денежные_срества.Учётная_группа группа);
    }

    public interface IДела_в_работе : IДела, IРегистратор_изменений, IDisposable
    {

    }

    public class Материалы
    {
        public IДела дела;
        public IРегистратор_изменений регистратор;
        public task.IПланировщик постановщик;
        public ITimeProvider time;
        public IGuidProvider guids;
        public Денежные_срества.IРасчёты operations;
    }

    public static class Дела
    {
        static public IEnumerable<Исходящее> ВсеИсходящие(this IДела дела, Процедура процедура, Исходящее.Шаблон шаблон_исходящего)
        {
            return дела.ВсеИсходящие(процедура.ID, шаблон_исходящего.Тип);
        }

        static public IEnumerable<Ответ> ВсеВходящие(this IДела дела, Процедура процедура, Исходящее.Шаблон шаблон_исходящего)
        {
            return дела.ВсеВходящие(процедура.ID, шаблон_исходящего.Тип);
        }
    }
}
