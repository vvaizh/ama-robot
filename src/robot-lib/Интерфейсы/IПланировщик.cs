﻿using System;
using System.Collections.Generic;

namespace AMA.Robot.task
{
    public interface IПланировщик
    {
        List<IЗадача> Запланировать_фронт_задач(Процедура proc);
        string Build_timeline(string procedureId);
    }
}
