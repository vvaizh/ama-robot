﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMA.Robot
{
    public interface IРасчёты
    {
        decimal ТекущееСальдо(IДела дела, string procedureID,
            Денежные_срества.Учётная_группа учетнаяГруппа);
    }
}
