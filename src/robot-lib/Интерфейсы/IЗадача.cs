﻿using System;

namespace AMA.Robot
{
    public interface IEfrsb_message_record
    {
        int MessageId           { get; set; }
        string DebtorId         { get; set; }
        int Revision            { get; set; }
        string Body             { get; set; }
        DateTime PublishDate    { get; set; }
        string MessageType      { get; set; }
        string MessageNumber    { get; set; }
    }

    public interface IЗадача : IПостановка_задачи
    {
        string      ЗадачаID        { get; set; }
        string      ПроцедураID     { get; set; }
        DateTime    К_сроку         { get; set; }
        string      Формулировка    { get;}
        ТипЗадачи   Тип             { get; set; }
    }

    public class ТипЗадачи
    {
        public string Основной          { get; set; }
        public string Второстепенный    { get; set; }
    }

    public interface IПостановка_задачи
    {
        task.Статус Статус          { get; set; }
        DateTime    ДатаРешения     { get; set; }
    }

    public interface IДля_процедуры
    {
        Процедура процедура { get; set; }
    }

    public interface IДля_собрания
    {
        Собрание собрание { get; set; }
    }

    public interface IДля_инвентаризации 
    {
        Инвентаризация инвентаризация { get; set; }
    }

    public interface IДля_торгов
    {
        Торги торги { get; set; }
    }

    public interface IПо_шаблону_исходящего
    {
        Исходящее.Шаблон шаблон_исходящего { get; set; }
    }

    public interface IДля_банка
    {
        string Наименование_банка { get; set; }
    }

    public interface IДля_кредитора
    {
        Кредитор кредитор { get; set; }
    }

    public interface IДля_требования
    {
        Требование требование { get; set; }
    }

    namespace task
    {
        public enum Статус
        {
            Открыта,
            Завершена,
            Проигнорирована,
        }

        public interface IПостановщик
        {
            IЗадача сформулировать(Type тип_задачи, DateTime к_сроку, Action<IЗадача> настроить_дополнительно, Процедура процедура);
            IЗадача сформулировать(Type тип_задачи, DateTime к_сроку, Процедура процедура);
        }
    }
}
