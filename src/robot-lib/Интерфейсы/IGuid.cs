﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMA.Robot
{
    public interface IGuidProvider 
    {
        Guid New { get; }
        string beauty(string g);
        string unbeauty(string g);
    }

    public class ActualGuidProvider : IGuidProvider
    {
        public Guid New { get { return Guid.NewGuid(); } }
        public string beauty(string g) { return g;  }
        public string unbeauty(string g) { return g; }
    }

}
