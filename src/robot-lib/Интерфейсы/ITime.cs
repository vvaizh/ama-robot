﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMA.Robot
{
    public interface ITimeProvider
    {
        DateTime Now { get; }
    }

    public class ActualTimeProvider : ITimeProvider
    {
        public DateTime Now { get { return DateTime.Now; } }
    }
}
