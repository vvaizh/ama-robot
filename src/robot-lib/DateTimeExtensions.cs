﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMA.Robot
{
    public static class DateTimeExtensions
    {
        public static DateTime AddWorkDays(this DateTime date, int days)
        {
            DateTime newDate = date;
            while (days != 0)
            {
                newDate = newDate.AddDays(1);
                if (newDate.DayOfWeek != DayOfWeek.Saturday
                    && newDate.DayOfWeek != DayOfWeek.Sunday
                    && !newDate.IsHoliday())
                    days -= 1;
            }
            return newDate;
        }

        public class Через_число
        {
            public int число;
            public DateTime дней_после(DateTime dt)
            {
                return dt.AddDays(число);
            }
            public DateTime рабочих_дней_после(DateTime dt)
            {
                return dt.AddWorkDays(число);
            }
            public DateTime рабочих_дня_после(DateTime dt)
            {
                return рабочих_дней_после(dt);
            }
            public DateTime месяцев_после(DateTime dt)
            {
                return dt.AddMonths(число);
            }
            public DateTime месяца_после(DateTime dt)
            {
                return месяцев_после(dt);
            }
            public DateTime месяц_после(DateTime dt)
            {
                return месяцев_после(dt);
            }
        }

        public class За_число
        {
            public int число;
            public DateTime дней_до(DateTime dt)
            {
                return dt.AddDays(-число);
            }
            public DateTime рабочих_дней_до(DateTime dt)
            {
                return dt.AddWorkDays(-число);
            }
            public DateTime месяца_до(DateTime dt)
            {
                return dt.AddMonths(-число);
            }
        }

        public static За_число За(int адней)
        {
            return new За_число { число = адней };
        }

        public static Через_число Через(int адней)
        {
            return new Через_число { число= адней };
        }

        public static bool IsHoliday(this DateTime date)
        {
            return false;
        }
    }
}
