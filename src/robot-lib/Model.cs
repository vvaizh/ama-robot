﻿using System;
using System.Collections.Generic;

namespace AMA.Robot
{
    public interface IИдентифицируемый_по_ID
    {
        string ID { get; set; }
    }

    public class Идентифицируемый_по_ID : IИдентифицируемый_по_ID
    {
        public string ID { get; set; }
    }

    public class Контрагент
    {
        public string Наименование { get; set; }
        public string ИНН { get; set; }
        public string Id { get; set; }
        public string TypeId { get; set; }
    }

    public class Процедура : Идентифицируемый_по_ID
    {
        public enum ТипПроцедуры
        {
            Н,
            КП,
            РД,
            РИ
        }

        public DateTime Дата_начала { get; set; }
        public DateTime Дата_окончания { get; set; }

        public Контрагент Должник = new Контрагент();

        public ТипПроцедуры типПроцедуры { get; set; }

        public Тип тип = new Тип();
        
        public string Чего_процедуры()
        {
            switch (типПроцедуры)
            {
                case ТипПроцедуры.Н:
                    тип.Кратко = "Н";
                    тип.Полностью = "Наблюдение";
                    return "Наблюдения";
                case ТипПроцедуры.КП: 
                    тип.Кратко = "КП";
                    тип.Полностью = "Конкурсного производства";
                    return "Конкурсного производства";
                case ТипПроцедуры.РД: 
                    тип.Кратко = "РД";
                    тип.Полностью = "Реструктуризации долгов";
                    return "Реструктуризации долгов";
                case ТипПроцедуры.РИ: 
                    тип.Кратко = "РИ";
                    тип.Полностью = "Реализации имущества";
                    return "Реализации имущества";
            }
            throw new IndexOutOfRangeException();
        }
    }

    public class Тип
    {
        public string Кратко { get; set; }
        public string Полностью { get; set; }
    }


    public class В_рамках_процедуры : Идентифицируемый_по_ID
    {
        public string ПроцедураID { get; set; }
    }

    public partial class Исходящее : В_рамках_процедуры
    {
        public string Тип { get; set; }

        public string Получатель { get; set; }
        public DateTime ДатаОтправки { get; set; }
    }

    public class Ответ : В_рамках_процедуры
    {
        public string Тип { get; set; }
        public string ЗапросID { get; set; }

        public string Отправитель { get; set; }
        public DateTime ДатаПолучения { get; set; }
    }

    public class Публикация : В_рамках_процедуры
    {
        public string СМИ { get; set; }
        public DateTime Дата { get; set; }
    }

    public class Банковский_счёт
    {
        public string Банк;
        public string Счёт;
        public string ИНН;
    }

    public class Счет : В_рамках_процедуры
    {
        public string  Банк { get; set; }
        public string  Счёт { get; set; }
        public decimal Остаток { get; set; }
        public ТипБанковскогоСчета тип { get; set; }
    }

    public enum ТипБанковскогоСчета
    {
        основной,
        обычный
    }

    public class Кредитор : Идентифицируемый_по_ID
    {
        public string Наименование { get; set; }
        public string ИНН { get; set; }
        public string процедураId { get; set; }
    }

    public class Требование : В_рамках_процедуры
    {
        public string КредиторID { get; set; }
        public DateTime ДатаПолучения { get; set; }
        public decimal Сумма { get; set; }

        public bool Включено { get; set; }
        public DateTime ДатаВключения { get; set; }
    }

    public class Собрание : В_рамках_процедуры
    {
        public DateTime Дата { get; set; }
        public DateTime ДатаНазначения { get; set; }
        public List<string> Участники { get; set; }
        public List<Вопрос> Вопросы { get; set; }
        public bool Проведено { get; set; }
    }

    public class Вопрос
    {
        public string Формулировка { get; set; }
        public List<Голос> Голоса { get; set; }
    }

    public class Голос
    {
        public string КредиторИНН { get; set; }
        public Решение Решение { get; set; } // TODO: there can be other types of votes
    }

    public enum Решение
    {
        Недействительно,
        Воздержался,
        За,
        Против,
    }
    public class TasksDB : IЗадача
    {
        public string       ЗадачаID        { get; set; }
        public string       Формулировка    { get; set; }
        public DateTime     ДатаРешения     { get; set; }
        public DateTime     К_сроку         { get; set; }
        public string       ПроцедураID     { get; set; }
        public task.Статус  Статус          { get; set; }
        public ТипЗадачи    Тип             { get; set; }
    }

    

    public class Анализ : В_рамках_процедуры
    {
        public string Результат { get; set; }
        public DateTime Дата { get; set; }
    }

    public class Инвентаризация : В_рамках_процедуры
    {
        public DateTime Дата { get; set; }
        public DateTime ДатаОценки { get; set; }
    }

    public class Торги : В_рамках_процедуры
    {
        public DateTime ДатаОткрытия { get; set; }
        public bool Завершен { get; set; }
        public DateTime ДатаЗавершения { get; set; }
    }



    public class Efrsb_message_record: IEfrsb_message_record
    {
        public int MessageId { get; set; }
        /// <summary>
        /// Идентификатор должника
        /// </summary>
        public string DebtorId { get; set; }
        /// <summary>
        /// номер ревизии (необходима для получения сообщений с сервера)
        /// </summary>
        public int Revision { get; set; }
        /// <summary>
        /// xml ответ от ЕФРСБ при отправке запроса на получении информации по объявлению
        /// </summary>
        public string Body { get; set; }
        /// <summary>
        /// дата публикации
        /// </summary>
        public DateTime PublishDate { get; set; }
        /// <summary>
        /// Тип сообщения (получаем при парсинге xml: MessageInfo.MessageType)
        /// </summary>
        public string MessageType { get; set; }
        /// <summary>
        /// Номер сообщения на сайте ЕФРСБ. (string - т.к. в документации API Efrsb четко не указан тип переменной номера сообщений)
        /// </summary>
        public string MessageNumber { get; set; }
    }
}
