﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMA.Robot
{
    public class Response
    {
        public string id { get; set; }
        public string number { get; set; }
        public string CaseNumber { get; set; }
        public PublisherInfo publisherInfo { get; set; }
    }

    public class PublisherInfo
    {
        public ArbitrManager arbitrManager { get; set; }
    }

    public class ArbitrManager
    {
        public string id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string INN { get; set; }
        public string SNILS { get; set; }
        public string RegistryNumber { get; set; }
    }
}
