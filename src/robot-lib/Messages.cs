﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AMA.Robot
{
    public partial class Исходящее
    {
        public bool По(Шаблон шаблон) { return Тип == шаблон.Тип; }
        public Шаблон По_шаблону() { return Шаблон.Типа(Тип); }

        public class Шаблон
        {
            public bool Является_запросом { get; private set; }
            public string Наименование { get; private set; }
            public string Тип { get { Упорядочить();  return _Тип; } }

            public class Неизвестный_тип_шаблона : Exception
            {
                public Неизвестный_тип_шаблона(string тип)
                    : base(string.Format("Неизвестный типПроцедуры шаблона входящего \"{0}\"",тип))
                { }
            }

            public static Шаблон Типа(string тип)
            {
                Упорядочить();
                Шаблон res= null;
                if (!Шаблоны.TryGetValue(тип,out res))
                    throw new Неизвестный_тип_шаблона(тип);
                return res;
            }

            public static Шаблон В_ЕГРЮЛ=           Запрос("Заявление в ЕГРЮЛ");
            public static Шаблон В_ЕГРН=            Запрос("Запрос в ЕГРН");
            public static Шаблон В_ЗАГС=            Запрос("Запрос в ЗАГС");
            public static Шаблон В_Роспатент=       Запрос("запрос в Росреестр");
            public static Шаблон Руководителю=      Запрос("Запрос сведений у руководителя");
            public static Шаблон Должнику=          Запрос("Запрос сведений у должника");
            public static Шаблон В_ГИБДД=           Запрос("Уведомление-запрос в ГИБДД");
            public static Шаблон В_ГИМС=            Запрос("Уведомление-запрос в ГИМС");
            public static Шаблон В_Гостехнадзор=    Запрос("Уведомление-запрос в Гостехнадзор");
            public static Шаблон В_ИФНС=            Запрос("Уведомление-запрос в ИФНС");
            public static Шаблон В_ПФ=              Запрос("Уведомление-запрос в ПФ");
            public static Шаблон В_Росимущество=    Запрос("Уведомление-запрос в Росимущество");
            public static Шаблон В_ССП=             Запрос("Уведомление-запрос в ССП");
            public static Шаблон В_ФСС=             Запрос("Уведомление-запрос в ФСС");

            public static Шаблон В_банк=            Запрос("Запрос в банк");
            public static Шаблон В_БТИ=             Запрос("Запрос в БТИ");
            public static Шаблон В_ц_кат_кред_ист=  Запрос("Запрос в Центральный каталог кредитных историй");
            public static Шаблон В_бюро_кред_ист=   Запрос("Запрос в Бюро кредитных историй");

            public static Шаблон О_введении=        Письмо("Уведомление о введении процедуры");
            public static Шаблон В_Росреестр=       Письмо("Уведомление в Росреестр");
            public static Шаблон В_Коммерсант=      Письмо("Заявка-договор в Коммерсант");
            public static Шаблон Возражение_на_ТК = Письмо("Возражение на требование кредитора");
            public static Шаблон План_РД=           Письмо("План реструктуризации долгов");
            public static Шаблон План_РИ=           Письмо("Положение о порядке, условиях и сроках реализации имущества");
            public static Шаблон Уведомление_о_СК=  Письмо("Уведомление о СК");
            public static Шаблон Отчёт_в_АС=        Письмо("Отчёт в АС");
            public static Шаблон Отчёт_в_СРО=       Письмо("Отчёт в СРО");
            public static Шаблон Отчёт_об_оценке=   Письмо("Отчёт об оценке имущества");
            public static Шаблон О_ликвидации=      Письмо("Запись о ликвидации должника в ЕГРЮЛ");



            Шаблон(string Наименование, bool Является_запросом)
            {
                this.Наименование = Наименование;
                this.Является_запросом = Является_запросом;
            }

            string _Тип;
            static Dictionary<string, Шаблон> Шаблоны= null;
            static void Упорядочить()
            {
                if (null==Шаблоны)
                {
                    Dictionary<string, Шаблон> шаблоны= new Dictionary<string, Шаблон>();
                    Type type = MethodBase.GetCurrentMethod().DeclaringType;
                    foreach (FieldInfo fi in type.GetFields(BindingFlags.Static|BindingFlags.Public|BindingFlags.GetField))
                    {
                        if (type==fi.DeclaringType)
                        {
                            Шаблон шаблон= (Шаблон)fi.GetValue(null);
                            шаблон._Тип = fi.Name;
                            шаблоны.Add(fi.Name, шаблон);
                        }
                    }
                    Шаблоны = шаблоны;
                }
            }

            static Шаблон Запрос(string n)
            {
                return new Шаблон(Наименование: n, Является_запросом: true);
            }

            static Шаблон Письмо(string n)
            {
                return new Шаблон(Наименование: n, Является_запросом: false);
            }
        }
    }
}
