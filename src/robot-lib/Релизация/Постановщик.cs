﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMA.Robot.task
{
    public class Постановщик : IПостановщик
    {
        public IЗадача сформулировать(Type type, DateTime date, Процедура proc)
        {
            try
            {
                System.Reflection.ConstructorInfo constructor = type.GetConstructor(new Type[] { });
                IЗадача задача = (IЗадача)constructor.Invoke(new object[] { });
                задача.ЗадачаID = Guid.NewGuid().ToString();
                задача.ПроцедураID = proc.ID;
                задача.К_сроку = date;
                задача.Тип = new ТипЗадачи();
                задача.Тип.Второстепенный = type.Name;
                задача.Тип.Основной = type.FullName.Split('.')[3];
                IДля_процедуры для_процедуры = задача as IДля_процедуры;
                if (null != для_процедуры)
                    для_процедуры.процедура = proc;
                return задача;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Не могу создать задачу \"{0}\"",type.FullName),ex);
            }
        }

        public IЗадача сформулировать(Type type, DateTime date, Action<IЗадача> настроить_дополнительно, Процедура proc)
        {
            IЗадача задача = сформулировать(type, date, proc);
            настроить_дополнительно(задача);
            return задача;
        }
    }
}