﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMA.Robot
{
    namespace Денежные_срества
    {
        public class Адрес
        {
            public Банковский_счёт банковский_счёт;

            public ТипУчетнойГруппы учётная_группа;
            public Контрагент контрагент;

            public string TypeToString(ТипУчетнойГруппы тип = new ТипУчетнойГруппы())
            {
                ТипУчетнойГруппы _тип = тип == new ТипУчетнойГруппы() ? учётная_группа : тип;
                switch (_тип)
                {
                    case ТипУчетнойГруппы.средстваДолжника:
                        return "Средства должника";
                    case ТипУчетнойГруппы.средстваАУ:
                        return "Средства АУ";
                    case ТипУчетнойГруппы.залог:
                        return "Залоги";
                    case ТипУчетнойГруппы.задаток:
                        return "Задатки";
                    case ТипУчетнойГруппы.касса:
                        return "Касса";
                    default:
                        throw new Exception();
                }
            }

            public string ToReadableString()
            {
                if (Денежные_срества.ТипУчетнойГруппы.Деньги_третьих_лиц != учётная_группа)
                {
                    return TypeToString();
                }
                else
                {
                    return контрагент.Наименование;
                }
            }
        }

        public class Движение
        {
            public string сумма;
            public DateTime дата;

            public Адрес Откуда;
            public Адрес Куда;

            public string обоснование;
            public string примечание;
        }

        public class Учётная_группа
        {
            public Банковский_счёт банковский_счёт;
            public ТипУчетнойГруппы учётная_группа;
            public DateTime Дата_начального_сальдо;
            public decimal Начальное_сальдо;
            public decimal Текущее_сальдо;

            public string TypeToString(ТипУчетнойГруппы тип = new ТипУчетнойГруппы())
            {
                ТипУчетнойГруппы _тип = тип == new ТипУчетнойГруппы() ? учётная_группа : тип;
                switch (_тип)
                {
                    case ТипУчетнойГруппы.средстваДолжника:
                        return "Основной счет";
                    case ТипУчетнойГруппы.средстваАУ:
                        return "Собственные средства КУ";
                    case ТипУчетнойГруппы.залог:
                        return "Залог";
                    case ТипУчетнойГруппы.задаток:
                        return "Задаток";
                    default:
                        throw new Exception();
                }
            }
        }

        public interface IРасчёты
        {
            decimal ТекущееСальдо(IДела дела, string procedureID, Денежные_срества.Учётная_группа учетнаяГруппа);
        }

        public enum ТипУчетнойГруппы
        {
            средстваДолжника = 1 // Средства должника
       ,    средстваАУ = 2 // средства АУ
       ,    залог = 3 // Спец счёт дял залогов
       ,    задаток = 4 // Спец счёт дял задатков
       ,    касса = 5 // Касса должника
       ,    Деньги_третьих_лиц = 6
        }
    }
}
