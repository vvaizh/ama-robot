﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace AMA.Robot.task
{
    public partial class Планировщик : IПланировщик
    {

        task.IПостановщик постановщик { get; set; }
        IДела дела { get; set; }
        IРегистратор_изменений РегистраторИзменений { get; set; }
        ITimeProvider TimeProvider { get; set; }

        public Планировщик(
            task.IПостановщик постановщик,
            IДела дело,
            IРегистратор_изменений регистраторИзменений,
            ITimeProvider timeProvider)
        {
            this.постановщик = постановщик;
            дела = дело;
            РегистраторИзменений = регистраторИзменений;
            TimeProvider = timeProvider;
        }

        Action<IЗадача> Для(Собрание собрание)
        {
            return (IЗадача задача) =>
            {
                ((IДля_собрания)задача).собрание = собрание;
            };
        }

        Action<IЗадача> Для(Инвентаризация инвентаризация)
        {
            return (IЗадача задача) =>
            {
                ((IДля_инвентаризации)задача).инвентаризация = инвентаризация;
            };
        }

        Action<IЗадача> Для(Торги торги)
        {
            return (IЗадача задача) =>
            {
                ((IДля_торгов)задача).торги = торги;
            };
        }

        Action<IЗадача> В_банк(string наименование_банка)
        {
            return (IЗадача задача) =>
            {
                ((IДля_банка)задача).Наименование_банка = наименование_банка;
            };
        }

        Action<IЗадача> По_шаблону(Исходящее.Шаблон шаблон_исходящего)
        {
            return (IЗадача задача) =>
            {
                ((IПо_шаблону_исходящего)задача).шаблон_исходящего = шаблон_исходящего;
            };
        }

        Action<IЗадача> Для(Требование требование, Кредитор кредитор)
        {
            return (IЗадача задача) =>
            {
                ((IДля_кредитора)задача).кредитор = кредитор;
                ((IДля_требования)задача).требование = требование;
            };
        }

        Action<IЗадача> Для(Собрание собрание, Кредитор участник)
        {
            return (IЗадача задача) =>
            {
                ((IДля_собрания)задача).собрание = собрание;
                ((IДля_кредитора)задача).кредитор = участник;
            };
        }

        static DateTimeExtensions.За_число за(int ачисло)
        {
            return DateTimeExtensions.За(ачисло);
        }

        static DateTimeExtensions.Через_число через(int ачисло)
        {
            return DateTimeExtensions.Через(ачисло);
        }

        List<Кредитор> GetIncludedCreditors(string procedureId)
        {
            return дела.ВсеТребования(procedureId, included: true)
                .Select(c => c.КредиторID)
                .Distinct()
                .Select(i => дела.Кредитор(i))
                .ToList();
        }

        Процедура процедура;
        List<IЗадача> фронт_задач;

        void Запланировать_процедуру(Процедура _процедура, List<IЗадача> _фронт_задач)
        {
            процедура = _процедура;
            фронт_задач = _фронт_задач;
            switch (процедура.типПроцедуры)
            {
                case Процедура.ТипПроцедуры.Н: Запланировать_Наблюдение(); break;
                case Процедура.ТипПроцедуры.КП: Запланировать_Конкурсное_производство(); break;
                case Процедура.ТипПроцедуры.РД: Запланировать_Реструктуризацию_долгов(); break;
                case Процедура.ТипПроцедуры.РИ: Запланировать_Реализацию_имущества(); break;
            }
        }

        public List<IЗадача> Запланировать_фронт_задач(Процедура proc)
        {
            List<IЗадача> tasks = new List<IЗадача>();
            List<IЗадача> пропущенныеЗадачи = new List<IЗадача>();

            if (null != proc)
            {
                Запланировать_процедуру(proc, tasks);
                пропущенныеЗадачи = дела.ВсеПропущенныеЗадачи(proc.ID);
            }
            else
            {
                List<Процедура> процедуры = дела.ВсеПроцедуры();
                foreach (Процедура процедура in процедуры)
                {
                    Запланировать_процедуру(процедура, tasks);
                    пропущенныеЗадачи = дела.ВсеПропущенныеЗадачи(процедура.ID);
                }
            }

            foreach(IЗадача пропущеннаяЗадача in пропущенныеЗадачи)
            {
                IЗадача задача = tasks.SingleOrDefault(t => t.Формулировка == пропущеннаяЗадача.Формулировка && t.ПроцедураID == пропущеннаяЗадача.ПроцедураID);
                if(задача != null)
                {
                    tasks.Remove(задача);
                }
            }

            return tasks.OrderBy(t => t.К_сроку).ToList();
        }

        public void Запланировать_объявление_в_ЕФРСБ_о_введении_процедуры()
        {
			Efrsb_message_record ефрсбСообщение = дела.ПолучитьЕфрсбСообщение(процедура.Должник.Id, "BeginExecutoryProcess");

			if (null == ефрсбСообщение || ефрсбСообщение.PublishDate > через(8).рабочих_дня_после(процедура.Дата_начала))
                Запланировать(typeof(Опубликовать_на_ЕФРСБ_объявление.О_введении_процедуры), 
                    через(3).рабочих_дня_после(процедура.Дата_начала));
        }
        
        void Запланировать_публикацию_в_СМИ___о_введении_процедуры()
        {
            Исходящее note = дела.ВсеИсходящие(процедура, Исходящее.Шаблон.В_Коммерсант).FirstOrDefault();
            DateTime maxNoteDate = процедура.Дата_начала.AddDays(10);
            if (note != null || !Запланировать(typeof(Публикация_в_коммерсанте.О_введении_процедуры.Заказать), maxNoteDate))
            {
                var maxPubDate = (note.ДатаОтправки!=null? note.ДатаОтправки:maxNoteDate).AddDays(7);
                var pub = дела.ВсеПубликации(процедура.ID).FirstOrDefault();
                if (pub == null)
                    Запланировать(typeof(Публикация_в_коммерсанте.О_введении_процедуры.Зарегистрировать), maxPubDate);
            }
        }

        bool Запланировать_начальные_запросы_уведомления(DateTime к_сроку, params Исходящее.Шаблон [] шаблоны_запросов_уведомлений)
        {
            bool allRequestsCompleted = true;

            foreach (Исходящее.Шаблон шаблон in шаблоны_запросов_уведомлений)
            {
                IEnumerable<Исходящее> исходящие_по_шаблону= дела.ВсеИсходящие(процедура, шаблон);
                Исходящее запрос = исходящие_по_шаблону.SingleOrDefault();
                IЗадача задача = сформулировать(typeof(Отправить_уведомление_запрос), По_шаблону(шаблон), к_сроку);
                if (null == запрос && Запланировать(задача))
                {
                    allRequestsCompleted = false;
                }
                else if (шаблон.Является_запросом)
                {
                    
                    if(null != запрос)
                    {
                        Ответ response = дела.ОтветНа(запрос.ID);
                        if (null == response)
                        {
                            DateTime maxResponseDate = (запрос.ДатаОтправки != null ? запрос.ДатаОтправки : к_сроку).AddDays(7);
                            if (TimeProvider.Now <= maxResponseDate)
                                allRequestsCompleted = false;
                            else
                            {
                                allRequestsCompleted &= !Запланировать
                                    (typeof(Отреагировать_на_отсутствие_ответа), По_шаблону(шаблон), maxResponseDate.AddDays(7));
                            }
                        }
                    }
                   
                }
            }
            return allRequestsCompleted;
        }

        void Запланировать_запросы_в_банки()
        {
            Исходящее запрос_в_ИФНС = дела.ВсеИсходящие(процедура, Исходящее.Шаблон.В_ИФНС).SingleOrDefault();
            if (null != запрос_в_ИФНС)
            {
                Ответ ответ_на_запрос_в_ИФНС = дела.ОтветНа(запрос_в_ИФНС.ID);
                if (null != ответ_на_запрос_в_ИФНС)
                {
                    bool получена_вся_информация = true;
                    IEnumerable<Исходящее> запросы_в_банк = дела.ВсеИсходящие(процедура, Исходящее.Шаблон.В_банк);
                    foreach (string наименование_банка in дела.ВсеСчета(процедура.ID).Select(a => a.Банк).Distinct())
                    {
                        Исходящее запрос_в_банк = запросы_в_банк.SingleOrDefault(r => r.Получатель == наименование_банка);
                        IЗадача в_банк = сформулировать(typeof(Отправить_уведомление_запрос_в_банк), В_банк(наименование_банка), через(7).дней_после(ответ_на_запрос_в_ИФНС.ДатаПолучения));
                        if (null == запрос_в_банк && Запланировать(в_банк))
                            получена_вся_информация = false;
                    }
                    if (получена_вся_информация)
                    {
                        Собрание собрание = дела.ПоследнееСобрание(процедура.ID);
                        if (!дела.ВсеАнализы(процедура.ID).Any())
                        {
                            DateTime крайний_срок_проведения_финанализа = (null!=собрание) ? за(5).дней_до(собрание.Дата) : за(15).дней_до(процедура.Дата_окончания);
                            Запланировать(typeof(Провести_финансовый_анализ), к_сроку: крайний_срок_проведения_финанализа);
                        }
                    }
                }
            }
        }

        void Запланировать_задачи_по_кредиторам(Func<Публикация, Требование, DateTime> крайний_срок_возражений)
        {
            Публикация публикация = дела.ВсеПубликации(процедура.ID).FirstOrDefault(); // TODO: better query?
            if (null != публикация)
            {
                foreach (Требование claim in дела.ВсеТребования(процедура.ID, included: false))
                {
                    Кредитор creditor = дела.Кредитор(claim.КредиторID);
                    if (дела.Задача_решена(new TasksDB { Формулировка = "Отреагировать на требование " + creditor.Наименование, ПроцедураID = процедура.ID }) == null)
                        Запланировать(typeof(Проанализировать_новое_требование_кредитора), Для(claim, creditor), крайний_срок_возражений(публикация, claim));

                    if (процедура.типПроцедуры == Процедура.ТипПроцедуры.КП || процедура.типПроцедуры == Процедура.ТипПроцедуры.РИ)
                    {
                        List<Efrsb_message_record> efrsb = дела.ВсеЕфрсбСообщения(процедура.Должник.Id).Where(m => m.MessageType == "ReceivingCreditorDemand").ToList();
                        Efrsb_message_record msg = null;
                        if (efrsb.Count > 0)
                        {
                            msg = GetCreditorNameinEFRSBMsg(efrsb, creditor.Наименование);
                        }

                        if (efrsb.Count() == 0 && msg == null)
                            Запланировать(typeof(Опубликовать_на_ЕФРСБ_объявление.О_получении_требования_кредитора), Для(claim, creditor), claim.ДатаПолучения.AddDays(5));
                    }
                }
            }
        }

        void Запланировать_собрание_кредиторов(Собрание собрание)
        {
            if (собрание.Проведено)
            {
                IEfrsb_message_record ефрсбСообщение = дела.ПолучитьЕфрсбСообщение(процедура.Должник.Id, "MeetingResult");
                if (null==ефрсбСообщение || ефрсбСообщение.PublishDate > через(10).рабочих_дней_после(собрание.Дата))
                    Запланировать(typeof(Опубликовать_на_ЕФРСБ_объявление.О_результатах_собрания_кредиторов), Для(собрание), через(5).рабочих_дней_после(собрание.Дата));
            }
            else
            {
                
                var notifications = дела.ВсеУведомления(процедура.ID, Исходящее.Шаблон.Уведомление_о_СК.ToString());
                var maxNotificationDate = собрание.Дата.AddDays(-14);
                foreach (Кредитор кредитор in GetIncludedCreditors(процедура.ID))
                {
                    if (!notifications.Any(
                        r => r.Получатель == кредитор.Наименование
                        && r.ДатаОтправки > собрание.ДатаНазначения
                    ))
                        Запланировать(typeof(Собрание_кредиторов.Уведомить), Для(собрание, кредитор), maxNotificationDate);
                }

                var eMsg = дела.ПолучитьЕфрсбСообщение(процедура.Должник.Id, "Meeting");
                if (eMsg == null || eMsg.PublishDate > maxNotificationDate.AddDays(5))
                    Запланировать(typeof(Опубликовать_на_ЕФРСБ_объявление.О_собрании_кредиторов), Для(собрание), maxNotificationDate);
            }
        }

        IЗадача сформулировать(Type тип_задачи, Action<IЗадача> настроить_дополнительно, DateTime к_сроку)
        {
            return постановщик.сформулировать(тип_задачи, к_сроку, настроить_дополнительно, процедура);
        }
        IЗадача сформулировать(Type тип_задачи, DateTime к_сроку)
        {
            return постановщик.сформулировать(тип_задачи, к_сроку, процедура);
        }

        bool Запланировать(Type тип_задачи, Action<IЗадача> дополнительно_настроить, DateTime к_сроку)
        {
            return Запланировать(сформулировать(тип_задачи, дополнительно_настроить, к_сроку));
        }

        bool Запланировать(Type type, DateTime к_сроку)
        {
            return Запланировать(сформулировать(type, к_сроку));
        }

        bool Запланировать(IЗадача задача)
        {
            if (дела.Задача_решена(задача) !=null)
            {
                return false;
            }
            else
            {
                фронт_задач.Add(задача);
                return true;
            }
        }

        struct TimelineEntry
        {
            public readonly DateTime date;
            public readonly string description;

            public TimelineEntry(DateTime date, string description)
            {
                this.date = date;
                this.description = description;
            }

            public override string ToString()
            {
                return String.Format("{0} {1}", date.ToString("d"), description);
            }
        }

        public string Build_timeline(string procedureId)
        {
            var timeline = new List<TimelineEntry>();
            var proc = дела.Процедура(procedureId);
            timeline.Add(new TimelineEntry(proc.Дата_начала, String.Format("Зарегистрирована процедура {0} над {1}", proc.тип.Полностью, proc.Должник.Наименование)));
            timeline.Add(new TimelineEntry(proc.Дата_окончания, String.Format("Окончание процедуры {0}", proc.тип.Полностью)));

            foreach (Исходящее msg in дела.ВсеИсходящие(proc.ID))
            {
                string description;
                if (msg.По(Исходящее.Шаблон.В_банк))
                    description = String.Format("Отправлен Запрос в {0}", msg.Получатель);
                else if (msg.По(Исходящее.Шаблон.Уведомление_о_СК))
                    description = String.Format("Отправлено Уведомление о СК \"{0}\"", msg.Получатель);
                else
                    description = String.Format("Отправлено {0}", msg.По_шаблону().Наименование);
                timeline.Add(new TimelineEntry(msg.ДатаОтправки, description));
            }

            foreach (Ответ msg in дела.ВсеВходящие(proc.ID))
            {
                string description;
                if (!String.IsNullOrEmpty(msg.ЗапросID))
                {
                    Исходящее req = дела.Исходящее(msg.ЗапросID);
                    if (req.По(Исходящее.Шаблон.В_банк))
                        description = String.Format("Получен ответ на Запрос в {0}", req.Получатель);
                    else
                        description = String.Format("Получен ответ на {0}", req.По_шаблону().Наименование);
                }
                else if (!string.IsNullOrEmpty(msg.Тип))
                    description = String.Format("Получен {0}", Исходящее.Шаблон.Типа(msg.Тип).Наименование);
                else
                    description = String.Format("Получено входящее от {0}", msg.Отправитель);
                timeline.Add(new TimelineEntry(msg.ДатаПолучения, description));
            }

            foreach (var msg in дела.ВсеЕфрсбСообщения(proc.Должник.Id))
            {
                timeline.Add(new TimelineEntry(msg.PublishDate, String.Format("Опубликовано сообщение в ЕФРСБ {0}", msg.MessageType)));
            }

            foreach (var claim in дела.ВсеТребования(proc.ID))
            {
                var cred = дела.Кредитор(claim.КредиторID);
                timeline.Add(new TimelineEntry(claim.ДатаПолучения, String.Format("Получено ТК \"{0}\"", cred.Наименование)));
                if (claim.Включено)
                    timeline.Add(new TimelineEntry(claim.ДатаВключения, String.Format("ТК \"{0}\" включено в РТК", cred.Наименование)));
            }

            {
                var pub = дела.ВсеПубликации(proc.ID).FirstOrDefault();
                if (pub != null)
                {
                    timeline.Add(new TimelineEntry(pub.Дата, String.Format("Публикация в \"{0}\"", pub.СМИ )));
                    if (proc.типПроцедуры == Процедура.ТипПроцедуры.Н)
                    {
                        timeline.Add(new TimelineEntry(pub.Дата.AddDays(30), "Окончание приёма ТК"));
                        timeline.Add(new TimelineEntry(pub.Дата.AddDays(45), "Окончание приёма возражений на ТК"));
                        timeline.Add(new TimelineEntry(pub.Дата.AddDays(75), "Завершение формирования ТК для 1СК"));
                    }
                    else if (proc.типПроцедуры == Процедура.ТипПроцедуры.КП)
                    {
                        timeline.Add(new TimelineEntry(pub.Дата.AddMonths(2), "Закрытие РТК"));
                    }
                }
            }

            foreach (var meeting in дела.ВсеСобрания(proc.ID))
            {
                timeline.Add(new TimelineEntry(meeting.ДатаНазначения, "Назначено СК"));
                if (meeting.Проведено)
                    timeline.Add(new TimelineEntry(meeting.Дата, "Проведено СК"));
            }

            foreach (var fa in дела.ВсеАнализы(proc.ID))
            {
                timeline.Add(new TimelineEntry(fa.Дата, "Проведен Финанализ"));
            }

            foreach (var inv in дела.ВсеИнвентаризации(proc.ID))
            {
                timeline.Add(new TimelineEntry(inv.Дата, "Проведена инвентаризация"));
            }

            foreach (var sale in дела.ВсеТорги(proc.ID))
            {
                timeline.Add(new TimelineEntry(sale.ДатаОткрытия, "Открыты торги"));
                timeline.Add(new TimelineEntry(sale.ДатаЗавершения, "Завершены торги"));
            }

            timeline.Sort((a, b) => a.date.CompareTo(b.date));
            return string.Join("\r\n", timeline.Select(t => t.ToString()).ToArray());
        }

        Efrsb_message_record GetCreditorNameinEFRSBMsg(List<Efrsb_message_record> messages, string creditorName)
        {
            foreach(Efrsb_message_record msg in messages)
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(msg.Body);
                XmlElement xRoot = xDoc.DocumentElement;
                XmlNodeList xnode = xRoot.GetElementsByTagName("BankruptFirm");
                foreach(XmlNode node in xnode)
                {
                    string ss = node.Attributes.GetNamedItem("ShortName").Value.ToString();
                    if (ss.Replace("\"", "").Replace(" ", "") == creditorName.Replace("\"", "").Replace(" ", ""))
                    {
                        return msg;
                    }
                }
            }
            return new Efrsb_message_record();
        }
    }
}
