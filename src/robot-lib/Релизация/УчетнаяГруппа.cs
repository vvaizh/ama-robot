﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMA.Robot
{ 
    public class ДенежныеОперации
    {
        
        public decimal сумма { get; set; }
        public decimal комиссияБанка { get; set; }
        public DateTime дата { get; set; }
        public КатегорияОперации категория { get; set; }
        public ТипУчетнойГруппы типУчетнойГруппы { get; set; }
        public Детали детали { get; set; }

        public decimal ПолучитьЗнакСуммы(ТипУчетнойГруппы группа = new ТипУчетнойГруппы())
        {
            switch (категория)
            {
                case КатегорияОперации.доход:
                    return сумма;
                case КатегорияОперации.расход:
                    return сумма * (-1);
                case КатегорияОперации.перенос:
                    return детали.деталиПереноса.сУчетнойГруппы == группа ? сумма * (-1) : сумма;
                default:
                    throw new Exception();
            }
        }

        public string TypeToString(ТипУчетнойГруппы тип = new ТипУчетнойГруппы())
        {
            ТипУчетнойГруппы _тип = тип == new ТипУчетнойГруппы() ? типУчетнойГруппы : тип;
            switch (_тип)
            {
                case ТипУчетнойГруппы.основнойСчет:
                    return "Основной счет";
                case ТипУчетнойГруппы.собственныеСредстваКУ:
                    return "Собственные средства КУ";
                case ТипУчетнойГруппы.залог:
                    return "Залог";
                case ТипУчетнойГруппы.задаток:
                    return "Задаток";
                default:
                    throw new Exception();
            }
        }
    }
    
    public class Детали
    {
        public ДеталиДохода     деталиДохода        { get; set; }
        public ДеталиРасхода    деталиРасхода       { get; set; }
        public ДеталиПереноса   деталиПереноса      { get; set; }
    }

    public interface IДетали
    {

    }

    public class ДеталиДохода : IДетали
    {
        public string получатель { get; set; }
        public string наСчет { get; set; }
        public string банковскийСчет { get; set; }
        public string видДохода { get; set; }
    }

    public class ДеталиРасхода : IДетали
    {
        public string получатель { get; set; }
        public string соСчета { get; set; }
        public string банковскийСчет { get; set; }
        public string видРасхода { get; set; }
        public string цельРасхода { get; set; }
    }

    public class ДеталиПереноса : IДетали
    {
        public ТипУчетнойГруппы сУчетнойГруппы { get; set; }
        public ТипУчетнойГруппы вУчетнуюГруппу { get; set; }
        public string видПереноса { get; set; }
        public string обоснование { get; set; }
        public string примечание { get; set; }
    }

    public class УчетнаяГруппа : В_рамках_процедуры
    {
        public Счет основнойСчет { get; set; }
        public decimal собственныеСредстваКУ { get; set; }
        public decimal залог { get; set; }
        public decimal задаток { get; set; }
        public List<ДенежныеОперации> денежныеОперации { get; set; }
    }

    public enum ТипУчетнойГруппы
    {
        основнойСчет =1,
        собственныеСредстваКУ =2,
        залог =3,
        задаток =4
    }

    public enum КатегорияОперации
    {
        доход,
        расход,
        перенос
    }

}
