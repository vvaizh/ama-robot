﻿using System;

namespace AMA.Robot.task.Сотрудников
{
    class Уведомить_об_увольнении : AЗадача_по_шаблону_исходящего
    {
        public override string Формулировка
        {
            get
            {
                return "Уведомить работников об увольнении";
            }
        }
    }

    class Уволить : AЗадача_по_шаблону_исходящего
    {
        public override string Формулировка
        {
            get
            {
                return "Уволить работников";
            }
        }
    }
}