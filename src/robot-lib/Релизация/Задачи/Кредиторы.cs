using System;

namespace AMA.Robot.task
{
    class Проанализировать_новое_требование_кредитора : Задача, IДля_кредитора, IДля_требования
    {
        public Кредитор кредитор { get; set; }
        public Требование требование { get; set; }

        public override string Формулировка
        {
            get { return String.Format("Отреагировать на требование \"{0}\"", кредитор.Наименование); }
        }
    }
}