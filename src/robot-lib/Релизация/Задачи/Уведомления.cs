using System;

namespace AMA.Robot.task.Уведомить_кредиторов
{
    class О_введении_РД : AЗадача_для_процедуры
    {
        public override string Формулировка
        {
            get { return "Уведомить известных кредиторов о введении РД"; }
        }
    }

    class О_введении_РИ : AЗадача_для_процедуры
    {
        public override string Формулировка
        {
            get { return "Уведомить известных кредиторов о реализации имущества"; }
        }
    }
}