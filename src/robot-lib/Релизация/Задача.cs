﻿using System;
using System.Collections.Generic;

namespace AMA.Robot
{
    public abstract class Задача : AMA.Robot.IЗадача
    {
        public string       ЗадачаID        { get; set; }
        public string       ПроцедураID     { get; set; }
        public DateTime     К_сроку         { get; set; }
        public task.Статус  Статус          { get; set; }
        public DateTime     ДатаРешения     { get; set; }
        public ТипЗадачи    Тип             { get; set; }

        public abstract string Формулировка { get; }

        public Задача()
        {
            Статус = task.Статус.Открыта;
        }

        public override bool Equals(object obj)
        {
            var task = obj as Задача;
            return task != null &&
                   ПроцедураID == task.ПроцедураID &&
                   К_сроку == task.К_сроку &&
                   Формулировка == task.Формулировка;
        }

        public override int GetHashCode()
        {
            var hashCode = 1254568802;
            hashCode = hashCode * -1521134295 + ПроцедураID.GetHashCode();
            hashCode = hashCode * -1521134295 + К_сроку.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Формулировка);
            return hashCode;
        }
    }

    public abstract class AЗадача_для_собрания : Задача, IДля_собрания
    {
        public Собрание собрание { get; set; }
    }

    public abstract class AЗадача_для_процедуры : Задача, IДля_процедуры
    {
        public Процедура процедура { get; set; }
    }

    public abstract class AЗадача_по_шаблону_исходящего : Задача, IПо_шаблону_исходящего
    {
        public Исходящее.Шаблон шаблон_исходящего { get; set; }
    }
}
