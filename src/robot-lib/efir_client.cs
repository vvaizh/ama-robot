﻿/*
 * ВНИМАНИЕ! ATTENTION! AHTUNG!
 * Данный файл редактируется и тестируется в проекте "efrsb"!
 * Он НЕ должен редактироваться в проекте "AMA"!
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

namespace ama.efrsb
{
    public class PlainMessage
    {
        public int Revision;
        public string Body;
    };

    public class Client
    {
        public static string base_url = "http://probili.ru/efrsb/";

        public class Proxy
        {
            public string host;
            public int port;
            public string login;
            public string password;
        }
        public static Proxy proxy_settings = new Proxy();

        public static JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        public List<PlainMessage> GetNewMessages(string inn, string ogrn, string snils, int revision_greater_than = 0, int limit = 0)
        {
            if (null == inn)
                inn = "";
            if (null == ogrn)
                ogrn = "";
            if (null == snils)
                snils = "";
            string url = string.Format("{0}/GetNewMessages.php?inn={1}&ogrn={2}&snils={3}&revision-greater-than={4}&limit={5}",
                base_url, inn, ogrn, snils, revision_greater_than, 0 == limit || limit > 100 ? 100 : limit);

            HttpStatusCode status;
            string responce_body = HttpGet(url, out status);

            if (HttpStatusCode.OK != status)
                throw new Exception("Can not GetNewMessages!", status, responce_body);

            return jsSerializer.Deserialize<List<PlainMessage>>(responce_body);
        }

        string HttpGet(string url, out HttpStatusCode status)
        {
            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(url);
            SafeFixProxy(request);
            request.Method = "GET";
            return ReadResponce(request, out status);
        }

        static string ReadResponce(HttpWebRequest request, out HttpStatusCode status)
        {
            using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse())
            using (Stream receiveStream = response.GetResponseStream())
            using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
            {
                status = response.StatusCode;
                return ReadResponceText(status, readStream);
            }
        }

        static string ReadResponceText(HttpStatusCode status, StreamReader readStream)
        {
            if (HttpStatusCode.OK == status)
            {
                return readStream.ReadToEnd();
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                for (int count = 0; count < 100; count++)
                {
                    string line = readStream.ReadLine();
                    if (null == line)
                    {
                        break;
                    }
                    else
                    {
                        sb.AppendLine(line);
                    }
                }
                return sb.ToString();
            }
        }

        void SafeFixProxy(HttpWebRequest request)
        {
            if (null != proxy_settings && !string.IsNullOrEmpty(proxy_settings.host))
            {
                WebProxy proxy = new WebProxy(proxy_settings.host, proxy_settings.port);
                if (!string.IsNullOrEmpty(proxy_settings.login))
                    proxy.Credentials = new NetworkCredential(proxy_settings.login, proxy_settings.password);
                request.Proxy = proxy;
            }
        }

        public class Exception : System.Exception
        {
            public HttpStatusCode status;
            public string responce_body;
            public Exception(string text, HttpStatusCode s, string b)
                : base(text)
            {
                status = s;
                responce_body = b;
            }
            public Exception(string text, HttpStatusCode s, string b, System.Exception e)
                : base(text, e)
            {
                status = s;
                responce_body = b;
            }
        }
    }
}
