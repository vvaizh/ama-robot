﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.OleDb;

namespace AMA.Robot.mdb
{
    public class Дело : IДела_в_работе
    {
        private Guid merge_id = Guid.NewGuid();
        private OleDbConnection connection;
        IGuidProvider guid;
        ITimeProvider timeProvider;
        public Дело(IGuidProvider _guid, string path, ITimeProvider _timeProvider)
        {
            timeProvider = _timeProvider;
            guid = _guid;
            try
            {
                connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"" +
                                 path + "\"; Jet OLEDB:Database Password=\"73BBE7DA0AEDF1CBA" + (char)1 + (char)8 + (char)27 + "\";" +
                                 "Jet OLEDB:Engine Type=5;Jet OLEDB:Encrypt Database=True;");
                connection.Open();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }

        }

        internal class Convert_type_request_шаблон_тип
        {
            internal Dictionary<string, int> type_request_BY_шаблон_тип = new Dictionary<string, int>();
            internal Dictionary<int, string> шаблон_тип_BY_type_request = new Dictionary<int, string>();
            internal void Add(int type_request, Исходящее.Шаблон шаблон)
            {
                type_request_BY_шаблон_тип.Add(шаблон.Тип, type_request);
                шаблон_тип_BY_type_request.Add(type_request, шаблон.Тип);
            }
        }

        static Convert_type_request_шаблон_тип type_request_шаблон_тип = null;

        static void Упорядочить_шаблоны()
        {
            if (null == type_request_шаблон_тип)
            {
                Convert_type_request_шаблон_тип cnv = new Convert_type_request_шаблон_тип();
                cnv.Add(0, Robot.Исходящее.Шаблон.В_Коммерсант);
                cnv.Add(1, Robot.Исходящее.Шаблон.В_ИФНС);
                cnv.Add(2, Robot.Исходящее.Шаблон.В_ГИБДД);
                cnv.Add(3, Robot.Исходящее.Шаблон.В_банк);
                cnv.Add(5, Robot.Исходящее.Шаблон.В_ФСС);
                cnv.Add(6, Robot.Исходящее.Шаблон.В_ПФ);
                cnv.Add(7, Robot.Исходящее.Шаблон.В_ССП);
                cnv.Add(8, Robot.Исходящее.Шаблон.В_ГИМС);
                cnv.Add(9, Robot.Исходящее.Шаблон.В_Гостехнадзор);
                cnv.Add(10, Robot.Исходящее.Шаблон.В_Росимущество);
                cnv.Add(11, Robot.Исходящее.Шаблон.Руководителю);
                cnv.Add(12, Robot.Исходящее.Шаблон.В_Росреестр);
                cnv.Add(13, Robot.Исходящее.Шаблон.Должнику);
                cnv.Add(14, Robot.Исходящее.Шаблон.В_ЕГРН);
                cnv.Add(15, Robot.Исходящее.Шаблон.В_ЗАГС);
                cnv.Add(16, Robot.Исходящее.Шаблон.В_БТИ);
                cnv.Add(17, Robot.Исходящее.Шаблон.В_Роспатент);
                cnv.Add(18, Robot.Исходящее.Шаблон.Возражение_на_ТК);

                // Далее следуют не используемые в реальном времени шаблоны, но они необходимы для тестов и возможного использования в будущем
                cnv.Add(101, Robot.Исходящее.Шаблон.О_введении);
                cnv.Add(102, Robot.Исходящее.Шаблон.В_ц_кат_кред_ист);
                cnv.Add(103, Robot.Исходящее.Шаблон.План_РД);
                cnv.Add(104, Robot.Исходящее.Шаблон.План_РИ);
                cnv.Add(105, Robot.Исходящее.Шаблон.Отчёт_об_оценке);
                cnv.Add(106, Robot.Исходящее.Шаблон.Уведомление_о_СК);
                cnv.Add(107, Robot.Исходящее.Шаблон.Отчёт_в_АС);
                cnv.Add(108, Robot.Исходящее.Шаблон.Отчёт_в_СРО);
                cnv.Add(109, Robot.Исходящее.Шаблон.О_ликвидации);
                type_request_шаблон_тип = cnv;
            }
        }

        static int type_request(string тип_шаблона_исходящего)
        {
            Упорядочить_шаблоны();
            return type_request_шаблон_тип.type_request_BY_шаблон_тип[тип_шаблона_исходящего];
        }

        static string тип_шаблона_исходящего(int type_request)
        {
            Упорядочить_шаблоны();
            return type_request_шаблон_тип.шаблон_тип_BY_type_request[type_request];
        }

        private Денежные_срества.ТипУчетнойГруппы ПолучитьТипУчетнойГруппы(Guid account_id)
        {
            string тип = string.Empty;
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT account_name FROM Account WHERE account_id=@account_id";
                cmd.Parameters.AddWithValue("account_id", account_id);
                тип = cmd.ExecuteScalar().ToString();
            }

            switch (тип.ToLower())
            {
                case "основной":
                    return Денежные_срества.ТипУчетнойГруппы.средстваДолжника;
                case "залог":
                    return Денежные_срества.ТипУчетнойГруппы.залог;
                case "задаток":
                    return Денежные_срества.ТипУчетнойГруппы.задаток;
                case "собственные средства ау":
                    return Денежные_срества.ТипУчетнойГруппы.средстваАУ;
                default:
                    throw new Exception();
            }
        }
        
        public void ЗарегистрироватьКонтрагента (Контрагент контрагент)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = @"INSERT INTO Contact (contact_id, type_id,sex,lastname,isAccredited, bank_is_structured, inn) VALUES
                (?,?,?,?,?,?,?)";
                OleDbParameterCollection pr = cmd.Parameters;
                pr.AddWithValue("contact_id", Guid.NewGuid());
                pr.AddWithValue("type_id", new Guid("00000003-0000-0000-0000-000000000000"));
                pr.AddWithValue("sex", true);
                pr.AddWithValue("lastname", контрагент.Наименование);
                pr.AddWithValue("isAccredited", true);
                pr.AddWithValue("bank_is_structured", true);
                pr.AddWithValue("inn", контрагент.ИНН);
                cmd.ExecuteNonQuery();
            }
        }

        public Контрагент КонтрагентByContactId (string contact_id)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT type_id, lastname, inn FROM Contact WHERE contact_id=@contact_id";
                cmd.Parameters.AddWithValue("contact_id", new Guid(contact_id));
                OleDbDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    Контрагент контрагент = new Контрагент();
                    контрагент.TypeId = reader.GetGuid(0).ToString();
                    контрагент.Наименование = reader.GetString(1);
                    контрагент.ИНН = reader.GetString(2);
                    return контрагент;
                }
            }
            return null;
        }

        public Контрагент КонтрагентПоИНН(string inn)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT contact_id, type_id, lastname FROM Contact WHERE inn=@inn";
                cmd.Parameters.AddWithValue("inn", inn);
                OleDbDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    Контрагент контрагент = new Контрагент();
                    контрагент.Id = reader.GetGuid(0).ToString();
                    контрагент.TypeId = reader.GetGuid(1).ToString();
                    контрагент.Наименование = reader.GetString(2);
                    return контрагент;
                }
            }
            return null;
        }

        public IEnumerable<Денежные_срества.Учётная_группа> ВсеУчетныеГруппы(string manproc_id)
        {
            List<Денежные_срества.Учётная_группа> учетныеГруппы = new List<Денежные_срества.Учётная_группа>();
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = @"SELECT account_id FROM Account WHERE manproc_id = @manproc_id";
                cmd.Parameters.AddWithValue("manproc_id", new Guid(manproc_id));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Денежные_срества.Учётная_группа учетнаяГруппа = new Денежные_срества.Учётная_группа();
                        учетнаяГруппа.Начальное_сальдо = 0;
                        учетнаяГруппа.учётная_группа = ПолучитьТипУчетнойГруппы(reader.GetGuid(0));
                        учетныеГруппы.Add(учетнаяГруппа);
                    }
                }
            }

            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = @"SELECT aс.amount FROM AccountTransaction AS aс
                INNER JOIN Account AS A ON aс.account_id=A.account_id WHERE 
                A.manproc_id=@manproc_id AND A.account_name=@account_name AND aс.transaction_type=5";

                cmd.Parameters.AddWithValue("manproc_id", new Guid(manproc_id));
                cmd.Parameters.AddWithValue("account_name", "Основной");
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (reader.GetDecimal(0) != 0)
                        {
                            учетныеГруппы.SingleOrDefault(t => t.учётная_группа ==
                            Денежные_срества.ТипУчетнойГруппы.средстваДолжника).Начальное_сальдо = reader.GetDecimal(0);
                        }
                    }
                }
            }

            return учетныеГруппы;
        }
        /// <summary>
        /// Получить из какой и в какую учетные группы был сделан перенос денежных средств
        /// </summary>
        /// <param name="merge_id"></param>
        /// <returns></returns>
        private Денежные_срества.Движение ДанныеПриПереносеДенежныхСредств(Guid merge_id)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = @"SELECT amount, account_id FROM AccountTransaction WHERE merge_id=@merge_id";
                cmd.Parameters.AddWithValue("merge_id", merge_id);
                Денежные_срества.Движение перенос = new Денежные_срества.Движение();

                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        перенос.сумма = reader.GetDecimal(0).ToString();
                        if (reader.GetDecimal(0) > 0)
                        {
                            перенос.Куда = new Денежные_срества.Адрес();
                            перенос.Куда.учётная_группа = ПолучитьТипУчетнойГруппы(reader.GetGuid(1));
                        }
                        else
                        {
                            перенос.Откуда = new Денежные_срества.Адрес();
                            перенос.Откуда.учётная_группа = ПолучитьТипУчетнойГруппы(reader.GetGuid(1));
                        }
                    }
                }
                return перенос;
            }
        }
        public IEnumerable<Денежные_срества.Движение> ДвиженияДенежныхСредств(string manproc_id)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                List<Денежные_срества.Движение> движения = new List<Денежные_срества.Движение>();
                cmd.CommandText = @"SELECT ac.transaction_type, ac.transaction_date,
                ac.amount, ac.payer, ac.merge_id, ac.account_id, ac.category FROM (AccountTransaction AS ac
                INNER JOIN Account AS A ON A.account_id = ac.account_id)
                WHERE A.manproc_id=@manproc_id";

                cmd.Parameters.AddWithValue("manproc_id", new Guid(manproc_id));

                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (reader.GetInt32(0) != 5)
                        {
                            Денежные_срества.Движение движение = new Денежные_срества.Движение();
                            движение.Куда = new Денежные_срества.Адрес();
                            движение.Откуда = new Денежные_срества.Адрес();
                            движение.дата = reader.GetDateTime(1);
                            движение.сумма = reader.GetDecimal(2).ToString();

                            if (Convert.ToDecimal(движение.сумма) > 0)
                            {
                                движение.Откуда.контрагент = new Контрагент();
                                if (!reader.IsDBNull(3))
                                    движение.Откуда.контрагент.Наименование = reader.GetString(3);

                                движение.Откуда.учётная_группа = ПолучитьТипУчетнойГруппы(reader.GetGuid(5));
                            }
                            else
                            { 
                                движение.Куда.контрагент = new Контрагент();
                                if(!reader.IsDBNull(3))
                                    движение.Куда.контрагент.Наименование = reader.GetString(3);

                                движение.Куда.учётная_группа = ПолучитьТипУчетнойГруппы(reader.GetGuid(5));

                            }
                            if(!reader.IsDBNull(6))
                            {
                                движение.обоснование = reader.GetString(6);
                            }
                            else
                            {
                                движение.обоснование = "";
                            }
                            движения.Add(движение);
                        }

                    }
                }
                return движения;
            }

        }

        private void зарегистрироватьПеренос(string manproc_id, Денежные_срества.Движение движение)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = @"INSERT INTO AccountTransaction 
                (transaction_id, date_created, transaction_date, amount, category, payment_date, transaction_type, account_id, merge_id) 
                VALUES (?,?,?,?,?,?,?,?,?)";
                cmd.Parameters.AddWithValue("transaction_id", guid.New);
                cmd.Parameters.AddWithValue("date_created", движение.дата.ToShortDateString());
                cmd.Parameters.AddWithValue("transaction_date", движение.дата.ToShortDateString());
                cmd.Parameters.AddWithValue("amount", движение.сумма);
                cmd.Parameters.AddWithValue("category", движение.обоснование);
                cmd.Parameters.AddWithValue("payment_date", движение.дата.ToShortDateString());
                if (движение.Куда.учётная_группа == Денежные_срества.ТипУчетнойГруппы.Деньги_третьих_лиц)
                {
                    cmd.Parameters.AddWithValue("transaction_type", 3);
                    cmd.Parameters.AddWithValue("account_id", ПолучитьAccountId(manproc_id, движение.Откуда.учётная_группа));
                    cmd.Parameters.AddWithValue("merge_id", merge_id);
                }
                else if (движение.Откуда.учётная_группа == Денежные_срества.ТипУчетнойГруппы.Деньги_третьих_лиц)
                {
                    cmd.Parameters.AddWithValue("transaction_type", 3);
                    cmd.Parameters.AddWithValue("account_id", ПолучитьAccountId(manproc_id, движение.Куда.учётная_группа));
                    cmd.Parameters.AddWithValue("merge_id", merge_id);
                }

                cmd.ExecuteNonQuery();
            }
        }

        public void ЗарегистрироватьДенежнуюОперацию(string manproc_id, Денежные_срества.Движение движение, int type = 0)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = @"INSERT INTO AccountTransaction 
                (transaction_id, date_created, transaction_date, amount, category, payment_date, transaction_type, account_id, payer) 
                VALUES (?,?,?,?,?,?,?,?,?)";
                cmd.Parameters.AddWithValue("transaction_id", Guid.NewGuid());
                cmd.Parameters.AddWithValue("date_created", движение.дата.ToShortDateString());
                cmd.Parameters.AddWithValue("transaction_date", движение.дата.ToShortDateString());
                cmd.Parameters.AddWithValue("amount", движение.сумма);
                cmd.Parameters.AddWithValue("category", движение.обоснование);
                cmd.Parameters.AddWithValue("payment_date", движение.дата.ToShortDateString());
                if (движение.Куда.учётная_группа == Денежные_срества.ТипУчетнойГруппы.Деньги_третьих_лиц)
                {
                    cmd.Parameters.AddWithValue("transaction_type", 2);
                    cmd.Parameters.AddWithValue("account_id", ПолучитьAccountId(manproc_id, движение.Откуда.учётная_группа));
                    cmd.Parameters.AddWithValue("payer", движение.Куда.контрагент != null? движение.Куда.контрагент.Наименование : "");
                }
                else if (движение.Откуда.учётная_группа == Денежные_срества.ТипУчетнойГруппы.Деньги_третьих_лиц)
                {
                    cmd.Parameters.AddWithValue("transaction_type", 1);
                    cmd.Parameters.AddWithValue("account_id", ПолучитьAccountId(manproc_id, движение.Куда.учётная_группа));
                    cmd.Parameters.AddWithValue("payer", движение.Откуда.контрагент != null ? движение.Откуда.контрагент.Наименование : "");
                }
                else
                {
                    merge_id = guid.New;

                    Денежные_срества.Движение откуда = new Денежные_срества.Движение();
                    откуда.дата = движение.дата;
                    откуда.обоснование = движение.обоснование;
                    откуда.Откуда = движение.Откуда;
                    откуда.Куда = new Денежные_срества.Адрес();
                    откуда.Куда.учётная_группа = Денежные_срества.ТипУчетнойГруппы.Деньги_третьих_лиц;
                    откуда.сумма = "-" + движение.сумма;
                    зарегистрироватьПеренос(manproc_id, откуда);

                    Денежные_срества.Движение куда = new Денежные_срества.Движение();
                    куда.дата = движение.дата;
                    куда.обоснование = движение.обоснование;
                    куда.Куда = движение.Куда;
                    куда.сумма = движение.сумма;
                    куда.Откуда = new Денежные_срества.Адрес();
                    куда.Откуда.учётная_группа = Денежные_срества.ТипУчетнойГруппы.Деньги_третьих_лиц;
                    зарегистрироватьПеренос(manproc_id, куда);
                    
                    return;
                }
                cmd.ExecuteNonQuery();
            }
        }
        private Guid ПолучитьAccountId(string manproc_id, Денежные_срества.ТипУчетнойГруппы тип)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT account_id FROM Account WHERE manproc_id=@manproc_id AND account_name=@account_name";
                cmd.Parameters.AddWithValue("manproc_id", new Guid(manproc_id));
                switch (тип)
                {
                    case Денежные_срества.ТипУчетнойГруппы.средстваДолжника:
                        cmd.Parameters.AddWithValue("account_name", "Основной");
                        break;
                    case Денежные_срества.ТипУчетнойГруппы.средстваАУ:
                        cmd.Parameters.AddWithValue("account_name", "Собственные средства АУ");
                        break;
                    case Денежные_срества.ТипУчетнойГруппы.залог:
                        cmd.Parameters.AddWithValue("account_name", "Залог");
                        break;
                    case Денежные_срества.ТипУчетнойГруппы.задаток:
                        cmd.Parameters.AddWithValue("account_name", "Задаток");
                        break;
                    default:
                        throw new Exception();
                }
                cmd.Parameters.AddWithValue("account_name", "");
                return (Guid)cmd.ExecuteScalar();
            }
        }

        public void ЗарегистрироватьНачальноеСальдо(string manproc_id, Денежные_срества.Учётная_группа группа)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = @"INSERT INTO AccountTransaction 
                    (transaction_id, date_created, transaction_date, amount, payment_basing, payment_date, transaction_type, account_id) 
                    VALUES (?,?,?,?,?,?,?,?)";
                cmd.Parameters.AddWithValue("transaction_id", Guid.NewGuid());
                cmd.Parameters.AddWithValue("date_created", группа.Дата_начального_сальдо);
                cmd.Parameters.AddWithValue("transaction_date", группа.Дата_начального_сальдо);
                cmd.Parameters.AddWithValue("amount", группа.Начальное_сальдо);
                cmd.Parameters.AddWithValue("payment_basing", "");
                cmd.Parameters.AddWithValue("payment_date", группа.Дата_начального_сальдо);
                cmd.Parameters.AddWithValue("transaction_type", 5);
                cmd.Parameters.AddWithValue("account_id", ПолучитьAccountId(manproc_id, группа.учётная_группа));
                cmd.ExecuteNonQuery();
            }
        }

        public void ПропуститьЗадачу(IЗадача задача)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT TOP 1 manproc_id FROM ManagerProcedure ";
                var manproc_id = (Guid)cmd.ExecuteScalar();
                OleDbParameterCollection p = cmd.Parameters;
                cmd.CommandText = "INSERT INTO Task (manproc_id, json_task, DateResolve,TaskResult, id_Task) VALUES (?,?,?,1,?)";

                p.AddWithValue("manproc_id",    new Guid(задача.ПроцедураID));
                p.AddWithValue("json_task",     (задача.Формулировка == "Запрос в банк") ?
                                                "Уведомить известных кредиторов о реализации имущества" : задача.Формулировка);
                p.AddWithValue("DateResolve",   timeProvider.Now);
                p.AddWithValue("id_Task",       Guid.NewGuid());
                cmd.ExecuteNonQuery();
            }
        }

        public List<Процедура> ВсеПроцедуры()
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                List<Процедура> procedures = new List<Процедура>();

                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                cmd.CommandText = "SELECT MP.manproc_id, MP.procedure_start, MP.procedure_finish, " +
                    "PL.shortname, " +
                    "D.name, D.inn, MP.closed, MP.deptor_id " +
                    "FROM (ManagerProcedure AS MP " +
                    "INNER JOIN ProcedureList as PL ON MP.procedure_id = PL.procedure_id) " +
                    "INNER JOIN Deptor as D ON MP.deptor_id = D.deptor_id";

                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (!reader.GetBoolean(6))
                        {
                            Процедура proc = new Процедура();
                            proc.ID = reader.GetGuid(0).ToString("D");
                            proc.Дата_начала = reader.GetDateTime(1);
                            proc.Дата_окончания = reader.GetDateTime(2);
                            string ss = reader.GetString(3);
                            switch (reader.GetString(3))
                            {
                                case "Н":
                                    proc.типПроцедуры = Robot.Процедура.ТипПроцедуры.Н;
                                    proc.тип.Кратко = "Н";
                                    proc.тип.Полностью = "Наблюдение";
                                    break;
                                case "КП":
                                    proc.тип.Кратко = "КП";
                                    proc.тип.Полностью = "Конкурсное производство";
                                    proc.типПроцедуры = Robot.Процедура.ТипПроцедуры.КП;
                                    break;
                                case "РД":
                                    proc.тип.Кратко = "РД";
                                    proc.тип.Полностью = "Реструктуризация долгов";
                                    proc.типПроцедуры = Robot.Процедура.ТипПроцедуры.РД;
                                    break;
                                case "РИ":
                                    proc.тип.Кратко = "РИ";
                                    proc.тип.Полностью = "Реализация имущества";
                                    proc.типПроцедуры = Robot.Процедура.ТипПроцедуры.РИ;
                                    break;
                                default:
                                    continue;
                            }
                            proc.Должник.Наименование = reader.GetString(4);
                            proc.Должник.ИНН = reader.GetString(5);
                            proc.Должник.Id = reader.GetGuid(7).ToString();
                            procedures.Add(proc);
                        }
                    }
                }
                return procedures;
            }

            
        }

        public Процедура Процедура(string ID)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                Процедура proc = new Процедура();
                cmd.CommandText = "SELECT MP.manproc_id, MP.procedure_start, MP.procedure_finish, " +
                    "PL.shortname, " +
                    "D.name, D.inn, D.deptor_id " +
                    "FROM (ManagerProcedure AS MP " +
                    "INNER JOIN ProcedureList as PL ON MP.procedure_id = PL.procedure_id) " +
                    "INNER JOIN Deptor as D ON MP.deptor_id = D.deptor_id " +
                    "WHERE MP.manproc_id=@manproc_id";

                cmd.Parameters.AddWithValue("@manproc_id",new Guid(ID));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        proc.ID = reader.GetGuid(0).ToString("D");
                        proc.Дата_начала = reader.GetDateTime(1);
                        proc.Дата_окончания = reader.GetDateTime(2);
                        switch (reader.GetString(3))
                        {
                            case "Н":
                                proc.типПроцедуры = Robot.Процедура.ТипПроцедуры.Н;
                                proc.тип.Кратко = "Н";
                                proc.тип.Полностью = "Наблюдение";
                                break;
                            case "КП":
                                proc.тип.Кратко = "КП";
                                proc.тип.Полностью = "Конкурсное производство";
                                proc.типПроцедуры = Robot.Процедура.ТипПроцедуры.КП;
                                break;
                            case "РД":
                                proc.тип.Кратко = "РД";
                                proc.тип.Полностью = "Реструктуризация долгов";
                                proc.типПроцедуры = Robot.Процедура.ТипПроцедуры.РД;
                                break;
                            case "РИ":
                                proc.тип.Кратко = "РИ";
                                proc.тип.Полностью = "Реализация имущества";
                                proc.типПроцедуры = Robot.Процедура.ТипПроцедуры.РИ;
                                break;
                            default:
                                throw new NotImplementedException();
                        }
                        proc.Должник.Наименование = reader.GetString(4);
                        proc.Должник.ИНН = reader.GetString(5);
                        proc.Должник.Id = reader.GetGuid(6).ToString();
                    }
                }
                return proc;
            }
        }

        public Исходящее Исходящее(string ID)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                Исходящее outcoming = new Исходящее();
                cmd.CommandText = "SELECT manproc_id, send_date, sender, type_request " +
                    "FROM MailOut " +
                    "WHERE mail_id=@mail_id";

                cmd.Parameters.AddWithValue("@mail_id", new Guid(ID));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        outcoming.ID = ID;
                        outcoming.ПроцедураID = reader.GetGuid(0).ToString();
                        outcoming.ДатаОтправки = reader.GetDateTime(1);
                        if (!reader.IsDBNull(2)) outcoming.Получатель = reader.GetString(2);
                        outcoming.Тип = тип_шаблона_исходящего(reader.GetInt16(3));
                    }
                }
                return outcoming;
            }
        }

        public IEnumerable<Исходящее> ВсеУведомления(string procedure, string type)
        {
            using (var cmd = connection.CreateCommand())
            {
                List<Исходящее> outcomingList = new List<Robot.Исходящее>();
                cmd.CommandText = @"SELECT C.lastname, MO.send_date FROM ((CreditorClaim AS CC
                INNER JOIN CreditorRegistry AS CR ON CR.registry_id=CC.registry_id)
                INNER JOIN Contact AS C ON CR.contact_id = C.contact_id)
                INNER JOIN MailOut AS MO ON MO.sender = c.lastname 
                WHERE CR.manproc_id=@manproc_id";
                cmd.Parameters.AddWithValue("manproc_id", new Guid(procedure));

                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Исходящее outcoming = new Исходящее();
                        outcoming.Получатель = reader.GetString(0);
                        outcoming.ПроцедураID = procedure;
                        outcoming.Тип = type;
                        outcoming.ДатаОтправки = reader.GetDateTime(1);
                        outcomingList.Add(outcoming);
                    }
                }
                return outcomingList;
            }
        }

        public IEnumerable<Исходящее> ВсеИсходящие(string manproc_id, string type)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                List<Исходящее> mailOut = new List<Исходящее>();
                cmd.CommandText = "SELECT mail_id, send_date, sender, type_request  FROM MailOut WHERE manproc_id=@manproc_id";

                cmd.Parameters.AddWithValue("manproc_id", new Guid(manproc_id));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Исходящее outcoming = new Исходящее();
                        outcoming.ПроцедураID = manproc_id;
                        outcoming.ID = reader.GetGuid(0).ToString("D");
                        outcoming.ДатаОтправки = reader.GetDateTime(1);
                        outcoming.Получатель = (!reader.IsDBNull(2) ? reader.GetString(2) : null);
                        if (!reader.IsDBNull(3))
                            outcoming.Тип = тип_шаблона_исходящего(reader.GetInt16(3));
                        outcoming.ПроцедураID = manproc_id;
                        mailOut.Add(outcoming);
                    }
                }
                return mailOut.Where(m => m.ПроцедураID == manproc_id
                                      && (string.IsNullOrEmpty(type) || type == m.Тип));
            }
        }

        public Ответ ОтветНа(string requestID)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                Ответ answer = null;
                cmd.CommandText = "SELECT mail_id, manproc_id, sender, receipt_date FROM MailIn WHERE reply_id=@reply_id";
                cmd.Parameters.AddWithValue("reply_id", new Guid(requestID));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        answer = new Ответ();
                        answer.ID = requestID;
                        answer.ПроцедураID = reader.GetGuid(1).ToString();
                        answer.Отправитель = reader.GetString(2);
                        answer.ДатаПолучения = reader.GetDateTime(3);
                    }
                }
                return answer;
            }
        }
        public IEnumerable<Ответ> ВсеВходящие(string procedureID, string type = null)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                List<Ответ> mailIn = new List<Ответ>();
                cmd.CommandText = "SELECT mail_id, receipt_date, sender, reply_id, type_reply FROM MailIn WHERE manproc_id=@manproc_id";
                cmd.Parameters.AddWithValue("manproc_id",new Guid(procedureID));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int i = 0;
                        Ответ incoming = new Ответ();
                        incoming.ПроцедураID = procedureID;
                        incoming.ID = reader.GetGuid(i++).ToString();
                        incoming.ДатаПолучения = reader.GetDateTime(i++);
                        if (!reader.IsDBNull(i++)) incoming.Отправитель = reader.GetString(2);
                        if (!reader.IsDBNull(i++)) incoming.ЗапросID = reader.GetGuid(3).ToString("D");
                        incoming.Тип = тип_шаблона_исходящего(reader.GetInt16(i++));
                        if (null == type || type == incoming.Тип)
                            mailIn.Add(incoming);
                    }
                }
                return mailIn;
            }
        }

        public IEnumerable<Публикация> ВсеПубликации(string procedureID)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                List<Публикация> publicationList = new List<Публикация>();
                cmd.CommandText = @"SELECT MP.publication_date, C.lastname, MP.contact_id
                    FROM ManagerProcedure AS MP
                    INNER JOIN Contact as C ON MP.contact_id = C.contact_id
                    WHERE MP.manproc_id=@manproc_id AND MP.publication_date <> null";
                cmd.Parameters.AddWithValue("@manproc_id",new Guid(procedureID));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int i = 0;
                        Публикация pub = new Публикация();
                        pub.Дата = reader.GetDateTime(i++);
                        pub.СМИ = reader.GetString(i++);
                        pub.ID = reader.GetGuid(i++).ToString();
                        pub.ПроцедураID = procedureID;
                        publicationList.Add(pub);
                    }
                }
                return publicationList;
            }
        }

       
        public Кредитор Кредитор(string ID)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                Кредитор кредитор = new Кредитор();

                cmd.CommandText = @"SELECT C.lastname, CR.account FROM CreditorRegistry AS CR
                INNER JOIN Contact AS C ON CR.contact_id = C.contact_id
                WHERE CR.registry_id=@registry_id";
                cmd.Parameters.AddWithValue("registry_id", new Guid(ID));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        кредитор.ID = ID;
                        кредитор.Наименование = (!reader.IsDBNull(0) ? reader.GetString(0) : null);
                        кредитор.ИНН = (!reader.IsDBNull(1) ? reader.GetString(1) : null);
                    }
                }
                return кредитор;
            }
        }

        public Требование Требование(string id)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                Требование требование = new Требование();

                cmd.CommandText = "SELECT claim_id, include, date_in, claim_date, registry_id, claim_sum FROM CreditorClaim WHERE claim_id=@claim_id";
                cmd.Parameters.AddWithValue("claim_id", new Guid(id));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        int i = 0;
                        требование.ID = reader.GetGuid(i++).ToString("D");
                        требование.Включено = reader.GetBoolean(i++);
                        требование.ДатаВключения = reader.GetDateTime(i++);
                        требование.ДатаПолучения = reader.GetDateTime(i++);
                        требование.КредиторID = reader.GetGuid(i++).ToString("D");
                        требование.Сумма = reader.GetDecimal(i++);
                    }
                }
                return требование;
            }
        }

        public IEnumerable<Требование> ВсеТребования(string procedureID, bool? included = null)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                List<Требование> creditorClaimList = new List<Требование>();
                cmd.CommandText = @"SELECT
                CC.claim_id, CC.include, CC.date_in, CC.claim_date, CR.registry_id, CC.claim_sum, CC.Commitment 
                FROM CreditorClaim AS CC 
                INNER JOIN CreditorRegistry AS CR ON CC.registry_id=CR.registry_id 
                WHERE CR.manproc_id=@manproc_id";

                cmd.Parameters.AddWithValue("manproc_id", new Guid(procedureID));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Требование creditorClaim = new Требование();
                        if (reader.GetBoolean(1) == included && !(!reader.IsDBNull(6) ? reader.GetString(6).StartsWith("Вознаграждение") : false))
                        {
                            creditorClaim.ID = reader.GetGuid(0).ToString("D");
                            creditorClaim.Включено = reader.GetBoolean(1);
                            if (!reader.IsDBNull(2))
                            {
                                creditorClaim.ДатаВключения = reader.GetDateTime(2);
                            }
                            if (!reader.IsDBNull(3))
                            {
                                creditorClaim.ДатаПолучения = reader.GetDateTime(3);
                            }
                            creditorClaim.КредиторID = reader.GetGuid(4).ToString();
                            creditorClaim.Сумма = reader.GetDecimal(5);
                            creditorClaim.ПроцедураID = procedureID;
                            creditorClaimList.Add(creditorClaim);
                        }
                    }
                }
                return creditorClaimList;
            }
        }

        public void IncludeClaim(string id)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "UPDATE CreditorClaim SET include=true WHERE claim_id=@claim_id";
                cmd.Parameters.AddWithValue("claim_id", new Guid(id));
                cmd.ExecuteNonQuery();
            }
        }

        public void ПровестиСобрание(Собрание собрание)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                string ss = собрание.ПроцедураID;
                cmd.CommandText = "UPDATE Assembly SET done = true WHERE assembly_id=@assembly_id";
                cmd.Parameters.AddWithValue("assembly_id", new Guid(собрание.ID));
                cmd.ExecuteNonQuery();
                собрание.Проведено = true;
            }
        }

        public IEnumerable<Собрание> ВсеСобрания(string procedureID)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                List<Собрание> assemblyList = new List<Собрание>();
                cmd.CommandText = "SELECT assembly_id, type, manproc_id, assembly_date, done, incomplete, inspection_address, msgInFedresursDate FROM Assembly WHERE manproc_id=@manproc_id";
                cmd.Parameters.AddWithValue("manproc_id", new Guid(procedureID));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Собрание assembly = new Собрание();
                        assembly.ID = reader.GetGuid(0).ToString();
                        assembly.Дата = reader.GetDateTime(3);
                        if (!reader.IsDBNull(7))
                        {
                            assembly.ДатаНазначения = reader.GetDateTime(7);
                        }
                        assembly.Проведено = reader.GetBoolean(4);
                        assembly.ПроцедураID = reader.GetGuid(2).ToString();
                        assemblyList.Add(assembly);
                    }
                }
                return assemblyList;
            }
        }

        public Собрание ПоследнееСобрание(string procedureID)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                Собрание meeting = null;
                cmd.CommandText = "SELECT assembly_id, type, manproc_id, assembly_date, done, incomplete, inspection_address,msgInFedresursDate FROM Assembly WHERE manproc_id=@manproc_id";
                cmd.Parameters.AddWithValue("manproc_id", new Guid(procedureID));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        meeting = new Собрание();
                        meeting.ID = reader.GetGuid(0).ToString();
                        meeting.Дата = reader.GetDateTime(3);
                        meeting.Проведено = reader.GetBoolean(4);
                        meeting.ДатаНазначения = reader.GetDateTime(3);
                    }
                }
                return meeting;
            }
        }

        public IEnumerable<Счет> ВсеСчета(string procedureID)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                List<Счет> accountList = new List<Счет>();
                cmd.CommandText = @"SELECT C.contact_id, C.lastname, C.middlename FROM (Contact AS C 
                INNER JOIN DeptorAccount AS DA ON C.contact_id=DA.account_contact_id)
                INNER JOIN ManagerProcedure AS MR ON MR.deptor_id=DA.deptor_id
                WHERE MR.manproc_id=@manproc_id";

                cmd.Parameters.AddWithValue("manproc_id", new Guid(procedureID));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Счет account = new Счет();
                        account.ID = reader.GetGuid(0).ToString();
                        account.ПроцедураID = procedureID;
                        account.Банк = reader.GetString(1);
                        account.Счёт = reader.GetString(2);
                        accountList.Add(account);
                    }
                }
                return accountList;
            }
        }

        public IEnumerable<Торги> ВсеТорги(string procedureID)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                List<Торги> saleList = new List<Торги>();
                cmd.CommandText = "SELECT procedure_start, declaration_date, closed FROM ManagerProcedure WHERE manproc_id=@manproc_id";
                cmd.Parameters.AddWithValue("manproc_id", new Guid(procedureID));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Торги sale = new Торги();
                        int i = 0;
                        sale.ID = Guid.NewGuid().ToString();
                        sale.ПроцедураID = procedureID;
                        sale.ДатаОткрытия = reader.GetDateTime(i++);
                        if (!reader.IsDBNull(i++))
                        {
                            sale.ДатаЗавершения = reader.GetDateTime(1);
                        }
                        sale.Завершен = reader.GetBoolean(i++);
                        saleList.Add(sale);
                    }
                }
                return saleList;
            }
        }

        public IEnumerable<Анализ> ВсеАнализы(string procedureID)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                List<Анализ> analizList = new List<Анализ>();
                cmd.CommandText = "SELECT finanaliz_id, date_created FROM Finanaliz WHERE manproc_id=@manproc_id";
                cmd.Parameters.AddWithValue("manproc_id", new Guid(procedureID));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Анализ analiz = new Анализ();
                        int i = 0;
                        analiz.ID = reader.GetGuid(i++).ToString();
                        analiz.Дата = reader.GetDateTime(i++);
                        analiz.ПроцедураID = procedureID;
                        analizList.Add(analiz);
                    }
                }
                return analizList;
            }
        }

        public IEnumerable<Инвентаризация> ВсеИнвентаризации(string procedureID)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                List<Инвентаризация> invenotryList = new List<Инвентаризация>();
                cmd.CommandText = "SELECT inventory_data, estimate_data FROM ManagerProcedure WHERE manproc_id=@manproc_id";
                cmd.Parameters.AddWithValue("manproc_id", new Guid(procedureID));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                        {
                            Инвентаризация ivent = new Инвентаризация();
                            ivent.ID = Guid.NewGuid().ToString();
                            string inventory_date = !reader.IsDBNull(0) ? reader.GetString(0) : "";
                            string estimate_date = !reader.IsDBNull(1) ? reader.GetString(1) : "";
                            string[][] inv = { inventory_date.Split(' '), estimate_date.Split(' ') };
                            if (inv.Count() > 0)
                            {
                                foreach (string i in inv[0])
                                {
                                    DateTime inv_date;
                                    if (DateTime.TryParse(i, out inv_date))
                                    {
                                        ivent.Дата = Convert.ToDateTime(i);
                                        break;
                                    }
                                }

                                foreach (string i in inv[1])
                                {
                                    DateTime est_date;
                                    if (DateTime.TryParse(i, out est_date))
                                    {
                                        ivent.ДатаОценки = Convert.ToDateTime(i);
                                        break;
                                    }
                                }
                            }
                            ivent.ПроцедураID = procedureID;
                            invenotryList.Add(ivent);
                        }
                    }
                }
                return invenotryList;
            }
        }

        public void Восстановить_задачу() 
        {
            using(OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT TOP 1 id_Task FROM Task";
                Guid id_task = (Guid)cmd.ExecuteScalar();
                cmd.CommandText = "DELETE FROM Task Where id_task=@id_task";
                cmd.Parameters.AddWithValue("id_task", id_task);
                cmd.ExecuteNonQuery();
            }
        }

        public void Зарегистрировать_решение(IЗадача задача, string TaskResult)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
				cmd.CommandText = @"INSERT INTO Appointments (appointment_id, manproc_id, appointment_date, short_description, done, type)
										VALUES (?,?,?,?,?,?)";
				OleDbParameterCollection p = cmd.Parameters;
				p.AddWithValue("appointment_id", Guid.NewGuid());
				p.AddWithValue("manproc_id", new Guid(задача.ПроцедураID));
				p.AddWithValue("appointment_date", timeProvider.Now.ToShortDateString());
				p.AddWithValue("short_description", (задача.Формулировка == "Запрос в банк") ?
												"Уведомить известных кредиторов о реализации имущества" : задача.Формулировка);
				p.AddWithValue("done", true);
				p.AddWithValue("type", Int16.Parse(TaskResult));
				cmd.ExecuteNonQuery();
			}
		}

        public IЗадача Задача_решена(IЗадача задача)
        {
            return ВсеРешенныеЗадачи(задача.ПроцедураID).SingleOrDefault(t => t.Формулировка == задача.Формулировка);
        }

        public List<IЗадача> ВсеРешенныеЗадачи(string procedureID)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                List<IЗадача> tasksList = new List<IЗадача>();
                cmd.CommandText = "SELECT short_description, appointment_date FROM Appointments WHERE type = 2 AND manproc_id=@manproc_id";
                cmd.Parameters.AddWithValue("manproc_id", new Guid(procedureID));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        TasksDB задача = new TasksDB();
                        задача.Формулировка = reader.GetString(0);
                        задача.Статус = task.Статус.Завершена;
                        if (!DBNull.Value.Equals(reader.GetDateTime(1)))
                        {
                            задача.ДатаРешения = reader.GetDateTime(1);
                        }
                        tasksList.Add(задача);
                    }
                }
                return tasksList;
            }
        }

        public List<IЗадача> ВсеПропущенныеЗадачи(string procedureID)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                List<IЗадача> tasksList = new List<IЗадача>();
                cmd.CommandText = "SELECT short_description, appointment_date FROM Appointments WHERE type = 1 AND manproc_id=@manproc_id";
                cmd.Parameters.AddWithValue("manproc_id", new Guid(procedureID));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        TasksDB задача = new TasksDB();
                        задача.Статус = task.Статус.Проигнорирована;
                        задача.Формулировка = reader.GetString(0);
                        if (!DBNull.Value.Equals(reader.GetDateTime(1)))
                        {
                            задача.ДатаРешения = reader.GetDateTime(1);
                        }
                        tasksList.Add(задача);
                    }
                }
                return tasksList;
            }
        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    connection.Close();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

        

        private Guid ПолучитьIdДолжника(Процедура proc)
        {
            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT deptor_id FROM Deptor WHERE inn=@inn";
                cmd.Parameters.AddWithValue("@inn", proc.Должник.ИНН);
                var guidObj = cmd.ExecuteScalar();
                if (guidObj != null)
                    return (Guid)guidObj;
            }

            using (var cmd = connection.CreateCommand())
            {
                var guid = Guid.NewGuid();
                cmd.CommandText = "INSERT INTO Deptor (deptor_id, fullname, name, inn, manager_type) VALUES (?,?,?,?,1)";
                cmd.Parameters.AddWithValue("deptor_id",    proc.Должник.Id);
                cmd.Parameters.AddWithValue("fullname",     proc.Должник.Наименование);
                cmd.Parameters.AddWithValue("name",         proc.Должник.Наименование);
                cmd.Parameters.AddWithValue("inn",          proc.Должник.ИНН);

                using (OleDbTransaction transaction = connection.BeginTransaction())
                {
                    cmd.Transaction = transaction;
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                }
                return new Guid(proc.Должник.Id);
            }
        }

        

        public string ЗарегистрироватьПроцедуру(Процедура proc)
        {
            Guid manager_id = Guid.NewGuid();
            Guid contact_id = Guid.NewGuid();
            Guid procedure_id = Guid.NewGuid();

            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT procedure_id FROM ProcedureList WHERE shortname=@shortname";
                cmd.Parameters.AddWithValue("@shortname", proc.тип.Кратко);
                procedure_id = (Guid)cmd.ExecuteScalar();
            }

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT TOP 1 contact_id FROM Contact";
                contact_id = (Guid)cmd.ExecuteScalar();
            }

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO Manager (manager_id, firstname, lastname, middlename, sex) VALUES(?,?,?,?,?)";
                cmd.Parameters.AddWithValue("manager_id", Guid.NewGuid());
                cmd.Parameters.AddWithValue("firstname", "Иван");
                cmd.Parameters.AddWithValue("lastname", "Иванов");
                cmd.Parameters.AddWithValue("middlename", "Иванович");
                cmd.Parameters.AddWithValue("sex", false);
                cmd.ExecuteNonQuery();
            }

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT TOP 1 manager_id FROM Manager";
                manager_id = (Guid)cmd.ExecuteScalar();
            }

            using (OleDbCommand cmd = connection.CreateCommand())
            {
                OleDbParameterCollection p = cmd.Parameters;
                Guid procGuid = guid.New;
                proc.ID = procGuid.ToString();
                cmd.CommandText = "INSERT INTO ManagerProcedure " +
                    "(manager_id, procedure_id, deptor_id, " +
                    "procedure_start, procedure_finish, assigned_manager_date, " +
                    "contact_id, closed, manproc_id, newspaper_contact_id,declaration_date) " +
                    "VALUES (?,?,?, ?,?,?, ?,?,?,?,?)";

                p.AddWithValue("manager_id", manager_id);
                p.AddWithValue("procedure_id", procedure_id);
                p.AddWithValue("deptor_id", ПолучитьIdДолжника(proc));
                p.AddWithValue("procedure_start", proc.Дата_начала);
                p.AddWithValue("procedure_finish", proc.Дата_окончания);
                p.AddWithValue("assigned_manager_date", proc.Дата_начала);
                p.AddWithValue("contact_id", contact_id);
                p.AddWithValue("closed", false);
                p.AddWithValue("manproc_id", proc.ID);
                p.AddWithValue("newspaper_contact_id", contact_id);
                p.AddWithValue("declaration_date", proc.Дата_окончания);

                using (OleDbTransaction transaction = connection.BeginTransaction())
                {
                    cmd.Transaction = transaction;
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                }
                
            }

            string[] учетныеГруппы =
            {
                "Основной","Собственные средства АУ","Залог","Задаток"
            };
            for (int i = 0; i < 4; i++)
            {
                using (OleDbCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO Account (account_id,manproc_id,account_name,inreport,is_main) VALUES (?,?,?,?,?)";
                    cmd.Parameters.AddWithValue("account_id", Guid.NewGuid());
                    cmd.Parameters.AddWithValue("manproc_id", proc.ID);
                    cmd.Parameters.AddWithValue("account_name", учетныеГруппы[i]);
                    cmd.Parameters.AddWithValue("inreport", 0);
                    cmd.Parameters.AddWithValue("is_main", 0);
                    cmd.ExecuteNonQuery();
                }
            }

            return proc.ID;
        }

        public string ЗарегистрироватьИсходящее(Исходящее msg)
        {
            long nextId = 0;
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                OleDbParameterCollection p = cmd.Parameters;

                cmd.CommandText = "SELECT COUNT(*) FROM MailOut " +
                    "WHERE manproc_id=@manproc_id AND is_list=false";
                p.AddWithValue("@manproc_id", msg.ПроцедураID);
                nextId = (int)cmd.ExecuteScalar() + 1;
                if(msg.ID == null)
                    msg.ID = guid.New.ToString();
            }

            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO MailOut " +
                    "(mail_id, manproc_id, mail_number, sender, send_date, is_list, is_return, type_request) " +
                    "VALUES (?,?,?,?,?,0,0,?)";
                OleDbParameterCollection p = cmd.Parameters;

                p.AddWithValue("mail_id",       msg.ID);
                p.AddWithValue("manproc_id",    msg.ПроцедураID);
                p.AddWithValue("mail_number",   nextId.ToString());
                p.AddWithValue("sender",        msg.Получатель);
                p.AddWithValue("send_date",     msg.ДатаОтправки);
                p.AddWithValue("type_request",  type_request(msg.Тип));

                using (OleDbTransaction transaction = connection.BeginTransaction())
                {
                    cmd.Transaction = transaction;
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                }
                return msg.ID;
            }
        }

        public string ЗарегистрироватьВходящее(Ответ ответ)
        {
            long nextId = 0;
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                OleDbParameterCollection p = cmd.Parameters;

                cmd.CommandText = "SELECT COUNT(*) FROM MailIn " +
                    "WHERE manproc_id=@manproc_id";

                p.AddWithValue("@manproc_id", OleDbType.Guid).Value = ответ.ПроцедураID;
                nextId = (int)cmd.ExecuteScalar() + 1;

                if(ответ.ID == null)
                    ответ.ID = guid.New.ToString();
            }

            using (OleDbCommand cmd = connection.CreateCommand())
            {
                OleDbParameterCollection p = cmd.Parameters;

                cmd.CommandText = "INSERT INTO MailIn " +
                    "(mail_id, manproc_id, mail_number, sender, receipt_date, reply_id, type_reply) " +
                    "VALUES (?,?,?,?,?,?,?)";
                p.AddWithValue("mail_id",       ответ.ID);
                p.AddWithValue("manproc_id",    ответ.ПроцедураID);
                p.AddWithValue("mail_number",   nextId.ToString());
                p.AddWithValue("sender",        ответ.Отправитель);
                p.AddWithValue("receipt_date",  ответ.ДатаПолучения);

                var replyParam = p.Add("reply_id", OleDbType.Guid, 0, "reply_id");
                var typeParam = p.Add("type_reply", OleDbType.SmallInt, 0, "type_reply");
                if (!string.IsNullOrEmpty(ответ.ЗапросID))
                {
                    Исходящее outcoming = Исходящее(ответ.ЗапросID);
                    replyParam.Value = new Guid(outcoming.ID);
                    typeParam.Value = type_request(outcoming.Тип);
                }
                else
                {
                    replyParam.Value = Guid.Empty;
                    typeParam.Value = type_request(ответ.Тип);
                }

                using (OleDbTransaction transaction = connection.BeginTransaction())
                {
                    cmd.Transaction = transaction;
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                }
                return nextId.ToString();
            }
        }

        public string ЗарегистрироватьПубликацию(Публикация pub)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "UPDATE ManagerProcedure " +
                    "SET newspaper_contact_id=?, publication_date=? ";
                cmd.Parameters.Add("newspaper_contact_id", OleDbType.Guid, 0, "newspaper_contact_id").Value = new Guid("{00000095-0000-0000-0000-000000000000}"); // For tests: always Commersant
                cmd.Parameters.Add("publication_date", OleDbType.Date, 0, "publication_date").Value = pub.Дата;
                using (OleDbTransaction transaction = connection.BeginTransaction())
                {
                    cmd.Transaction = transaction;
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                }

                return "1";
            }
        }
        public string ЗарегистрироватьКредитора(Кредитор creditor)
        {
            var ID = guid.New;

            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO Contact (contact_id, lastname, middlename,type_id,sex,isAccredited,bank_is_structured) VALUES (?,?,?,?,0,0,0)";
                cmd.Parameters.AddWithValue("contact_id",ID);
                cmd.Parameters.AddWithValue("lastname", creditor.Наименование);
                cmd.Parameters.AddWithValue("middlename", creditor.ИНН);
                cmd.Parameters.AddWithValue("type_id", new Guid("00000006-0000-0000-0000-000000000000"));
                cmd.ExecuteNonQuery();
            }

            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO CreditorRegistry (registry_id, contact_id, queue, manproc_id) VALUES (?,?,3,?)";
                cmd.Parameters.AddWithValue("registry_id", Guid.NewGuid());
                cmd.Parameters.AddWithValue("contact_id", ID);
                cmd.Parameters.AddWithValue("manproc_id", creditor.процедураId);
                cmd.ExecuteNonQuery();
            }

            creditor.ID = ID.ToString();
            return creditor.ID;
        }

        private Guid getContactId(string contact_id)
        {
            using(OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT registry_id FROM CreditorRegistry WHERE contact_id=@contact_id";
                cmd.Parameters.AddWithValue("contact_id", new Guid(contact_id));
                return (Guid)cmd.ExecuteScalar();
            }
        }

        public string ЗарегистрироватьТребованиеКредитора(Требование claim)
        {
            Guid ID = guid.New;
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "UPDATE CreditorRegistry SET date_in=@date_in WHERE registry_id=@registry_id";
                cmd.Parameters.AddWithValue("date_in", claim.ДатаВключения);
                cmd.Parameters.AddWithValue("registry_id", new Guid(claim.КредиторID));
                cmd.ExecuteNonQuery();
            }

            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO CreditorClaim (claim_id, include, canvote, party_building, registry_id, date_in, claim_date, claim_sum)" +
                    " VALUES (?,?,?,?,?,?,?,?)";
                OleDbParameterCollection p = cmd.Parameters;
                p.AddWithValue("claim_id",          ID);
                p.AddWithValue("include",           claim.Включено);
                p.AddWithValue("canvote",           true);
                p.AddWithValue("party_building",    true);
                p.AddWithValue("registry_id",       getContactId(claim.КредиторID));
                p.AddWithValue("date_in",           claim.ДатаВключения);
                p.AddWithValue("claim_date",        claim.ДатаПолучения);
                p.AddWithValue("claim_sum",         claim.Сумма);
                cmd.ExecuteNonQuery();
            }

            claim.ID = ID.ToString("D");
            return claim.ID;
        }
        public void ОбновитьТребования(string id, decimal sum)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                OleDbParameterCollection p = cmd.Parameters;
                cmd.CommandText = "UPDATE CreditorClaim SET claim_sum=@claim_sum WHERE claim_id=@claim_id";
                p.AddWithValue("claim_sum", sum);
                p.AddWithValue("claim_id",  id);
                cmd.ExecuteNonQuery();
            }
        }
        public void ОбновитьТребования(string id, DateTime date)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "UPDATE CreditorClaim SET date_in=@date_in WHERE claim_id=@claim_id";
                cmd.Parameters.AddWithValue("date_in",  date);
                cmd.Parameters.AddWithValue("claim_id", id);
                cmd.ExecuteNonQuery();
            }
        }

        public string ЗарегистрироватьСобрание(Собрание meeting)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                Guid procGuid = guid.New;
                cmd.CommandText = "INSERT INTO Assembly (assembly_id, type, manproc_id, assembly_date, done, incomplete, inspection_address,msgInFedresursDate)" +
                    " VALUES (?,?,?,?,?,?,?,?)";
                OleDbParameterCollection p = cmd.Parameters;
                p.AddWithValue("assembly_id",       procGuid);
                p.AddWithValue("type",              0);
                p.AddWithValue("manproc_id",        new Guid(meeting.ПроцедураID));
                p.AddWithValue("assembly_date",     meeting.Дата);
                p.AddWithValue("done",              meeting.Проведено);
                p.AddWithValue("incomplete",        meeting.Проведено);
                p.AddWithValue("inspection_address", 0);
                p.AddWithValue("msgInFedresursDate", meeting.ДатаНазначения);
                cmd.ExecuteNonQuery();
                meeting.ID = procGuid.ToString();
                return procGuid.ToString();
            }
        }

        public string ЗарегистрироватьСчёт(Счет account)
        {
            Guid contactId = Guid.NewGuid();

            using (OleDbCommand cmd = connection.CreateCommand())
            {
                OleDbParameterCollection p = cmd.Parameters;
                cmd.CommandText = "INSERT INTO Contact (contact_id, lastname, middlename,type_id,sex,isAccredited,bank_is_structured) VALUES (?,?,?,?,0,0,0)";
                p.AddWithValue("contact_id",   contactId);
                p.AddWithValue("lastname",     account.Банк);
                p.AddWithValue("middlename",   account.Счёт);
                p.AddWithValue("type_id",      new Guid("00000006-0000-0000-0000-000000000000"));
                cmd.ExecuteNonQuery();
            }

            using(OleDbCommand cmd = connection.CreateCommand())
            {
                Процедура процедура = Процедура(account.ПроцедураID);

                OleDbParameterCollection p = cmd.Parameters;
                cmd.CommandText = "INSERT INTO DeptorAccount (account_contact_id, deptor_id, account, deptor_account_id) VALUES (?,?,?,?)";
                
                p.AddWithValue("account_contact_id",    contactId);
                p.AddWithValue("deptor_id",             ПолучитьIdДолжника(процедура));
                p.AddWithValue("account",               account.Счёт);
                p.AddWithValue("deptor_account_id",     Guid.NewGuid());
                cmd.ExecuteNonQuery();
            }

            return contactId.ToString();
        }

        public void ОбновитьСчёт(Счет account, decimal sum)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "UPDATE Account SET notes=@notes WHERE account_name = @account_name";
                cmd.Parameters.AddWithValue("notes",        sum);
                cmd.Parameters.AddWithValue("account_name", account.Счёт);
                cmd.ExecuteNonQuery();
            }
        }

        public string ЗарегистрироватьФинанализ(Анализ analysis)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                OleDbParameterCollection p = cmd.Parameters;
                cmd.CommandText = "INSERT INTO Finanaliz (finanaliz_id, manproc_id," +
                    " name, date_created, date_start, date_finish," +
                    " step, active,autocompute, refinancing_rate ) VALUES (?,?,?,?,?,?,?,?,?,?)";
                p.AddWithValue("finanaliz_id",      Guid.NewGuid());
                p.AddWithValue("manproc_id",        analysis.ПроцедураID);
                p.AddWithValue("name",              "Финансовый анализ");
                p.AddWithValue("date_created",      analysis.Дата);
                p.AddWithValue("date_start",        analysis.Дата);
                p.AddWithValue("date_finish",       analysis.Дата);
                p.AddWithValue("step",              0);
                p.AddWithValue("active",            0);
                p.AddWithValue("autocompute",       -1);
                p.AddWithValue("refinancing_rate", 0.13);
                cmd.ExecuteNonQuery();
                return "1";
            }
        }

        public string ЗарегистрироватьОценкуСчетов(Инвентаризация inv)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "UPDATE ManagerProcedure SET estimate_data=@estimate_data WHERE manproc_id=@manproc_id";

                cmd.Parameters.AddWithValue("estimaete_data",   inv.ДатаОценки);
                cmd.Parameters.AddWithValue("manproc_id",       new Guid(inv.ПроцедураID));
                cmd.ExecuteNonQuery();
            }
            return "1";
        }

        public string ЗарегистрироватьИнвентаризацию(Инвентаризация inv)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "UPDATE ManagerProcedure SET estimate_data=@estimate_data, inventory_data=@inventory_data WHERE manproc_id=@manproc_id";
                cmd.Parameters.AddWithValue("estimaete_data",   inv.ДатаОценки);
                cmd.Parameters.AddWithValue("inventory_data",   inv.Дата);
                cmd.Parameters.AddWithValue("manproc_id",       new Guid(inv.ПроцедураID));
                cmd.ExecuteNonQuery();
                return "1";
            }
        }
        public string ЗарегистрироватьТорги(Торги sale)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "UPDATE ManagerProcedure SET inventory_data=@inventory_data";
                cmd.Parameters.AddWithValue("inventory_data", sale.ДатаОткрытия);
                cmd.ExecuteNonQuery();

                return sale.ПроцедураID;
            }
        }

        public void ЗакрытьТорги(Торги sale)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "UPDATE ManagerProcedure SET declaration_date=@declaration_date";
                cmd.Parameters.AddWithValue("declaration_date", sale.ДатаЗавершения);
                cmd.ExecuteNonQuery();
            }
        }

        public int ПолучитьПоследнююРевизииюЕфрсбСообщения(string debtor_id)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT MAX(revision) From EfirMessages where debtor_id=?";
                cmd.Parameters.AddWithValue("debtor_id", debtor_id);
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                            return reader.GetInt32(0);
                    }
                }
                return 0;
            }
        }

        public void ДобавитьНовоеОбъявлениеЕФРСБ(string debtorId, Efrsb_message_record msg)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                OleDbParameterCollection p = cmd.Parameters;

                cmd.CommandText = "INSERT INTO EfirMessages (debtor_id,revision,body,publish_date,message_type,message_number) VALUES (?, ?, ?, ?, ?, ?)";

                p.AddWithValue("debtor_id",         debtorId);
                p.AddWithValue("revision",          msg.Revision);
                p.AddWithValue("body",              msg.Body);
                p.AddWithValue("publish_date",      msg.PublishDate.ToShortDateString());
                p.AddWithValue("message_type",      msg.MessageType);
                p.AddWithValue("message_number",    msg.MessageNumber);
                cmd.ExecuteNonQuery();
            }
        }

        public IEnumerable<Efrsb_message_record> ВсеЕфрсбСообщения(string debtorId)
        {

            using (OleDbCommand cmd = connection.CreateCommand())
            {
                List<Efrsb_message_record> messages = new List<Efrsb_message_record>();
                cmd.CommandText = @"SELECT debtor_id,revision,body,publish_date,message_type,message_number
                    FROM EfirMessages
                    WHERE debtor_id=?";
                cmd.Parameters.AddWithValue("debtor_id", new Guid(debtorId));
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int ifield = 0;
                        Efrsb_message_record msg = new Efrsb_message_record();
                        msg.DebtorId = reader.GetGuid(ifield++).ToString();
                        msg.Revision = reader.GetInt32(ifield++);
                        msg.Body = reader.GetString(ifield++);
                        msg.PublishDate = reader.GetDateTime(ifield++);
                        msg.MessageType = reader.GetString(ifield++);
                        msg.MessageNumber = reader.GetString(ifield++);
                        messages.Add(msg);
                    }
                }
                return messages;
            }
        }

        public void ЗарегистрироватьОсновнойСчет(Счет счет, string id_manproc) { }

        public Efrsb_message_record ПолучитьЕфрсбСообщение(string debtorId, string type)
        {
            using (OleDbCommand cmd = connection.CreateCommand())
            {
                Efrsb_message_record message = null;
                cmd.CommandText = "SELECT debtor_id,revision,body,publish_date,message_type,message_number" +
                    " From EfirMessages WHERE debtor_id = @debtor_id AND message_type = @message_type";
                cmd.Parameters.AddWithValue("debtor_id", new Guid(debtorId));
                cmd.Parameters.AddWithValue("message_type", type);
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    bool more = false;
                    while(reader.Read())
                    {
                        if(!more)
                        {
                            message = new Efrsb_message_record();
                            int index = 1;
                            message.DebtorId = debtorId;
                            message.Revision = reader.GetInt32(index++);
                            message.Body = reader.GetString(index++);
                            message.PublishDate = reader.GetDateTime(index++);
                            message.MessageType = reader.GetString(index++);
                            message.MessageNumber = reader.GetString(index++);
                            more = !more;
                        }
                        else
                        {
                            return null;
                        }
                        
                    }
                }
                return message;
            }
        }
    }
}