call :build-ama-robot
exit

rem -----------------------------------------------------------
:build-ama-robot
del                          built\ama-robot.js
node optimizers\r.js -o conf\build-ama-robot.js
if NOT 0==%ERRORLEVEL% exit
copy                         built\ama-robot.js ..\robot-gui-lib\phtml\js\
exit /B
