wait_text "Правильное оформление ajax запроса"
shot_check_png ..\..\shots\00new.png
click_text "Статический текст"
wait_text "форма со статическим текстом"
shot_check_png ..\..\shots\static-txt.png
click_text "OK"
shot_check_png ..\..\shots\00new.png
click_text "Статический шаблон"
wait_text "форма с результатом html шаблона"
shot_hide_class hide-on-test
shot_check_png ..\..\shots\static-tpl.png
click_text "OK"
shot_check_png ..\..\shots\00new.png
exit