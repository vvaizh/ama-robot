include ..\..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\..\fastened\simple\tests\cases\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "Добавить"
shot_check_png ..\..\..\..\editable\tests\shots\00new.png

click_text "Добавить"
shot_check_png ..\..\shots\01sav-add.png
je wbt.Model_selector_prefix= '*[array-index="0"] ';
play_stored_lines simple_fields_1

click_text "Добавить"
je wbt.Model_selector_prefix= '*[array-index="1"] ';
play_stored_lines simple_fields_2

shot_check_png ..\..\shots\01sav.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\..\..\simple\tests\contents\01edt-2.json.result.txt
exit