﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/test/collection/grid/local/e_test_grid.html'
	, 'forms/base/h_msgbox'
	, 'forms/test/fastened/simple/c_simple'
	, 'tpl!forms/test/collection/grid/local/v_test_grid_remove_confirm.html'
],
function (c_fastened, tpl, h_msgbox, c_simple, tpl_remove_confirm)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this,sel);
			this.RenderGrid();

			var self= this;
			$(sel + ' a.button.add').button().click(function(e){e.preventDefault();self.OnAdd();});
			$(sel + ' a.button.edit').button().click(function(e){e.preventDefault();self.OnEdit();});
			$(sel + ' a.button.remove	').button().click(function(e){e.preventDefault();self.OnDelete();});
		}

		controller.colModel =
		[
			  { label: 'Ученик', name: 'Ученик' }
			, { label: 'Алгебра ', name: 'алгебра' }
			, { label: 'Пение', name: 'пение' }
		];

		controller.PrepareGridRow= function(row,i)
		{
			return {
				  id: i
				, Ученик: row.Ученик
				, алгебра: row.Оценки.алгебра
				, пение: row.Оценки.пение
				, data: row
			};
		}

		controller.PrepareGridRows= function(rows)
		{
			var grid_rows= [];
			if (null!=rows)
			{
				for (var i= 0; i<rows.length; i++)
				{
					grid_rows.push(this.PrepareGridRow(rows[i],i));
				}
			}
			return grid_rows;
		}

		controller.RenderGrid= function()
		{
			var sel = this.fastening.selector;
			var self= this;

			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'local'
				, data: this.PrepareGridRows(this.fastening.model)
				, colModel: this.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Ученики {0} - {1} из {2}'
				, emptyText: 'Нет учеников для просмотра'
				, rownumbers: false
				, rowNum: 10
				, rowList: [5, 10, 15]
				, pager: '#crowbar-test-grid-pager'
				, viewrecords: true
				, height: 'auto'
				, width: '800'
				, multiselect: true
				, multiboxonly: true
				, ignoreCase: true
				, ondblClickRow: function () { self.OnEdit(); }
				, onSelectRow: function () { self.OnSelect(); }
				, onSelectAll: function () { self.OnSelect(); }
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnSelect = function ()
		{
			this.UpdateButtonsState();
		}

		controller.UpdateButtonsState = function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selected = grid.jqGrid('getGridParam', 'selarrrow').length;
			$(sel + ' a.button.edit').css('display', (1 == selected) ? 'inline-block' : 'none');
			$(sel + ' a.button.remove').css('display', (0 != selected) ? 'inline-block' : 'none');
		}

		controller.OnEdit= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var self= this;
			var selected = grid.jqGrid('getGridParam', 'selarrrow');
			if (1 == selected.length)
			{
				var edited_grid_row= grid.jqGrid('getLocalRow', selected[0]);
				var simple= c_simple();
				simple.SetFormContent(edited_grid_row.data);
				var btnOk= 'Сохранить';
				h_msgbox.ShowModal
				({
					  title:'Редактирование записи'
					, controller: simple
					, width:400, height:250
					, buttons:[btnOk, 'Отмена']
					, onclose: function(btn)
					{
						if (btn==btnOk)
						{
							if (!self.fastening.model)
								self.fastening.model = [];
							self.fastening.model[edited_grid_row.id] = simple.GetFormContent();
							self.ReloadGrid();
							$(sel).trigger('model_change');
						}
					}
				});
			}
		}

		controller.OnAdd= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var self= this;
			var simple= c_simple();
			simple.SetFormContent({});
			var btnOk= 'Сохранить';
			h_msgbox.ShowModal
			({
				  title:'Добавление записи'
				, controller: simple
				, width:400, height:250
				, buttons:[btnOk, 'Отмена']
				, onclose: function(btn)
				{
					if (btn==btnOk)
					{
						if (!self.fastening.model)
							self.fastening.model = [];
						self.fastening.model.push(simple.GetFormContent());
						self.ReloadGrid();
						$(sel).trigger('model_change');
					}
				}
			});
		}

		controller.ReloadGrid= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			grid.jqGrid('GridUnload');
			this.RenderGrid();
			this.UpdateButtonsState();
		}

		controller.OnDelete= function()
		{
			var self = this;
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selected= grid.jqGrid('getGridParam', 'selarrrow');
			if (0 != selected.length)
			{
				var to_delete = [];
				for (var i = 0; i < selected.length; i++)
					to_delete.push(grid.jqGrid('getLocalRow', selected[i]));
				h_msgbox.ShowModal
				({
					  html: tpl_remove_confirm(to_delete)
					, title: 'Подтверждение удаления'
					, width: '500'
					, buttons: ['Да', 'Нет']
					, onclose: function (bname)
					{
						if ('Да' == bname)
						{
							selected.sort();
							for (var i= selected.length-1; i>=0; i--)
								self.fastening.model.splice(selected[i], 1);
							self.ReloadGrid();
							$(sel).trigger('model_change');
						}
					}
				});
			}
		}

		var base_GetFormContent= controller.GetFormContent;
		controller.GetFormContent= function()
		{
			if (!this.fastening.model || null == this.fastening.model)
				this.fastening.model = [];
			var res = this.fastening.model;
			return res;
		}

		return controller;
	}
});