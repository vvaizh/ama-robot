include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "из списка"

shot_check_png ..\..\shots\03edt0.png

je $('div.cpw-collection [array-index="0"] .down').click();

shot_check_png ..\..\shots\03edt1.png

je $('div.cpw-collection [array-index="2"] .up').click();

shot_check_png ..\..\shots\03edt.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\03edt.json.result.txt
exit