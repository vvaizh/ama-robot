define
(
	[
		  'forms/base/collector'
		, 'txt!forms/test/core/min/tests/contents/01new.etalon.txt'
		, 'txt!forms/test/core/xml/tests/contents/01new.etalon.xml'

		, 'txt!forms/test/fastened/fields/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/test/fastened/simple/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/test/fastened/containers/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/test/fastened/a_modal/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/test/fastened/select2/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/test/fastened/typeahead/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/test/fastened/with/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/test/fastened/with_collection/tests/contents/test1.json.txt'
		, 'txt!forms/test/fastened/with_collection/tests/contents/01edt-2.json.etalon.txt'

		, 'txt!forms/test/collection/simple/tests/contents/test0.json.txt'
		, 'txt!forms/test/collection/simple/tests/contents/test1.json.txt'
		, 'txt!forms/test/collection/simple/tests/contents/test2.json.txt'
		, 'txt!forms/test/collection/simple/tests/contents/test3.json.txt'

		, 'txt!forms/test/complex/view/tests/contents/complex1.json.txt'

		, 'txt!forms/test/collection/simplest/tests/contents/test3.json.txt'
	],
	function (collect)
	{
		return collect([
		  'min-01new'
		, 'xml-01new'

		, 'fields-01sav'
		, 'simple-01sav'
		, 'containers-01sav'
		, 'a_modal-01sav'
		, 'select2-01sav'
		, 'typeahead-01sav'
		, 'with-01sav'
		, 'with-collection-test1'
		, 'with-collection-01sav'

		, 'collection-0'
		, 'collection-1'
		, 'collection-2'
		, 'collection-3'

		, 'complex-1'

		, 'simplest-collection-3'
		], Array.prototype.slice.call(arguments,1));
	}
);