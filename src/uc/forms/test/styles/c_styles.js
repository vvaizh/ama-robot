﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/test/styles/v_styles.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);
		return controller;
	}
});