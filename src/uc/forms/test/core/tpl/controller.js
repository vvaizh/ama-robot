define([
	  'forms/base/controller'
	, 'tpl!forms/test/core/tpl/view.html'
],
function (BaseFormController, index_tpl)
{
	return function ()
	{
		var edit_id_spec = '#test-test-form-edit';
		var res = BaseFormController();

		var RenderForm = function (sel, value)
		{
			var form_div = $(sel);
			var content = index_tpl(value);
			form_div.append(content);
			$(edit_id_spec).focus();
		}

		res.CreateNew = function (sel) { RenderForm(sel, ""); };

		res.Edit = function (sel) { RenderForm(sel, this.form_content); };

		res.GetFormContent = function () { return $(edit_id_spec).val(); };

		res.SetFormContent = function (form_content)
		{
			var isnum = /^\d+$/.test(form_content);
			if (!isnum)
			{
				return "Документ должен содержать только цифры!";
			}
			else
			{
				this.form_content = form_content;
				return null;
			}
		};

		res.Validate = function ()
		{
			var form_content = $(edit_id_spec).val();
			var isnum = /^\d+$/.test(form_content);
			return isnum ? null : "Документ должен содержать только цифры!";
		};

		return res;
	}
});
