﻿define(function () {
	var helper =
		{
			СобратьПолноеИмя: function (txt_json) {
				if (txt_json.фамилия && txt_json.имя) {
					var фамилия = txt_json.фамилия;
					var имя = txt_json.имя;
				}
				var отчество = !txt_json.отчество ? "" : txt_json.отчество;
				var fullname = фамилия + " " + имя + ". " + отчество;
				return fullname;
			}
		};
	return helper;
});