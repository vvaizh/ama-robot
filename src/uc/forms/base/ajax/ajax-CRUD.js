define([
	  'forms/base/codec/codec.copy'
	, 'forms/base/codec/url/codec.url'
	, 'forms/base/codec/url/codec.url.args'
]
, function (codec_copy, codec_url, codec_url_args)
{
	return function()
	{
		var transport = {}

		transport.rows_all = [];
		transport.options = {
			ParseUrl: function(url)
			{
				var decoded_url = codec_url().Decode(url);
				var args = codec_url_args().Decode(decoded_url);
				return !args.cmd ? false : args;
			}
		};

		var urldecode = function (str)
		{
			return decodeURIComponent((str + '').replace(/\+/g, '%20'));
		}

		transport.prepare_send_abort = function (options, originalOptions, jqXHR, parsed_args)
		{
			var self = this;
			var send_abort=
			{
				send: function (headers, completeCallback)
				{
					var rows_all = self.rows_all;
					if ('function' == typeof rows_all)
						rows_all = rows_all();
					switch (parsed_args.cmd)
					{
						case 'add':
							var data_decoded_as_url = codec_url().Decode(options.data);
							var data_decoded_as_args = codec_url_args().Decode(data_decoded_as_url);
							self.create(data_decoded_as_args);
							completeCallback(200, 'success', { text: '{ "ok": true }' });
							return;
						case 'get':
							var row = self.read(parsed_args.id);
							completeCallback(200, 'success', { text: JSON.stringify(row) });
							return;
						case 'update':
							var data_decoded_as_url = codec_url().Decode(options.data);
							var data_decoded_as_args = codec_url_args().Decode(data_decoded_as_url);
							self.update(parsed_args.id, data_decoded_as_args);
							completeCallback(200, 'success', { text: '{ "ok": true }' });
							return;
						case 'delete':
							self.delete(parsed_args.id);
							completeCallback(200, 'success', { text: '{ "ok": true }' });
							return;
					}

					completeCallback(200, 'success', { ok: true });
				}
				, abort: function ()
				{
				}
			}
			return send_abort;
		};

		transport.prepare_try_to_prepare_send_abort = function ()
		{
			var self = this;
			return function (options, originalOptions, jqXHR)
			{
				if (0 == options.url.indexOf(self.options.url_prefix))
				{
					var parsed_args = self.options.ParseUrl(options.url);
					if (false != parsed_args)
					{
						return self.prepare_send_abort(options, originalOptions, jqXHR, parsed_args);
					}
				}
			}
		}

		return transport;
	}
});
