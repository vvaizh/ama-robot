﻿define(function ()
{
	var h_fastening_clip = {

		set_model_field_value: function (root_model, model_selector, value)
		{
			if ('' == model_selector)
			{
				return value;
			}
			else
			{
				if (!root_model || null == root_model || ('string' == typeof root_model))
					root_model = {};
				var model = root_model;
				var name_parts = model_selector.split('.');
				for (var i = 0; i < name_parts.length - 1; i++)
				{
					var name_part = name_parts[i];
					if (!model[name_part] || null == model[name_part] || 'string' == (typeof model[name_part]))
						model[name_part] = {};
					model = model[name_part];
				}
				var last_part = name_parts[name_parts.length - 1];
				var fixed_value= (false == value || true == value || !('string' == typeof value)) ? value : $.trim(value);
				model[last_part]= fixed_value;
				return root_model;
			}
		}

		, get_model_field_value: function (model, model_selector)
		{
			if (!model || null == model)
			{
				return null;
			}
			else if ('' == model_selector)
			{
				return model;
			}
			else
			{
				var name_parts = model_selector.split('.');
				for (var i = 0; i < name_parts.length - 1; i++)
				{
					var name_part = name_parts[i];
					if (!model[name_part])
						return null;
					model = model[name_part];
				}
				var last_name_part = name_parts[name_parts.length - 1];
				return (!model[last_name_part] && false != model[last_name_part]) ? null : model[last_name_part];
			}
		}

		, each_first_level_fastening: function (root_dom_item, on_fc_item)
		{
			var root_selector = (!root_dom_item.selector || null == root_dom_item.selector) ? '' : root_dom_item.selector;
			var self= this;
			root_dom_item.children().each(function (index)
			{
				var dom_item = $(this);
				var selector = root_selector + ' > ' + dom_item.prop('tagName');
				if ('cpw'!=dom_item.attr('fc-by'))
				{
					if (!dom_item.selector || null == dom_item.selector)
						dom_item.selector = selector;
					self.each_first_level_fastening(dom_item, on_fc_item);
				}
				else
				{
					var fc_id = dom_item.attr('fc-id');
					if (!dom_item.selector || null == dom_item.selector)
					{
						dom_item.selector = !fc_id || null == fc_id
							? selector : selector + '[fc-id="' + fc_id + '"]';
					}
					on_fc_item(dom_item);
					var model_selector = dom_item.attr('model-selector');
					if (''!=model_selector && (!model_selector || null==model_selector))
						self.each_first_level_fastening(dom_item, on_fc_item);
				}
			});
		}

		, get_parent_fc_of_type: function (dom_item, fc_type)
		{
			for (var parent = dom_item
				; null != parent && fc_type != parent.attr('fc-type')
				; parent = parent.parent())
				;
			return parent;
		}

	};

	return h_fastening_clip;
});