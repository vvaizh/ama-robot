﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_select = fc_abstract();

	fc_select.match = function (adom_item, tag_name, fc_type)
	{
		return 'SELECT' == tag_name;
	}

	fc_select.load_from_model = function (model, model_selector, dom_item, fc_data)
	{
		var value = h_fastening_clip.get_model_field_value(model, model_selector);
		$(dom_item).val(value);
		return fc_data;
	}

	fc_select.save_to_model = function (model, model_selector, dom_item)
	{
		var value = $(dom_item).val();
		return h_fastening_clip.set_model_field_value(model, model_selector, value);
	}

	fc_select.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.select_attrs = function (model_selector)
		{
			return this.fastening_attrs(model_selector);
		}

		_template_argument.option_attrs = function (option_value)
		{
			var selected_value = this.value();
			return (option_value != selected_value ? "" : "selected='selected'")
				+ " value='" + option_value + "'";
		},

		_template_argument.numbers_options = function (ifrom, ito)
		{
			var res = "";
			for (var i = ifrom; i < ito; i++)
			{
				res += "<option " + this.option_attrs(i) + " >" + i + "</option>";
			}
			return res;
		}
	}

	return fc_select;
});