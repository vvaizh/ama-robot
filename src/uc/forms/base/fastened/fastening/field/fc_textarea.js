﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_textarea = fc_abstract();

	fc_textarea.match = function (adom_item, tag_name, fc_type)
	{
		return 'TEXTAREA' == tag_name;
	}

	fc_textarea.load_from_model = function (model, model_selector, dom_item, fc_data)
	{
		var value = h_fastening_clip.get_model_field_value(model, model_selector);
		$(dom_item).text(value);
		return fc_data;
	}

	fc_textarea.save_to_model = function (model, model_selector, dom_item)
	{
		var value = $(dom_item).text();
		return h_fastening_clip.set_model_field_value(model, model_selector, value);
	}

	fc_textarea.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.textarea_attrs = function (model_selector)
		{
			return this.fastening_attrs(model_selector);
		}
	}

	return fc_textarea;
});