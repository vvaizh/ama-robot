﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_input_text = fc_abstract();

	fc_input_text.match = function (dom_item, tag_name, fc_type)
	{
		return 'INPUT' == tag_name;
	}

	fc_input_text.load_from_model = function (model, model_selector, dom_item, fc_data)
	{
		var value = h_fastening_clip.get_model_field_value(model, model_selector);
		$(dom_item).val(value);
		return fc_data;
	}

	fc_input_text.save_to_model = function (model, model_selector, dom_item)
	{
		var value = $(dom_item).val();
		model = h_fastening_clip.set_model_field_value(model, model_selector, value);
		return model;
	}

	fc_input_text.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.txt = function (model_selector, rended_as)
		{
			var attrs = this.fastening_attrs(model_selector, rended_as);

			var value = this.value();
			var fixed_value = (!value || null == value) ? '' : value.toString().replace(/\"/g, '&quot;');

			this.store_fc_data({ fc_type: 'txt' });

			var res = fixed_value + '"' + attrs.substring(0, attrs.length - 2);
			return res;
		}
	}

	return fc_input_text;
});