﻿define([
	  'forms/base/fastened/test/fft_default'
	, 'forms/base/fastened/test/fft_select2'
	, 'forms/base/fastened/test/fft_typeahead'
	, 'forms/base/fastened/test/fft_checkbox'
	, 'forms/base/fastened/test/fft_radio'
]
, function (fft_default)
{
	var fastened_field_testers = arguments;
	return {

		find_tester_for: function(adom_item)
		{
			var dom_item= $(adom_item);
			var tag_name = dom_item.prop("tagName").toUpperCase();
			var fc_type = dom_item.attr('fc-type');
			for (var i = 1; i < fastened_field_testers.length; i++)
			{
				var fastened_field_tester = fastened_field_testers[i];
				
				if (fastened_field_tester.match(dom_item, tag_name, fc_type))
					return fastened_field_tester;
			}
			return fft_default;
		}

	};
});