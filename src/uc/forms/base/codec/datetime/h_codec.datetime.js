﻿define(['forms/base/codec/codec'],
function (base_codec)
{
	var h_codec = {};

	h_codec.format = {

		ru: {
			date_time_splitter: ' '
			, date: { splitter: '.', parts: ['DD', 'MM', 'YYYY'] }
			, time: { splitter: ':', parts: ['hh', 'mm', 'ss'] }
		}

		, ru_legal: {
			date_time_splitter: ' '
			, date: { splitter: '.', parts: ['DD', 'MM', 'YYYY'] }
			, time: { splitter: ':', parts: ['hh', 'mm'] }
		}

		, xsd: {
			date_time_splitter: 'T'
			, date: { splitter: '-', parts: ['YYYY', 'MM', 'DD'] }
			, time: { splitter: ':', parts: ['hh', 'mm', 'ss'] }
		}

	};

	h_codec.Encode_d = function (txt, format)
	{
		var res = {text:txt};
		var parts= txt.split(format.splitter);
		for (var i= 0; i<format.parts.length && i<parts.length; i++)
			res[format.parts[i]]= parts[i];
		return res;
	}

	h_codec.Encode = function (txt, format)
	{
		var res = {text:txt};
		var date_time_parts = txt.split(format.date_time_splitter);

		res.date = this.Encode_d(date_time_parts[0], format.date);

		if (date_time_parts.length > 1)
			res.time = this.Encode_d(date_time_parts[1], format.time);

		return res;
	}

	h_codec.EncodeDateTime = function (txt_date, txt_time, format)
	{
		return {
			date: this.Encode_d(txt_date, format.date)
			, time: this.Encode_d(txt_time, format.time)
		};
	}

	h_codec.Decode_d = function (p, format)
	{
		var txt = '';
		for (var i = 0; i < format.parts.length; i++)
		{
			var part= format.parts[i];
			if (p[part])
			{
				if ('' != txt)
					txt += format.splitter;
				txt += p[part];
			}
		}
		return txt;
	}

	h_codec.Decode = function (dt, format)
	{
		var txt = this.Decode_d(dt.date, format.date);
		if (dt.time)
		{
			var txt_time = this.Decode_d(dt.time, format.time);
			if ('' != txt_time)
			{
				if ('' != txt)
					txt += format.date_time_splitter;
				txt += txt_time;
			}
		}
		return txt;
	}

	h_codec.txt2dt= function(format)
	{
		var codec = base_codec();
		codec.EncodeDateTime = function (txt_date,txt_time) { return h_codec.EncodeDateTime(txt_date,txt_time, format); }
		codec.Encode = function (txt) { return h_codec.Encode(txt, format); }
		codec.Decode = function (dt)  { return h_codec.Decode(dt,  format); }
		return codec;
	}

	h_codec.ru_txt2dt= function()
	{
		return this.txt2dt(this.format.ru);
	}

	h_codec.xsd_txt2dt = function ()
	{
		return this.txt2dt(this.format.xsd);
	}

	h_codec.txt2txt = function (format_from,format_to)
	{
		var x_txt2dt_from = this.txt2dt(format_from);
		var x_txt2dt_to = this.txt2dt(format_to);
		var codec = base_codec();
		codec.EncodeDateTime = function (txt_date,txt_time) { return x_txt2dt_to.Decode(x_txt2dt_from.EncodeDateTime(txt_date,txt_time)); }
		codec.Encode = function (txt) { return x_txt2dt_to.Decode(x_txt2dt_from.Encode(txt)); }
		codec.Decode = function (dt) { return x_txt2dt_from.Decode(x_txt2dt_to.Encode(dt)); }
		codec.DecodeDate = function (dt)
		{
			var dt = x_txt2dt_to.Encode(dt);
			if (dt.time)
				delete dt.time;
			return x_txt2dt_from.Decode(dt);
		}
		return codec;
	}

	h_codec.ru_txt2txt_xsd = function ()
	{
		return this.txt2txt(this.format.ru, this.format.xsd);
	}

	h_codec.ru_legal_txt2txt_xsd = function ()
	{
		return this.txt2txt(this.format.ru_legal, this.format.xsd);
	}

	return h_codec;
});