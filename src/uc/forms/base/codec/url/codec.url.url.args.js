﻿define([
	  'forms/base/codec/url/codec.url'
	, 'forms/base/codec/url/codec.url.args'
],
function (codec_url, codec_url_args)
{
	return function ()
	{
		var codec = {};

		codec.cc_codec_url = codec_url();
		codec.cc_odec_url_args = codec_url_args();

		codec.Decode = function (url_encoded_args)
		{
			return this.cc_odec_url_args.Decode(this.cc_codec_url.Decode(url_encoded_args));
		}

		codec.Encode = function (args)
		{
			return this.cc_codec_url.Encode(this.cc_odec_url_args.Encode(args));
		}

		return codec;
	}
});