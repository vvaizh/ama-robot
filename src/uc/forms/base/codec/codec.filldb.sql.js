define([
	  'forms/base/codec/codec'
],
function (BaseCodec, GetLogger)
{
	return function ()
	{
		var codec = BaseCodec();

		codec.EncodeValue= function(value)
		{
			var t = typeof value;
			switch (t)
			{
				case 'string': return '\'' + value + '\'';
				case 'boolean': return ((true==value) ? 1 : 0);
				default: return value;
			}
		}

		codec.Encode = function (data)
		{
			var res_txt_sql = '';
			for (var table_name in data)
			{
				if ('xml_mode' != table_name)
					res_txt_sql += "select 'insert into " + table_name + "' as '';\r\n";

				var table = data[table_name];
				var row_prefix = 'insert into `' + table_name + '`';
				for (var i = 0; i < table.length; i++)
				{
					var row = table[i];
					res_txt_sql += row_prefix;
					var j = 0;
					for (var field_name in row)
					{
						res_txt_sql += (0 != j) ? ', ' : ' set ';
						res_txt_sql += '`' + field_name + '`= ' + codec.EncodeValue(row[field_name]);
						j++;
					}
					res_txt_sql += ';\r\n';
				}
				res_txt_sql += '\r\n';
			}
			return res_txt_sql;
		}

		return codec;
	}
});
