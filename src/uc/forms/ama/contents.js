define
(
	[
		  'forms/base/collector'
		, 'txt!forms/ama/robot/todo/tests/contents/sample0.json.txt'
		, 'txt!forms/ama/robot/tasks/default/tests/contents/sample0.json.txt'
		, 'txt!forms/ama/robot/tasks/default/tests/contents/sample1.json.txt'
		, 'txt!forms/ama/robot/tasks/efrsb/tests/contents/sample0.json.txt'
		, 'txt!forms/ama/robot/help/tests/contents/sample0.json.txt'
		, 'txt!forms/ama/robot/help/tests/contents/sample1.json.txt'
		, 'txt!forms/ama/robot/main/tests/contents/sample0.json.txt'
	],
	function (collect)
	{
		return collect([
		  'r-todo-sample0'
		, 'r-task-default-sample0'
		, 'r-task-default-sample1'
		, 'r-task-efrsb-sample0'
		, 'r-help-sample0'
		, 'r-help-sample1'
		, 'r-main-sample0'
		], Array.prototype.slice.call(arguments,1));
	}
);