﻿require.config
({
	enforceDefine: true,
	urlArgs: "bust" + (new Date()).getTime(),
	baseUrl: '.',
	paths:
	{
		styles: 'css',
		images: 'images'
	},
	map:
	{
		'*':
		{
			tpl: 'js/libs/tpl',
			css: 'js/libs/css',
			img: 'js/libs/image',
			txt: 'js/libs/txt'
		}
	}
}),

require
(
	[
		'forms/base/codec/url/codec.url.url.args'
		, 'forms/ama/robot/todo/f_todo'
		, 'forms/ama/robot/tasks/f_task'
        , 'forms/ama/robot/tasks/f_efrsb_task'
	],
	function ()
	{
		var extension =
		{
			Title: 'ПАУ'
			, key: 'ama'
			, forms: {}
			, codec_url_url_args: arguments[0]
		};
		var forms = Array.prototype.slice.call(arguments, 0);
		for (var i = 1; i < forms.length; i++)
		{
		    var form = arguments[i];
			extension.forms[form.key] = form;
		}
		if (RegisterCpwFormsExtension)
		{
			RegisterCpwFormsExtension(extension);
		}
		return extension;
	}
);