﻿define([
	  'forms/base/codec/url/codec.url.url.args'
],
function (codec_url_url_args)
{
	var url_prefix = 'ama/robot/task';

	var transport = { options: { url_prefix: url_prefix } };

	transport.prepare_try_to_prepare_send_abort = function ()
	{
		var self = this;
		return function (options, originalOptions, jqXHR)
		{
			if (self.options && 0 == options.url.indexOf(self.options.url_prefix))
			{
				var send_abort =
				{
					send: function (headers, completeCallback)
					{
						var decodec = codec_url_url_args();
						var args = decodec.Decode(options.url);
						var data = !options.data ? null : decodec.Decode(options.data);
						var res = null;
						switch (args.action)
						{
							case 'select':		res = self.options.select_task(args); break;
						    case 'done':		res = self.options.done_task(data); break;
							case 'skip':		res = self.options.skip_task(data); break;
                            case 'co_efrsb':    res = self.options.complete_efrsb(data); break;
                            case 'ch_efrsb':    res = self.options.check_status_efrsb_task(data); break;
						}
						completeCallback(200, 'success', { text: JSON.stringify(res) });
					}
					, abort: function ()
					{
					}
				}
				return send_abort;
			}
		}
	}

	return transport;
});