﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/robot/main/e_robot_main.html'
	, 'forms/ama/robot/todo/c_todo_ajax_select'
	, 'forms/ama/robot/main/a_robot_task'
	, 'forms/ama/robot/tasks/default/c_task_default'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, c_todo, a_robot_task, c_task_default, h_msgbox)
{
	var ajax_transport_included = false;
	return function ()
	{
		var options = {
			field_spec:
			{
				todo: { controller: c_todo }
			}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			if (!ajax_transport_included)
			{
				ajax_transport_included = true;
				a_robot_task.options.select_task = function (args) { self.SelectTask(args); return true; }
				a_robot_task.options.done_task = function (args) { self.DoneTask(args); return true; }
				a_robot_task.options.skip_task = function (args) { self.SkipTask(args); return true; }
				$.ajaxTransport('+*', a_robot_task.prepare_try_to_prepare_send_abort());
			}
		}

		controller.Найти_задачу = function (id_task, id_procedure)
		{
			if (this.model && this.model.todo && this.model.todo.Задачи)
			{
				var задачи = this.model.todo.Задачи;
				for (var i = 0; i < задачи.length; i++)
				{
					var задача = задачи[i];
					if (задача.ЗадачаID == id_task && задача.ПроцедураID == id_procedure)
						return задача;
				}
			}
			return null;
		}

		controller.Удалить_задачу = function (id_task, id_procedure)
		{
			if (this.model && this.model.todo && this.model.todo.Задачи)
			{
				var задачи = this.model.todo.Задачи;
				for (var i = 0; i < задачи.length; i++)
				{
					var задача = задачи[i];
					if (задача.ЗадачаID == id_task && задача.ПроцедураID == id_procedure)
					{
						задачи.splice(i, 1);
						var sel = this.fastening.selector;
						$(sel + ' div.task[id_procedure="' + id_procedure + '"][id_task="' + id_task + '"]').remove();
						return;
					}
				}
			}
		}

		controller.Найти_процедуру= function (id_procedure)
		{
			if (this.model && this.model.todo && this.model.todo.Процедуры)
			{
				var процедуры = this.model.todo.Процедуры;
				for (var i = 0; i < процедуры.length; i++)
				{
					var процедура = процедуры[i];
					if (процедура.ID == id_procedure)
						return процедура;
				}
			}
			return null;
		}

		controller.SelectTask = function (args)
		{
			var sel = this.fastening.selector;
			var task_model = { 
				Задача: this.Найти_задачу(args.id_task, args.id_procedure)
				, Процедура: this.Найти_процедуру(args.id_procedure)
			}
			this.SafeClearTaskController();
			this.task_controller = c_task_default();
			this.task_controller.SetFormContent(task_model);
			this.task_controller.Edit(sel + ' div.cpw-ama-robot-main-form > div.task');
		}

		var base_Destroy = controller.Destroy;
		controller.Destroy= function()
		{
			this.SafeClearTaskController();
			base_Destroy.call(this);
		}

		controller.SafeClearTaskController= function()
		{
			if (this.task_controller && null != this.task_controller)
			{
				this.task_controller.Destroy();
				this.task_controller = null;
				delete this.task_controller;
			}
			var sel = this.fastening.selector;
			$(sel + ' div.cpw-ama-robot-main-form > div.task').text('Задача не выбрана');
		}

		controller.DoneTask = function (args)
		{
			this.DropTask(args, 'сделано');
		}

		controller.SkipTask = function (args)
		{
			this.DropTask(args, 'пропущено');
		}

		controller.DropTask = function (args, action)
		{
			var Задача = this.Найти_задачу(args.id_task, args.id_procedure);

			this.SafeClearTaskController();
			this.Удалить_задачу(args.id_task, args.id_procedure);

			var Процедура = this.Найти_процедуру(args.id_procedure);
			var html = 'Задача "' + Задача.Формулировка
				+ '"<br/>&nbsp;&nbsp;для процедуры ' + Процедура.тип.Кратко + ', "' + Процедура.Должник.Наименование
				+ '"<br/>&nbsp;&nbsp;&nbsp;&nbsp; отмечена как "' + action + '"';
			if (args.description && null != args.description && '' != args.description)
				html += '<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; с комментарием: ' + args.description;

			setTimeout(function () { h_msgbox.ShowModal({ title: action, html: html, width:600 }); }, 10);
		}

		return controller;
	}
});