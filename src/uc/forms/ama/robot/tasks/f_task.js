﻿define(['forms/ama/robot/tasks/c_task'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'task'
		, Title: 'Задача в рамках программы ПАУ'
		, FileFormat:
			{
				FileExtension: 'tsk'
				, Description: 'tsk. Задача ПАУ'
			}
	};
	return form_spec;
});
