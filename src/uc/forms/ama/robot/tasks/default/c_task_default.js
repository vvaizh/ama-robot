﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/robot/tasks/default/e_task_default.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/robot/help/c_r_help'
],
function (c_fastened, tpl, h_msgbox, c_r_help)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);
			h_msgbox.ShowModal();
			h_msgbox.Close();
			var self = this;
			$(sel + ' button').button();
			$(sel + ' a.how').click(function (e) { e.preventDefault(); self.OnQuestion('как'); });
			$(sel + ' a.why').click(function (e) { e.preventDefault(); self.OnQuestion('почему'); });

			$(sel + ' button.done').click(function () { self.OnDone(); });
			$(sel + ' button.skip').click(function () { self.OnSkip(); });
		}

		controller.OnQuestion= function(q)
		{
			var help = c_r_help(); 
			var model = this.fastening.model;
			var задача = !model ? {} : model.Задача;
			var процедура = !model ? { Тип: {}, Должник: {} } : model.Процедура;
			help.SetFormContent({ Вопрос: q, Процедура: процедура, Задача: задача });
			var self = this;
			h_msgbox.ShowModal
			({
				title: 'Описание типовой задачи АУ', controller: help
			});
		}

		controller.OnDone= function()
		{
			this.ProcessTask('done', "Отправка команды отметки задачи как сделанной");
		}

		controller.OnSkip = function ()
		{
			this.ProcessTask('skip', "Отправка команды отметки задачи как пропущенной");
		}

		controller.ProcessTask= function(action,ajax_text)
		{
			var sel = this.fastening.selector;
			var description = $(sel + ' textarea').text();
			var model = this.fastening.model;
			var задача = !model ? {} : model.Задача;
			self = this;
			var ajaxurl = 'ama/robot/task?action=' + action;
			var v_ajax = h_msgbox.ShowAjaxRequest(ajax_text, ajaxurl);
			var description =
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: { id_task: задача.ЗадачаID, id_procedure: задача.ПроцедураID, description: description }
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
				}
			});
		}

		return controller;
	}
});