﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/robot/tasks/efrsb/e_efrsb_task.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/robot/main/a_robot_task'
	, 'forms/ama/robot/help/c_r_help'
	, 'tpl!forms/ama/robot/tasks/efrsb/v_no_message.html'
	, 'tpl!forms/ama/robot/tasks/efrsb/v_skip_efrsb_task.html'
    , 'forms/base/h_fullname'
    , 'tpl!forms/ama/robot/tasks/efrsb/v_message.html'
],
    function (c_fastened, tpl, h_msgbox, a_robot_task, c_r_help, v_no_message, v_skip_efrsb_task, h_fullname, v_message) {
	var ajax_transport_for_efrsb = false;

    return function () {
		var controller = c_fastened(tpl);
		var base_Render = controller.Render;

			controller.Render = function (sel) {
				base_Render.call(this, sel);
				h_msgbox.ShowModal();
				h_msgbox.Close();
				var self = this;

				$(sel + ' button').button();
				$(sel + ' a.how').click(function (e) { e.preventDefault(); self.OnQuestion('как'); });
				$(sel + ' a.why').click(function (e) { e.preventDefault(); self.OnQuestion('почему'); });

				$(sel + ' button.co_efrsb').click(function () { self.OnComplete() })
				$(sel + ' button.ch_efrsb').click(function () { self.OnCheckEfrbMessage() })
			}

		controller.CompleteEfrsbTask = function () {
			alert('Зарегистрировано');
		}

        controller.CheckStatusEfrsb = function () {
            var model = JSON.stringify(this.fastening.model.Результат_проверки.Статус_сообщения);
            return model;
		}

		controller.занести_данные_о_пользователе = function ()
		{
			var model = this.fastening.model;
			var пользователи;
			if (!model) {
				return пользователи = {};
			}
			else {
				return пользователи = !model.Результат_проверки ? model.users : model.Результат_проверки.users;
			}
		}

		var base_SetFormContent = controller.SetFormContent;

		controller.SetFormContent = function (data)
		{
			base_SetFormContent.call(this, data);
			var result = typeof (data) === 'object' ? data : JSON.parse(data);
			if (result.Результат_проверки)
			{
				if (!ajax_transport_for_efrsb) {
					ajax_transport_for_efrsb = true;

					$.ajaxTransport(
						'+*',
						a_robot_task.prepare_try_to_prepare_send_abort()
					);
				}
				a_robot_task.options.check_status_efrsb_task = function (args) { return self.CheckStatusEfrsb(args); }
				a_robot_task.options.complete_efrsb = function (args) { self.CompleteEfrsbTask(args); return true; }

			}
		}

		controller.OnWaitForm = function () {
			var data = new Date();
			var startDate = data.getHours() + ":" + data.getMinutes() + ':'+data.getSeconds();
			var v_html = '<div style="margin:10px 20px 2px 20px;font-size:16px;">Идёт загрузка новых объявлений, подождите...</div>' +
				'<div style="margin-left:20px; font-size:12px;color:#808080;">Время начала загрузки: '+startDate+'</div>'+
				'<div style="margin-left:20px; font-size:12px;color:#808080;"> Окно закроется автоматически</div> ';
			h_msgbox.ShowModal({
				title: 'Загрузка новых объявлений с сайта ЕФРСБ',
				html: v_html
			});
		}

		controller.Check = function (on_result) {
			var sel = this.fastening.selector;
			var model = this.fastening.model;
			var задача = !model ? {} : model.Задача;
			self = this;
			var ajaxurl = 'ama/robot/task?action=ch_efrsb';
			var v_ajax = h_msgbox.ShowAjaxRequest("Проверка сообщения на ефрсб", ajaxurl);
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: { id_task: задача.ЗадачаID, id_procedure: задача.ПроцедураID }
				, beforeSend: function () {
					
				}
                , success: function (data, textStatus) {
				if (null == data) {
					v_ajax.ShowAjaxError(data, textStatus);
                }
                var obj = JSON.parse(data);
                on_result(obj);
				}
			});
		}

		controller.OnCheckEfrbMessage = function ()
		{
			var sel = this.fastening.selector;
			var description = $(sel + ' textarea').text();
			var model = this.fastening.model;
            var задача = !model ? {} : model.Задача;
            var процедура = !model ? {} : model.Процедура;
			var message = null;
			var self = this;

            this.Check(function (res) {
                var btnOkName = 'OK';
                var btnCancel = 'Отмена';
                var btnDoneName = 'Подвердить наличие сообщения';
				h_msgbox.ShowModal
				({
					title: 'Проверка публикации объявления на ЕФРСБ',
                        html: res != null ? v_message({result: res, task: задача, proc: процедура}) : v_no_message(), width: 580
                        , buttons: res != null ? [btnCancel, btnDoneName] : ['Принудительно отметить задачу как завершенную', btnCancel]
					, onclose: function (btn) {
                        if (btn != btnCancel && btn != btnDoneName) {
                            // todo: принудительно отмечаем как незавершённую
                            self.OnForcedSkipTask();
                        }
						else if (btn != btnCancel) {
							self.OnCompleteTask();
                        }
                    }
                });
			});
        }

        controller.OnCompleteTask = function () {
            var btnCancelName = 'Отмена';
            var self = this;
            var model = this.fastening.model;

            var users = {};
            var процедура = {};
            var задача = {};

            if (model) {
                users = !model.Результат_проверки ? model.users : model.Результат_проверки.users;
                задача = model.Задача;
                процедура = model.Процедура;
            }

            h_msgbox.ShowModal
                ({
                    title: 'Подтверждение наличия сообщения на ЕФРСБ',
                    id_div: 'cpw-ama-robot-task-complete-verification-form'
                    , html: v_skip_efrsb_task({ users: users, процедура: процедура, задача: задача, helper: h_fullname, haveMessage : true }), width: 600
                    , buttons: [btnCancelName,'Отметить задачу как завершённую']
                    , onclose: function (btn) {
                        if (btn != btnCancelName) {
                            // todo: принудительно отмечаем как незавершённую
                            self.OnSkip();
                        }
                    }
                });
        }

		controller.OnForcedSkipTask = function () {
			var btnOkName = 'OK';
			var self = this;
			var model = this.fastening.model;
			
			var users = {};
			var процедура = {};
			var задача = {};

			if (model) {
				users = !model.Результат_проверки ? model.users : model.Результат_проверки.users;
				задача = model.Задача;
				процедура = model.Процедура;
			}

			h_msgbox.ShowModal
			({
				title: 'Принудительное выделение задачи как завершенная',
				id_div: 'cpw-ama-robot-task-forced-skip-form'
                    , html: v_skip_efrsb_task({ users: users, процедура: процедура, задача: задача, helper: h_fullname, haveMessage: false}), width: 600
				, buttons: ['Отметить задачу как завершённую', btnOkName]
				, onclose: function (btn) {
					if (btn != btnOkName) {
						// todo: принудительно отмечаем как незавершённую
						self.OnSkip();
					}
				}
			});
		}

		controller.OnQuestion = function (q) {
			var help = c_r_help();
			var model = this.fastening.model;
			var задача = !model ? {} : model.Задача;
			var процедура = !model ? { Тип: {}, Должник: {} } : model.Процедура;
			help.SetFormContent({ Вопрос: q, Процедура: процедура, Задача: задача });
			var self = this;
			h_msgbox.ShowModal
			({
			    title: 'Описание типовой задачи АУ', controller: help
			});
        }

		controller.OnSkip = function ()
        {
			this.ProcessTask('done', "Отправка команды отметки задачи как сделанной");
		}

		controller.OnComplete = function ()
		{
			return this.ProcessTask('co_efrsb', "Отправка команды запуска процесса отправки сообщения на ЕФРСБ");
		}

		controller.OnCheck = function ()
		{
			return this.ProcessTask("ch_efrsb", "Получить статус сообщения на ЕФРСБ")
		}

		controller.ProcessTask = function (action, ajax_text) {
            var sel = this.fastening.selector;
			var description = $(sel + ' textarea').text();
            var model = this.fastening.model;
			var задача = !model ? {} : model.Задача;
			var message = null;
            self = this;
            var ajaxurl = 'ama/robot/task?action=' + action;
			var v_ajax = h_msgbox.ShowAjaxRequest(ajax_text, ajaxurl);
			var description = 
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: { id_task: задача.ЗадачаID, id_procedure: задача.ПроцедураID, description: description }
				, success: function (data, textStatus) {
					if (null == data) {
						v_ajax.ShowAjaxError(data, textStatus);
					}
				}
			});

        }
		function ajax_callback() {
			alert("1");
		}
        return controller;
    }
});


