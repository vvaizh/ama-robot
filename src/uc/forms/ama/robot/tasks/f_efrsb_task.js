﻿define(['forms/ama/robot/tasks/c_efrsb_task'],
function (CreateController) {
    var form_spec =
	{
	    CreateController: CreateController
		, key: 'efrsb'
		, Title: 'Задача в рамках программы ПАУ'
		, FileFormat:
			{
			    FileExtension: 'efrsb'
                , Description: 'efrsb. Задача ПАУ'
			}
	};
    return form_spec;
});
