﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/robot/todo/v_todo.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' select').change(function () { self.OnSelectProcedure(); });
			$(sel + ' div.task').click(function (e) { self.OnClickTask(e); });
		}

		controller.OnClickTask= function(e)
		{
			var sel = this.fastening.selector;
			var div_task = $(e.target);
			if (!div_task.hasClass('task'))
				div_task = div_task.parents('div.task');
			var id_task = div_task.attr('id_task');
			var id_procedure = div_task.attr('id_procedure');
			$(sel + ' div.task').removeClass('selected');
			div_task.addClass('selected');
			this.OnSelectTask(id_task, id_procedure);
		}

		controller.OnSelectProcedure= function()
		{
			var sel = this.fastening.selector;
			var id_selected_Procedure = $(sel + ' select').val();
			
			$(sel + ' div.tasks div.task').each(function ()
			{
				var itask = $(this);
				var id_Procedure = itask.attr('id_Procedure');
				itask.parents().toggleClass('no-proc', '' != id_selected_Procedure && id_Procedure != id_selected_Procedure);
			});
			$(sel + ' div.tasks').toggleClass('no-proc', '' != id_selected_Procedure);
		}

		controller.OnSelectTask = function (id_task, id_procedure)
		{
		}

		return controller;
	}
});