﻿define(['forms/ama/robot/todo/c_todo_ajax_select'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'todo'
		, Title: 'Фронт задач в рамках программы ПАУ'
		, FileFormat:
			{
				FileExtension: 'tdo'
				, Description: 'TDO. Фронт задач ПАУ'
			}
	};
	return form_spec;
});
