﻿define([
	  'forms/ama/robot/todo/c_todo'
	, 'forms/base/h_msgbox'
],
function (c_todo, h_msgbox)
{
	return function ()
	{
		var controller = c_todo();

		controller.OnSelectTask = function (id_task, id_procedure)
		{
			self = this;
			var ajaxurl = 'ama/robot/task?action=select';
			var v_ajax = h_msgbox.ShowAjaxRequest(200, "Отправка команды выбора задачи", ajaxurl);
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, data: { id_task: id_task, id_procedure: id_procedure }
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
				}
			});
		}

		return controller;
	}
});