﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ama/robot/help/v_r_help.html'
	, 'forms/ama/robot/help/h_r_help_content'
	, 'txt!forms/ama/robot/help/content/default_why.html'
	, 'txt!forms/ama/robot/help/content/default_how.html'
],
function (c_fastened, tpl, h_r_help_content, default_why, default_how)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		controller.size = { width: 650, height: 480 };

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			if (this.fastening.model)
			{
				var вопрос = this.fastening.model.Вопрос;
				var tabcontrol = $(sel + ' #cpw-ama-robot-task-help-form-tabs');
				switch (вопрос)
				{
					case 'почему': tabcontrol.tabs("option", "active", 0); break;
					case 'как': tabcontrol.tabs("option", "active", 1); break;
				}

				var pname = this.fastening.model.Процедура.тип.Полностью;
				var qhelp = null;
				if (h_r_help_content[pname])
				{
					var phelp= h_r_help_content[pname];
					var задача = this.fastening.model.Задача;
					if (phelp[задача.Тип])
						qhelp = phelp[задача.Тип];
				}
				if (!qhelp || null==qhelp)
					qhelp = { why: default_why, how: default_how };

				$(sel + ' #cpw-ama-robot-task-help-tab-why div.content').html(qhelp.why);
				$(sel + ' #cpw-ama-robot-task-help-tab-how div.content').html(qhelp.how);
			}
		}

		return controller;
	}
});