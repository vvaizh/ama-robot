﻿define([
	  'txt!forms/ama/robot/help/content/kp/efrsb_start/why.html'
	, 'txt!forms/ama/robot/help/content/kp/efrsb_start/how.html'
],
function (
	kp_efrsb_start_why, kp_efrsb_start_how
)
{
	var content = {};

	content.Н = {};

	content.КП = {
		efrsb_start: { why: kp_efrsb_start_why, how: kp_efrsb_start_how }
	};
	content.РД = {};
	content.РИ = {};

	content['Наблюдение'] = content.Н;
	content['Конкурсное производство'] = content.КП;
	content['Реструктуризация долгов'] = content.РД;
	content['Реализация имущества'] = content.РИ;

	return content;
});