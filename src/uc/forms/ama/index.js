define
(
	[
		  'forms/base/collector'
		, 'forms/ama/robot/todo/c_todo'
		, 'forms/ama/robot/tasks/default/c_task_default'
		, 'forms/ama/robot/help/c_r_help'
		, 'forms/ama/robot/main/c_robot_main'
		, 'forms/ama/robot/tasks/efrsb/c_efrsb_task'
	],
	function (collect)
	{
		return collect([
		  'robot-todo'
		, 'r-task-default'
		, 'r-help'
		, 'r-main'
		, 'r-task-efrsb'
		], Array.prototype.slice.call(arguments,1));
	}
);