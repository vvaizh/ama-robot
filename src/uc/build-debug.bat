call :build-forms
call :build-contents
call :build-wbt
call :build-test-ajax
exit

rem -----------------------------------------------------------
:build-forms
del built\forms.js
node optimizers\r.js -o conf\build-debug-forms.js
if NOT 0==%ERRORLEVEL% exit
exit /B

rem -----------------------------------------------------------
:build-contents
del built\contents.js
node optimizers\r.js -o conf\build-debug-contents.js
if NOT 0==%ERRORLEVEL% exit
exit /B

rem -----------------------------------------------------------
:build-wbt
del built\wbt.js
node optimizers\r.js -o conf\build-debug-wbt.js
if NOT 0==%ERRORLEVEL% exit
exit /B

rem -----------------------------------------------------------
:build-test-ajax
del built\test-ajax.js
node optimizers\r.js -o conf\build-debug-test-ajax.js
if NOT 0==%ERRORLEVEL% exit
exit /B
