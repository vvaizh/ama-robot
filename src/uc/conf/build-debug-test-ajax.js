({
    baseUrl: "..",
    include: ['js/test-ajax'],
    name: "optimizers/almond",
    out: "..\\built\\test-ajax.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/txt'
      }
    }
})