﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AMA.Robot.mock
{
    public class Дело : IДела_в_работе
    {
        IGuidProvider guid;
        ITimeProvider timeProvider;

        readonly HashSet<IЗадача> РешенныеЗадачи = new HashSet<IЗадача>();
        public void ЗарегистрироватьКонтрагента(Контрагент контрагент) { }
        public void ОбновитьТребования  (string id, decimal sum)    { }
        public void ОбновитьТребования  (string id, DateTime date)  { }
        public void IncludeClaim        (String id)                 { }
        public void Восстановить_задачу ()                          { }
        public void ПропуститьЗадачу    (IЗадача задача)            { }

        private List<Денежные_срества.Учётная_группа>   учетныеГруппы   { get; set; }
        private List<Денежные_срества.Движение>         движениеСредств { get; set; }
        private List<Процедура>                         Процедуры       { get; set; }
        private List<Исходящее>                         Исходящие       { get; set; }
        private List<Ответ>                             Ответы          { get; set; }
        private List<Efrsb_message_record>              ЕФРСБ           { get; set; }
        private List<Публикация>                        Публикации      { get; set; }
        private List<Кредитор>                          Кредиторы       { get; set; }
        private List<Требование>                        Требования      { get; set; }
        private List<Собрание>                          Собрания        { get; set; }
        private List<Счет>                              Счета           { get; set; }
        private List<Анализ>                            Анализы         { get; set; }
        private List<Инвентаризация>                    Инвентаризации  { get; set; }
        private List<Торги>                             Торги           { get; set; }


        public List<IЗадача> ВсеПропущенныеЗадачи(string procedureID)
        {
            return new List<IЗадача>();
        }
        public Контрагент КонтрагентПоИНН(string inn) { return null; }
        public Контрагент КонтрагентByContactId(string contact_id) { return null; }
        public void ЗарегистрироватьДенежнуюОперацию(string manproc_id, Денежные_срества.Движение движение, int type = 0 )
        {
            if (движениеСредств == null)
            {
                движениеСредств = new List<Денежные_срества.Движение>();
            }
                    
            if(движение.Куда.учётная_группа != Денежные_срества.ТипУчетнойГруппы.Деньги_третьих_лиц &&
                движение.Откуда.учётная_группа != Денежные_срества.ТипУчетнойГруппы.Деньги_третьих_лиц)
            {
                Денежные_срества.Движение откуда = new Денежные_срества.Движение();
                откуда.дата = движение.дата;
                откуда.обоснование = движение.обоснование;
                откуда.Откуда = движение.Откуда;
                откуда.Куда = new Денежные_срества.Адрес();
                откуда.Куда.учётная_группа = Денежные_срества.ТипУчетнойГруппы.Деньги_третьих_лиц;
                откуда.сумма = "-" + движение.сумма;
                движениеСредств.Add(откуда);

                Денежные_срества.Движение куда = new Денежные_срества.Движение();
                куда.дата = движение.дата;
                куда.обоснование = движение.обоснование;
                куда.Куда = движение.Куда;
                куда.сумма = движение.сумма;
                куда.Откуда = new Денежные_срества.Адрес();
                куда.Откуда.учётная_группа = Денежные_срества.ТипУчетнойГруппы.Деньги_третьих_лиц;
                движениеСредств.Add(куда);
            }
            else
            {
                движениеСредств.Add(движение);
            }

        }

        public void ЗарегистрироватьНачальноеСальдо(string procedureID, Денежные_срества.Учётная_группа группа)
        {
            if (учетныеГруппы == null)
                учетныеГруппы = new List<Денежные_срества.Учётная_группа>();

            учетныеГруппы.Add(группа);
        }

        public IEnumerable<Денежные_срества.Движение> ДвиженияДенежныхСредств(string procedureID)
        {
            return движениеСредств;
        }

        public IEnumerable<Денежные_срества.Учётная_группа> ВсеУчетныеГруппы(string procedureID)
        {
            return учетныеГруппы;
        }

        public List<Процедура> ВсеПроцедуры()
        {
            return Процедуры;
        }

        public string ЗарегистрироватьОценкуСчетов(Инвентаризация inv)
        {
            Инвентаризация invent = new Инвентаризация();
            invent = Инвентаризации.Where(i => i.ПроцедураID == inv.ПроцедураID).LastOrDefault();
            invent.ДатаОценки = inv.ДатаОценки;
            return "1";
        }

        public void Зарегистрировать_решение(IЗадача задача, string taskResult)
        {
            задача.Статус = task.Статус.Завершена;
            задача.ДатаРешения = timeProvider.Now;
            РешенныеЗадачи.Add(задача);
        }

        public IЗадача Задача_решена(IЗадача задача)
        {
            return РешенныеЗадачи.SingleOrDefault(t => t.Формулировка == задача.Формулировка);
        }
        
        public Дело(IGuidProvider _guid, ITimeProvider _timeProvider)
        {
            timeProvider = _timeProvider;
            guid = _guid;
            Процедуры= new List<Процедура>();
            Исходящие= new List<Исходящее>();
            Ответы= new List<Ответ>();
            ЕФРСБ= new List<Efrsb_message_record>();
            Публикации= new List<Публикация>();
            Кредиторы= new List<Кредитор>();
            Требования= new List<Требование>();
            Собрания= new List<Собрание>();
            Счета= new List<Счет>();
            Анализы= new List<Анализ>();
            Инвентаризации= new List<Инвентаризация>();
            Торги= new List<Торги>();
            РешенныеЗадачи= new HashSet<IЗадача>();
        }

        public void ПровестиСобрание(Собрание собрание)
        {
            собрание.Проведено = true;
        }

        public Процедура Процедура(string ID)
        {
            return Процедуры.SingleOrDefault(p => p.ID == ID);
        }

        public Процедура ПроцедураПоИНН(string INN)
        {
            return Процедуры.SingleOrDefault(p => p.Должник.ИНН == INN);
        }

        public Исходящее Исходящее(string ID)
        {
            return Исходящие.SingleOrDefault(m => m.ID == ID);
        }
        public List<IЗадача> ВсеРешенныеЗадачи(string procedureID)
        {
            return new List<IЗадача>(); 
        }

        public IEnumerable<Исходящее> ВсеИсходящие(string procedureID, string type = null)
        {
            return Исходящие.Where(m => m.ПроцедураID == procedureID
                                   && !string.IsNullOrEmpty(type) ? (type == m.Тип) : true);
        }

        public Ответ ОтветНа(string requestID)
        {
            return Ответы.SingleOrDefault(m => m.ЗапросID == requestID);
        }

        public IEnumerable<Ответ> ВсеВходящие(string procedureID, string type = null)
        {
            return Ответы.Where(m => m.ПроцедураID == procedureID
                                && !string.IsNullOrEmpty(type) ? (type == m.Тип) : true);
        }

        public IEnumerable<Публикация> ВсеПубликации(string procedureID)
        {
            return Публикации.Where(p => p.ПроцедураID == procedureID);
        }

        public Efrsb_message_record ПоследнееСообщениеЕФРСБ(string Id, string type)
        {
            return ЕФРСБ.LastOrDefault(m => m.DebtorId == Id && m.MessageType== type);
        }

        public Кредитор Кредитор(string ID)
        {
            return Кредиторы.SingleOrDefault(c => c.ID.ToString() == ID);
        }

        public Требование Требование(string ID)
        {
            return Требования.SingleOrDefault(c => c.ID == ID);
        }

        public IEnumerable<Требование> ВсеТребования(string procedureID, bool? included = null)
        {
            return Требования.Where(c => c.ПроцедураID == procedureID
                                    && included.HasValue? (included.Value == c.Включено) : true);
        }

        public IEnumerable<Собрание> ВсеСобрания(string procedureID)
        {
            return Собрания.Where(m => m.ПроцедураID == procedureID);
        }

        public Собрание ПоследнееСобрание(string procedureID)
        {
            return Собрания.LastOrDefault(m => m.ПроцедураID == procedureID);
        }

        public IEnumerable<Счет> ВсеСчета(string procedureID)
        {
            return Счета.Where(a => a.ПроцедураID == procedureID);
        }

        public IEnumerable<Торги> ВсеТорги(string procedureID)
        {
            return Торги.Where(s => s.ПроцедураID == procedureID);
        }

        public IEnumerable<Анализ> ВсеАнализы(string procedureID)
        {
            return Анализы.Where(a => a.ПроцедураID == procedureID);
        }

        public IEnumerable<Инвентаризация> ВсеИнвентаризации(string procedureID)
        {
            return Инвентаризации.Where(i => i.ПроцедураID == procedureID);
        }

        public string ЗарегистрироватьПроцедуру(Процедура proc)
        {
            proc.ID = guid.New.ToString("D");
            Процедуры.Add(proc);

            Денежные_срества.ТипУчетнойГруппы[] типы = {
                 Денежные_срества.ТипУчетнойГруппы.задаток,
                 Денежные_срества.ТипУчетнойГруппы.средстваАУ,
                 Денежные_срества.ТипУчетнойГруппы.залог
            };
            for(int i = 0; i<3;i++)
            {
                Денежные_срества.Учётная_группа группа = new Денежные_срества.Учётная_группа();
                группа.Дата_начального_сальдо = proc.Дата_начала;
                группа.Начальное_сальдо = 0;
                группа.учётная_группа = типы[i];
                ЗарегистрироватьНачальноеСальдо(proc.ID, группа);
            }

            return proc.ID;
        }

        public string ЗарегистрироватьИсходящее(Исходящее msg)
        {
            msg.ID = guid.New.ToString("D");
            Исходящие.Add(msg);
            return msg.ID;
        }

        public string ЗарегистрироватьВходящее(Ответ msg)
        {
            msg.ID = guid.New.ToString("D");
            Ответы.Add(msg);
            return msg.ID;
        }

        public IEnumerable<Исходящее> ВсеУведомления(string procedure, string type)
        {
            return Исходящие.Where(m => m.ПроцедураID == procedure
                                   && !string.IsNullOrEmpty(type) ? (type == m.Тип) : true);
        }

        public string ЗарегистрироватьПубликацию(Публикация pub)
        {
            var proc = Процедура(pub.ПроцедураID);
            if (proc != null)
            {
                Публикации.AddEntry(pub);
                return pub.ID;
            }
            throw new IndexOutOfRangeException();
        }

        public string ЗарегистрироватьКредитора(Кредитор creditor)
        {
            creditor.ID = guid.New.ToString("d");
            Кредиторы.AddEntry(creditor);
            return creditor.ID;
        }

        public string ЗарегистрироватьТребованиеКредитора(Требование claim)
        {
            claim.ID= guid.New.ToString("D");
            Требования.Add(claim);
            return claim.ID;
        }

        public string ЗарегистрироватьСобрание(Собрание meeting)
        {
            meeting.ID = guid.New.ToString("D");
            Собрания.Add(meeting);
            return meeting.ID;
        }

        public string ЗарегистрироватьСчёт(Счет account)
        {
            Счета.AddEntry(account);
            return account.ID;
        }

        public void ОбновитьСчёт(Счет account, decimal sum)
        {
            Счета.Single(a => a.Счёт == account.Счёт).Остаток = sum;
        }

        public string ЗарегистрироватьФинанализ(Анализ analysis)
        {
            Анализы.AddEntry(analysis);
            return analysis.ID;
        }

        public string ЗарегистрироватьИнвентаризацию(Инвентаризация inv)
        {
            Инвентаризации.AddEntry(inv);
            return inv.ID;
        }

        public string ЗарегистрироватьТорги(Торги sale)
        {
            Торги.AddEntry(sale);
            return sale.ID;
        }

        public void ЗакрытьТорги(Торги sale)
        {
            var rSale = ВсеТорги(sale.ПроцедураID).Single(s => s.ID == sale.ID);
            rSale.Завершен = true;
            rSale.ДатаЗавершения = sale.ДатаЗавершения;
        }

        public int ПолучитьПоследнююРевизииюЕфрсбСообщения(string debtor_id)
        {
            return ЕФРСБ.Count > 0 ? ЕФРСБ.Where(t => t.DebtorId == debtor_id).Last().Revision : 0; 
        }

        public void ДобавитьНовоеОбъявлениеЕФРСБ(string debtorId, Efrsb_message_record msg)
        {
            msg.DebtorId = debtorId;
            msg.Revision = ПолучитьПоследнююРевизииюЕфрсбСообщения(debtorId);
            ЕФРСБ.Add(msg);
        }

        public IEnumerable<Efrsb_message_record> ВсеЕфрсбСообщения(string debtorId)
        {
            return ЕФРСБ.Where(t=> t.DebtorId == debtorId);
        }

        public Efrsb_message_record ПолучитьЕфрсбСообщение(string debtorId, string type)
        {
            return ЕФРСБ.SingleOrDefault(t=> t.DebtorId == debtorId && t.MessageType == type);
        }

        public void Dispose()
        {
        }
    }
    
    public static class ContextEntriesExtensions
    {
        private static string NewID<T>(List<T> entries) where T : IИдентифицируемый_по_ID
        {
            if(entries.Count != 0) return (Int32.Parse(entries.LastOrDefault().ID) + 1).ToString();
            else return "1";
        }

        public static string AddEntry<T>(this List<T> entries, T entry) where T : IИдентифицируемый_по_ID
        {
            entry.ID = NewID(entries);
            entries.Add(entry);
            return entry.ID;
        }
    }
}
