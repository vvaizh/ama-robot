﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace ama_time_control
{
    public partial class TimeControlForm : Form
    {
        public TimeControlForm()
        {
            InitializeComponent();
        }

        AMA.Robot.TestFileTimeProvider provider = null;

        private void TimeControlForm_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(linkLabelTestTimeFile.Text))
                provider = new AMA.Robot.TestFileTimeProvider(linkLabelTestTimeFile.Text);
            if (!string.IsNullOrEmpty(linkLabelBookmarksFile.Text))
                LoadBookmarks();
            timer.Enabled= true;
        }

        class TimeBookmark
        {
            public DateTime Time;
            public string Title;
            public TimeBookmark(string line)
            {
                string[] line_part = line.Split('|');
                if(!String.IsNullOrEmpty(line_part[0]))
                {
                    Time = DateTime.Parse(line_part[0]);
                    Title = line_part[1];
                }
                
            }
            public static void Read(string filename, List<TimeBookmark> bookmarks)
            {
                using (TextReader r = new StreamReader(filename))
                {
                    for (string line = r.ReadLine(); null != line; line = r.ReadLine())
                        bookmarks.Add(new TimeBookmark(line));
                }
            }
            public static void Show(List<TimeBookmark> bookmarks, System.Windows.Forms.DataGridView g)
            {
                g.Rows.Clear();
                foreach (TimeBookmark b in bookmarks)
                {
                    g.Rows.Add(b.Time.ToShortDateString(), b.Time.ToString("HH:mm:ss"), b.Title);
                }
            }
        }

        void SetCurrentTime(DateTime dt)
        {
            dateTimePickerCurrentTestTime.Value = dt;
            if (null != provider)
                provider.SetTime(dt);
        }

        List<TimeBookmark> m_Bookmarks= new List<TimeBookmark>();
        void LoadBookmarks()
        {
            m_Bookmarks.Clear();
            TimeBookmark.Read(linkLabelBookmarksFile.Text, m_Bookmarks);
            TimeBookmark.Show(m_Bookmarks, dataGridViewBookmarks);
            SetCurrentTime(m_Bookmarks[0].Time);
        }

        private void linkLabelBookmarksFile_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = Environment.CurrentDirectory;
            dlg.Filter = "time bookmark files (*.tbm.txt)|*.tbm.txt|All files (*.*)|*.*"; ;
            dlg.CheckFileExists = true;
            dlg.Multiselect = false;
            if (DialogResult.OK == dlg.ShowDialog(this))
            {
                linkLabelBookmarksFile.Text = dlg.FileName;
                LoadBookmarks();
            }
        }

        private void linkLabelTestTimeFile_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = Environment.CurrentDirectory;
            dlg.Filter = "test time files (*.ttm.txt)|*.tbm.txt|All files (*.*)|*.*"; ;
            dlg.CheckFileExists = true;
            dlg.Multiselect = false;
            if (DialogResult.OK == dlg.ShowDialog(this))
            {
                linkLabelTestTimeFile.Text = dlg.FileName;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (!dateTimePickerCurrentTestTime.ContainsFocus && !string.IsNullOrEmpty(linkLabelTestTimeFile.Text) && null != provider)
                dateTimePickerCurrentTestTime.Value = provider.QuietNow;
        }

        private void dateTimePickerCurrentTestTime_ValueChanged(object sender, EventArgs e)
        {
            SetCurrentTime(dateTimePickerCurrentTestTime.Value);
        }

        private void dataGridViewBookmarks_SelectionChanged(object sender, EventArgs e)
        {
            if (0!=dataGridViewBookmarks.SelectedRows.Count)
            {
                int iselected= dataGridViewBookmarks.SelectedRows[0].Index;
                SetCurrentTime(m_Bookmarks[iselected].Time);
            }
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            if (0!=m_Bookmarks.Count)
            {
                int iselected = (0 == dataGridViewBookmarks.SelectedRows.Count)
                    ? -1 : dataGridViewBookmarks.SelectedRows[0].Index;
                iselected++;
                if (iselected < m_Bookmarks.Count)
                {
                    dataGridViewBookmarks.ClearSelection();
                    dataGridViewBookmarks.Rows[iselected].Selected= true;
                }
            }
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            if (0 != m_Bookmarks.Count)
            {
                int iselected = (0 == dataGridViewBookmarks.SelectedRows.Count)
                    ? m_Bookmarks.Count : dataGridViewBookmarks.SelectedRows[0].Index;
                iselected--;
                if (iselected >= 0)
                {
                    dataGridViewBookmarks.ClearSelection();
                    dataGridViewBookmarks.Rows[iselected].Selected = true;
                }
            }
        }
    }
}
