﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using ama_time_control.Properties;

namespace ama_time_control
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string [] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            TimeControlForm frm = new TimeControlForm();
            if (args.Length > 0)
                frm.linkLabelTestTimeFile.Text = new FileInfo(args[0]).FullName;
            if (args.Length > 1)
                frm.linkLabelBookmarksFile.Text = new FileInfo(args[1]).FullName;
            Application.Run(frm);
        }
    }
}
