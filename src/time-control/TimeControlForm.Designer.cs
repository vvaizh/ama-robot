﻿namespace ama_time_control
{
    partial class TimeControlForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelCurrentTestTime = new System.Windows.Forms.Label();
            this.dateTimePickerCurrentTestTime = new System.Windows.Forms.DateTimePicker();
            this.labelTimeBookmarks = new System.Windows.Forms.Label();
            this.dataGridViewBookmarks = new System.Windows.Forms.DataGridView();
            this.labelTestTimeFile = new System.Windows.Forms.Label();
            this.labelBookmarksFile = new System.Windows.Forms.Label();
            this.linkLabelTestTimeFile = new System.Windows.Forms.LinkLabel();
            this.linkLabelBookmarksFile = new System.Windows.Forms.LinkLabel();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBookmarks)).BeginInit();
            this.SuspendLayout();
            // 
            // labelCurrentTestTime
            // 
            this.labelCurrentTestTime.AutoSize = true;
            this.labelCurrentTestTime.Location = new System.Drawing.Point(12, 9);
            this.labelCurrentTestTime.Name = "labelCurrentTestTime";
            this.labelCurrentTestTime.Size = new System.Drawing.Size(139, 13);
            this.labelCurrentTestTime.TabIndex = 0;
            this.labelCurrentTestTime.Text = "Текущее тестовое время:";
            // 
            // dateTimePickerCurrentTestTime
            // 
            this.dateTimePickerCurrentTestTime.CustomFormat = "       dd.MM.yyyy          HH:mm:ss";
            this.dateTimePickerCurrentTestTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerCurrentTestTime.Location = new System.Drawing.Point(157, 5);
            this.dateTimePickerCurrentTestTime.Name = "dateTimePickerCurrentTestTime";
            this.dateTimePickerCurrentTestTime.Size = new System.Drawing.Size(199, 20);
            this.dateTimePickerCurrentTestTime.TabIndex = 2;
            this.dateTimePickerCurrentTestTime.ValueChanged += new System.EventHandler(this.dateTimePickerCurrentTestTime_ValueChanged);
            // 
            // labelTimeBookmarks
            // 
            this.labelTimeBookmarks.AutoSize = true;
            this.labelTimeBookmarks.Location = new System.Drawing.Point(12, 33);
            this.labelTimeBookmarks.Name = "labelTimeBookmarks";
            this.labelTimeBookmarks.Size = new System.Drawing.Size(106, 13);
            this.labelTimeBookmarks.TabIndex = 3;
            this.labelTimeBookmarks.Text = "Закладки времени:";
            // 
            // dataGridViewBookmarks
            // 
            this.dataGridViewBookmarks.AllowUserToAddRows = false;
            this.dataGridViewBookmarks.AllowUserToDeleteRows = false;
            this.dataGridViewBookmarks.AllowUserToResizeColumns = false;
            this.dataGridViewBookmarks.AllowUserToResizeRows = false;
            this.dataGridViewBookmarks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewBookmarks.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridViewBookmarks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBookmarks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date,
            this.Time,
            this.Title});
            this.dataGridViewBookmarks.Location = new System.Drawing.Point(15, 49);
            this.dataGridViewBookmarks.MultiSelect = false;
            this.dataGridViewBookmarks.Name = "dataGridViewBookmarks";
            this.dataGridViewBookmarks.ReadOnly = true;
            this.dataGridViewBookmarks.RowHeadersVisible = false;
            this.dataGridViewBookmarks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewBookmarks.Size = new System.Drawing.Size(457, 358);
            this.dataGridViewBookmarks.TabIndex = 4;
            this.dataGridViewBookmarks.SelectionChanged += new System.EventHandler(this.dataGridViewBookmarks_SelectionChanged);
            // 
            // labelTestTimeFile
            // 
            this.labelTestTimeFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelTestTimeFile.AutoSize = true;
            this.labelTestTimeFile.Location = new System.Drawing.Point(12, 440);
            this.labelTestTimeFile.Name = "labelTestTimeFile";
            this.labelTestTimeFile.Size = new System.Drawing.Size(109, 13);
            this.labelTestTimeFile.TabIndex = 5;
            this.labelTestTimeFile.Text = "Файл со временем:";
            // 
            // labelBookmarksFile
            // 
            this.labelBookmarksFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelBookmarksFile.AutoSize = true;
            this.labelBookmarksFile.Location = new System.Drawing.Point(12, 499);
            this.labelBookmarksFile.Name = "labelBookmarksFile";
            this.labelBookmarksFile.Size = new System.Drawing.Size(113, 13);
            this.labelBookmarksFile.TabIndex = 6;
            this.labelBookmarksFile.Text = "Файл с закладками:";
            // 
            // linkLabelTestTimeFile
            // 
            this.linkLabelTestTimeFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabelTestTimeFile.Location = new System.Drawing.Point(15, 461);
            this.linkLabelTestTimeFile.Name = "linkLabelTestTimeFile";
            this.linkLabelTestTimeFile.Size = new System.Drawing.Size(457, 32);
            this.linkLabelTestTimeFile.TabIndex = 7;
            this.linkLabelTestTimeFile.TabStop = true;
            this.linkLabelTestTimeFile.Text = "Не выбран";
            this.linkLabelTestTimeFile.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelTestTimeFile_LinkClicked);
            // 
            // linkLabelBookmarksFile
            // 
            this.linkLabelBookmarksFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabelBookmarksFile.Location = new System.Drawing.Point(18, 520);
            this.linkLabelBookmarksFile.Name = "linkLabelBookmarksFile";
            this.linkLabelBookmarksFile.Size = new System.Drawing.Size(454, 27);
            this.linkLabelBookmarksFile.TabIndex = 8;
            this.linkLabelBookmarksFile.TabStop = true;
            this.linkLabelBookmarksFile.Text = "Не выбран";
            this.linkLabelBookmarksFile.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelBookmarksFile_LinkClicked);
            // 
            // timer
            // 
            this.timer.Interval = 700;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNext.Location = new System.Drawing.Point(109, 413);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(363, 23);
            this.buttonNext.TabIndex = 9;
            this.buttonNext.Text = "Следующее";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonPrev
            // 
            this.buttonPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrev.Location = new System.Drawing.Point(15, 413);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(88, 23);
            this.buttonPrev.TabIndex = 10;
            this.buttonPrev.Text = "Предыдущее";
            this.buttonPrev.UseVisualStyleBackColor = true;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // Date
            // 
            this.Date.HeaderText = "Дата";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            this.Date.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Date.Width = 70;
            // 
            // Time
            // 
            this.Time.HeaderText = "Время";
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            this.Time.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Time.Width = 70;
            // 
            // Title
            // 
            this.Title.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Title.HeaderText = "Наименование";
            this.Title.Name = "Title";
            this.Title.ReadOnly = true;
            this.Title.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // TimeControlForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 562);
            this.Controls.Add(this.buttonPrev);
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.linkLabelBookmarksFile);
            this.Controls.Add(this.linkLabelTestTimeFile);
            this.Controls.Add(this.labelBookmarksFile);
            this.Controls.Add(this.labelTestTimeFile);
            this.Controls.Add(this.dataGridViewBookmarks);
            this.Controls.Add(this.labelTimeBookmarks);
            this.Controls.Add(this.dateTimePickerCurrentTestTime);
            this.Controls.Add(this.labelCurrentTestTime);
            this.MinimumSize = new System.Drawing.Size(500, 400);
            this.Name = "TimeControlForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Управление тестовым временем";
            this.Load += new System.EventHandler(this.TimeControlForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBookmarks)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCurrentTestTime;
        private System.Windows.Forms.DateTimePicker dateTimePickerCurrentTestTime;
        private System.Windows.Forms.Label labelTimeBookmarks;
        private System.Windows.Forms.DataGridView dataGridViewBookmarks;
        private System.Windows.Forms.Label labelTestTimeFile;
        private System.Windows.Forms.Label labelBookmarksFile;
        public System.Windows.Forms.LinkLabel linkLabelTestTimeFile;
        public System.Windows.Forms.LinkLabel linkLabelBookmarksFile;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrev;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title;
    }
}

