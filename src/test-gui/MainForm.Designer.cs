﻿namespace ama_robot_ztest_gui
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lTodo = new System.Windows.Forms.Label();
            this.lTask = new System.Windows.Forms.Label();
            this.lCommand = new System.Windows.Forms.Label();
            this.textBoxCommand = new System.Windows.Forms.TextBox();
            this.buttonExecute = new System.Windows.Forms.Button();
            this.linkLabelPath = new System.Windows.Forms.LinkLabel();
            this.labelTime = new System.Windows.Forms.Label();
            this.comboBoxTime = new System.Windows.Forms.ComboBox();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.buttonSettings = new System.Windows.Forms.Button();
            this.ucTodo = new AMA.Robot.UCTodo();
            this.ucTask = new AMA.Robot.UCTask();
            this.btnUpdateView = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lTodo
            // 
            this.lTodo.AutoSize = true;
            this.lTodo.Location = new System.Drawing.Point(12, 9);
            this.lTodo.Name = "lTodo";
            this.lTodo.Size = new System.Drawing.Size(46, 13);
            this.lTodo.TabIndex = 0;
            this.lTodo.Text = "Задачи:";
            // 
            // lTask
            // 
            this.lTask.AutoSize = true;
            this.lTask.Location = new System.Drawing.Point(338, 10);
            this.lTask.Name = "lTask";
            this.lTask.Size = new System.Drawing.Size(46, 13);
            this.lTask.TabIndex = 1;
            this.lTask.Text = "Задача:";
            // 
            // lCommand
            // 
            this.lCommand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lCommand.AutoSize = true;
            this.lCommand.Location = new System.Drawing.Point(9, 794);
            this.lCommand.Name = "lCommand";
            this.lCommand.Size = new System.Drawing.Size(55, 13);
            this.lCommand.TabIndex = 4;
            this.lCommand.Text = "Команда:";
            // 
            // textBoxCommand
            // 
            this.textBoxCommand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCommand.Location = new System.Drawing.Point(12, 810);
            this.textBoxCommand.Multiline = true;
            this.textBoxCommand.Name = "textBoxCommand";
            this.textBoxCommand.Size = new System.Drawing.Size(911, 39);
            this.textBoxCommand.TabIndex = 5;
            // 
            // buttonExecute
            // 
            this.buttonExecute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExecute.Location = new System.Drawing.Point(929, 826);
            this.buttonExecute.Name = "buttonExecute";
            this.buttonExecute.Size = new System.Drawing.Size(75, 23);
            this.buttonExecute.TabIndex = 6;
            this.buttonExecute.Text = "Выполнить";
            this.buttonExecute.UseVisualStyleBackColor = true;
            this.buttonExecute.Click += new System.EventHandler(this.buttonExecute_Click);
            // 
            // linkLabelPath
            // 
            this.linkLabelPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabelPath.Location = new System.Drawing.Point(70, 794);
            this.linkLabelPath.Name = "linkLabelPath";
            this.linkLabelPath.Size = new System.Drawing.Size(853, 13);
            this.linkLabelPath.TabIndex = 7;
            this.linkLabelPath.TabStop = true;
            this.linkLabelPath.Text = "Путь к файлам заготовок";
            this.linkLabelPath.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelTime
            // 
            this.labelTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTime.AutoSize = true;
            this.labelTime.Location = new System.Drawing.Point(638, 5);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(43, 13);
            this.labelTime.TabIndex = 8;
            this.labelTime.Text = "Время:";
            // 
            // comboBoxTime
            // 
            this.comboBoxTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTime.FormattingEnabled = true;
            this.comboBoxTime.Items.AddRange(new object[] {
            "Тестовое",
            "Реальное",
            "Тестовое из файла"});
            this.comboBoxTime.Location = new System.Drawing.Point(687, 2);
            this.comboBoxTime.Name = "comboBoxTime";
            this.comboBoxTime.Size = new System.Drawing.Size(135, 21);
            this.comboBoxTime.TabIndex = 9;
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker.Location = new System.Drawing.Point(824, 3);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(93, 20);
            this.dateTimePicker.TabIndex = 10;
            // 
            // buttonSettings
            // 
            this.buttonSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSettings.Location = new System.Drawing.Point(930, 1);
            this.buttonSettings.Name = "buttonSettings";
            this.buttonSettings.Size = new System.Drawing.Size(75, 23);
            this.buttonSettings.TabIndex = 11;
            this.buttonSettings.Text = "Настройки";
            this.buttonSettings.UseVisualStyleBackColor = true;
            // 
            // ucTodo
            // 
            this.ucTodo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ucTodo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucTodo.Location = new System.Drawing.Point(15, 25);
            this.ucTodo.Name = "ucTodo";
            this.ucTodo.PathToHtml = null;
            this.ucTodo.Size = new System.Drawing.Size(334, 734);
            this.ucTodo.TabIndex = 12;
            this.ucTodo.TaskSelected += new AMA.Robot.UCTodo.OnTaskSelected(this.ucTodo_TaskSelected);
            // 
            // ucTask
            // 
            this.ucTask.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ucTask.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucTask.Location = new System.Drawing.Point(355, 25);
            this.ucTask.Name = "ucTask";
            this.ucTask.PathToHtml = null;
            this.ucTask.Size = new System.Drawing.Size(650, 734);
            this.ucTask.TabIndex = 13;
            this.ucTask.TaskDone += new AMA.Robot.UCTask.OnTaskDroped(this.ucTask_TaskDone);
            this.ucTask.TaskSkiped += new AMA.Robot.UCTask.OnTaskDroped(this.ucTask_TaskSkiped);
            this.ucTask.EfrsbComplete += new AMA.Robot.UCTask.OnTaskDroped(this.ucTask_EfrsbComplete);
            this.ucTask.EfrsbChecking += new AMA.Robot.UCTask.OnCheckEfrsb(this.ucTask_EfrsbChecking);
            // 
            // btnUpdateView
            // 
            this.btnUpdateView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUpdateView.DialogResult = System.Windows.Forms.DialogResult.Retry;
            this.btnUpdateView.Location = new System.Drawing.Point(15, 766);
            this.btnUpdateView.Name = "btnUpdateView";
            this.btnUpdateView.Size = new System.Drawing.Size(111, 23);
            this.btnUpdateView.TabIndex = 14;
            this.btnUpdateView.Text = "Обновить данные";
            this.btnUpdateView.UseVisualStyleBackColor = true;
            this.btnUpdateView.Click += new System.EventHandler(this.btnUpdateView_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 861);
            this.Controls.Add(this.btnUpdateView);
            this.Controls.Add(this.ucTask);
            this.Controls.Add(this.ucTodo);
            this.Controls.Add(this.buttonSettings);
            this.Controls.Add(this.dateTimePicker);
            this.Controls.Add(this.comboBoxTime);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.linkLabelPath);
            this.Controls.Add(this.buttonExecute);
            this.Controls.Add(this.textBoxCommand);
            this.Controls.Add(this.lCommand);
            this.Controls.Add(this.lTask);
            this.Controls.Add(this.lTodo);
            this.MinimumSize = new System.Drawing.Size(741, 200);
            this.Name = "MainForm";
            this.Text = "тестовый стенд для делопроизводящей машины ПАУ";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lTodo;
        private System.Windows.Forms.Label lTask;
        private System.Windows.Forms.Label lCommand;
        private System.Windows.Forms.TextBox textBoxCommand;
        private System.Windows.Forms.Button buttonExecute;
        private System.Windows.Forms.LinkLabel linkLabelPath;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.ComboBox comboBoxTime;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.Button buttonSettings;
        public AMA.Robot.UCTodo ucTodo;
        public AMA.Robot.UCTask ucTask;
        private System.Windows.Forms.Button btnUpdateView;
    }
}

