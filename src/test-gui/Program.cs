﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;

namespace ama_robot_ztest_gui
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            string relative_path_to_html_folder = @"..\..\..\Robot-gui-lib\phtml\";
            string path_to_app_exe_folder = Path.GetDirectoryName(Application.ExecutablePath);
            string path_to_html_folder = Path.Combine(path_to_app_exe_folder, relative_path_to_html_folder);

            MainForm frm = new MainForm();
            frm.ucTodo.PathToHtml = Path.Combine(path_to_html_folder, "todo.html");
            frm.ucTask.PathToHtml = Path.Combine(path_to_html_folder, "task.html");

            string test_time_path = System.Configuration.ConfigurationManager.AppSettings["test_time_path"];
            if (!string.IsNullOrEmpty(test_time_path))
            {
                //time_provider = new ama.Robot.TestFileTimeProvider(test_time_path);
            }

            Application.Run(frm);
        }
    }
}
