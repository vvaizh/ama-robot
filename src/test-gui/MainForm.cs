﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Web.Script.Serialization;
using AMA.Robot.cmd;
using AMA.Robot;
using AMA.Robot.mdb;



namespace ama_robot_ztest_gui
{
    public partial class MainForm : Form
    {
        private Todo todo;
        private static Executor executor;
		private static Материалы материалы;

		public MainForm()
        {
            InitializeComponent();
            try
            {
                AMA.Robot.task.Постановщик factory = new AMA.Robot.task.Постановщик();
                ITimeProvider time = (ITimeProvider)new ActualTimeProvider();
                IGuidProvider guids = (IGuidProvider)new ActualGuidProvider();
                using (IДела_в_работе дело = (IДела_в_работе)new Дело(guids, "../../../test-console/tests/mdb/ama.mdb", time))
                {
                    AMA.Robot.task.Планировщик постановщик = new AMA.Robot.task.Планировщик(factory, дело, дело, time);
                    материалы = new Материалы { дела = дело, регистратор = дело, time = time, постановщик = постановщик, guids = guids };
                    executor = new AMA.Robot.cmd.Executor(материалы, null);
                }
                todo = new Todo
                {
                    Процедуры = ВсеПроцедуры(),
                    Задачи = ВсеЗадачи()
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private List<object> ВсеПроцедуры()
        {
            return executor.ExecuteCommand("tproc", null);
        }

        private List<object> ВсеЗадачи()
        {
            return executor.ExecuteCommand("atasks", null);
        }

        Задача Найти_задачу(string id_task, string id_procedure)
        {
            List<object> задачи = todo.Задачи;
            foreach (Задача задача in задачи)
            {
                if (задача.ЗадачаID == id_task && задача.ПроцедураID == id_procedure)
                    return задача;
            }
            return null;
        }

        void Удалить_задачу(string id_task, string id_procedure, string comands, string taskResult)
        {
            Задача задача = Найти_задачу(id_task, id_procedure);
            executor.ExecuteCommand(string.Format("{0} {1} {2} {3}", comands, id_procedure, задача.Формулировка.ToString().Replace(' ', '_'), taskResult), null);
            var задачи = new object[todo.Задачи.Count() - 1];
            var j = 0;
            for (var i = 0; i < todo.Задачи.Count(); i++)
            {
                задача = (Задача)todo.Задачи[i];
                if (задача.ЗадачаID != id_task || задача.ПроцедураID != id_procedure)
                    задачи[j++] = задача;
            }
            todo.Задачи = ВсеЗадачи();
        }

        Процедура Найти_процедуру(string id_procedure)
        {
            List<object> процедуры = todo.Процедуры;
            foreach (Процедура процедура in процедуры)
            {
                if (процедура.ID == id_procedure)
                    return процедура;
            }
            return null;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.comboBoxTime.SelectedItem = "Тестовое";
            this.ucTask.LoadHtml();
            this.ucTodo.LoadHtml();
            LoadTodo();
        }

        void LoadTodo()
        {
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            string txt_json_tasks = jsonSerializer.Serialize(todo);
            this.ucTodo.LoadTasks(txt_json_tasks);
        }

        private void ucTodo_TaskSelected(string id_task, string id_procedure)
        {
            var task = new
            {
                Задача = Найти_задачу(id_task, id_procedure)
                ,Процедура = Найти_процедуру(id_procedure)
            };

            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            string txt_json_task = jsonSerializer.Serialize(task);
            string user_information = @"{""users"":{""Пользователь"":{
                ""фамилия"":""Иванов"",
			    ""имя"":""И"",
			    ""отчество"":""И""},
		        ""АрбитражныйУправляющий"":{
                ""фамилия"":""Петров"",
			    ""имя"":""П"",
			    ""отчество"":""П""}},
		        ""ТекстСообщения"":""Тут хранится текст сообщения"",";
            txt_json_task = user_information + txt_json_task.Substring(1);
            this.ucTask.LoadTask(txt_json_task);
        }

        private void ucTask_TaskDone(string id_task, string id_procedure, string description)
        {
            Удалить_задачу(id_task, id_procedure, "rtask", "2");
            DropTask("сделано", id_task, id_procedure, description);
        }

        private void ucTask_TaskSkiped(string id_task, string id_procedure, string description)
        {
            Удалить_задачу(id_task, id_procedure, "rtask", "1");
            DropTask("пропущено", id_task, id_procedure, description);
        }

        void DropTask(string action, string id_task, string id_procedure, string description)
        {
            var Задача = Найти_задачу(id_task, id_procedure);
            var Процедура = Найти_процедуру(id_procedure);
            ucTask.ClearTask();

            LoadTodo();
        }

        private void btnUpdateView_Click(object sender, EventArgs e)
        {
            todo.Задачи = ВсеЗадачи();
            ucTask.ClearTask();
            LoadTodo();
        }

        private void btnRepeatTask_Click(object sender, EventArgs e)
        {
            executor.ExecuteCommand("treturn", null);
        }

		private object ucTask_EfrsbChecking(string id_task, string id_procedure)
        {
            Задача задача = Найти_задачу(id_task, id_procedure);
            List<object> messages = executor.ExecuteCommand(String.Format("chefrsb {0} {1}", id_procedure, задача.Формулировка.ToString().Replace(' ', '_')), null);
            
            JavaScriptSerializer scriptSerializer = new JavaScriptSerializer();
            
            if (messages.Count() > 0)
            {
                string json = scriptSerializer.Serialize(messages);
                return json;
            }
            else
            {
                return null;
            }
        }

        private void ucTask_EfrsbComplete(string id_task, string id_procedure, string description)
        {
            MessageBox.Show("Запустили процесс отправки сообщения на ЕФРСБ");
        }

        private void buttonExecute_Click(object sender, EventArgs e)
        {
            executor.ExecuteCommand(textBoxCommand.Text, null);
        }
    }

    public class Todo
    {
        public List<object> Процедуры;
        public List<object> Задачи;
    }
}

