﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;

namespace AMA.Robot
{
    [ComVisible(true)]
    public partial class UCTodo : UserControl
    {
        public string PathToHtml { get; set; }
		BackgroundWorker m_BackgrowndWorker;
		WebBrowser m_Browser;

		object ajax_options = null;
		public UCTodo()
        {
            InitializeComponent();
			m_Browser = webBrowser;
			m_BackgrowndWorker = new BackgroundWorker();
			m_BackgrowndWorker.DoWork += m_BackgrowndWorker_DoWork;
			m_BackgrowndWorker.RunWorkerAsync();
		}
		void m_BackgrowndWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
		{
			for (; ; )
			{
				object options = ajax_options;
				if (null == options)
				{
					Thread.Sleep(10);
				}
				else
				{
					ajax_options = null;
					m_Browser.Invoke(new System.Windows.Forms.MethodInvoker(delegate {
						m_Browser.Document.InvokeScript("ajax_callback", new object[] { });
					}));
				}
			}
		}

		public void ajax(object options, bool ch_efrsb)
		{
			ajax_options = options;
		}
		public void LoadHtml()
        {
            webBrowser.Navigate(new Uri(PathToHtml, System.UriKind.Absolute));
        }

        string initial_txt_json = null;
        bool DocumentIsCompleted = false;
        private void webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            webBrowser.ObjectForScripting = this;
            if (!DocumentIsCompleted)
            {
                DocumentIsCompleted = true;
                if (!string.IsNullOrEmpty(initial_txt_json))
                    LoadTasks(initial_txt_json);
            }
        }

        public void LoadTasks(string txt_json)
        {
            if (!DocumentIsCompleted)
            {
                initial_txt_json = txt_json;
            }
            else
            {
                initial_txt_json = null;
                webBrowser.Document.InvokeScript("ama_todo_LoadTasks", new object[] { txt_json });
            }
        }

        public delegate void OnTaskSelected(string id_task, string id_procedure) ;
        public event OnTaskSelected TaskSelected;

        public bool SelectTask(string id_task, string id_procedure)
        {
            if (null != TaskSelected)
                TaskSelected(id_task, id_procedure);
            return true;
        }
    }
}
