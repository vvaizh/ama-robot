﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.Net;

namespace AMA.Robot
{
    [ComVisible(true)]
    public partial class UCTask : UserControl
    {
        public string PathToHtml { get; set; }
		BackgroundWorker m_BackgrowndWorker;
		WebBrowser m_Browser;

		object ajax_options = null;
		bool chEfrsbCommand = false;
		public UCTask()
        {
            InitializeComponent();
            webBrowser.ObjectForScripting = this;
			m_Browser = webBrowser;
			m_BackgrowndWorker = new BackgroundWorker();
			m_BackgrowndWorker.DoWork += m_BackgrowndWorker_DoWork;
			m_BackgrowndWorker.RunWorkerAsync();
		}

		void m_BackgrowndWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
		{
			for (; ; )
			{
				object options = ajax_options;
				if (null == options)
				{
					Thread.Sleep(10);
				}
				else
				{
					ajax_options = null;
					m_Browser.Invoke(new MethodInvoker(delegate
					{
						if(chEfrsbCommand)
						{
							chEfrsbCommand = false;
							webBrowser.Document.InvokeScript("wait_function");
						}
						webBrowser.Document.InvokeScript("ajax_callback");
					}));
				}
			}
		}

		public void ajax(object options, bool ch_efrsb)
		{
			ajax_options = options;
			chEfrsbCommand = ch_efrsb;
		}

		public void LoadHtml()
        {
            webBrowser.Navigate(new Uri(PathToHtml, System.UriKind.Absolute));
        }

        public void LoadTask(string txt_json)
        {
            webBrowser.Document.InvokeScript("ama_todo_LoadTask", new object[] { txt_json });
        }

        public void ClearTask()
        {
            webBrowser.Document.InvokeScript("ama_todo_ClearTask");
        }

        public delegate void OnTaskDroped(string id_task, string id_procedure, string description);
        public delegate object OnCheckEfrsb(string id_task, string id_procedure);
        public delegate string OnGetSomeInformation(string id_task, string id_procedure);
        public event OnTaskDroped TaskDone;

        public bool DoneTask(string id_task, string id_procedure, string description)
        {
            if (null != TaskDone)
                TaskDone(id_task, id_procedure, description);
            return true;
        }

        public event OnTaskDroped TaskSkiped;
        public bool SkipTask(string id_task, string id_procedure, string description)
        {
            if (null != TaskSkiped)
                TaskSkiped(id_task, id_procedure, description);
            return true;
        }

        public event OnTaskDroped EfrsbComplete;
        public bool CompleteEfrsb(string id_task, string id_procedure, string description)
        {
            if (null != EfrsbComplete)
                EfrsbComplete(id_task, id_procedure, description);
            return true;
            
        }

		

        public event OnCheckEfrsb EfrsbChecking;
        public object CheckEfrsb(string id_task, string id_procedure)
        {
            if (null != EfrsbChecking)
               return EfrsbChecking(id_task, id_procedure);

            return null;
        }

        public event OnGetSomeInformation GetSomeInformation;
        public string GetMoreInformation(string id_task, string id_procedure)
        {
            if (null != GetSomeInformation)
                return GetSomeInformation(id_task, id_procedure);

            return "";
        }

    }
}
