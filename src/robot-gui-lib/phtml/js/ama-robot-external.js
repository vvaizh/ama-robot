﻿
var ama_robot_ajax_function= function(options)
{
    var decodec = ama_url_args_decodec();
	var args = decodec.Decode(options.url);
	var dargs = !options.data ? null : decodec.Decode(options.data);
	switch (args.action)
	{
		case 'select': return window.external.SelectTask(args.id_task, args.id_procedure);
	    case 'done': return window.external.DoneTask(dargs.id_task, dargs.id_procedure, dargs.description);
        case 'skip': return window.external.SkipTask(dargs.id_task, dargs.id_procedure, dargs.description);
        case 'co_efrsb': return window.external.CompleteEfrsb(dargs.id_task, dargs.id_procedure, dargs.description);
        case 'ch_efrsb': return window.external.CheckEfrsb(dargs.id_task, dargs.id_procedure);
	}
	return null;
}

$(function ()
{
	$.ajaxTransport
	(
		'+*',
		function (options, originalOptions, jqXHR)
		{
			if (0 == options.url.indexOf('ama/robot/task'))
			{
				var external_ajax_transport =
				{
					send: function (headers, completeCallback)
					{
						window.ajax_callback = function (status, responceText) {
							if (options.url === 'ama/robot/task?action=ch_efrsb') {
								setTimeout(function () {
									var res = ama_robot_ajax_function(options);
									completeCallback(200, 'success', { text: JSON.stringify(res) });
								}, 1000);
							}
							else {
								var res = ama_robot_ajax_function(options);
								completeCallback(200, 'success', { text: JSON.stringify(res) });
							}
						}
						if (options.url === 'ama/robot/task?action=ch_efrsb') {
							window.external.ajax(options, true);
						} else {
							window.external.ajax(options, false);
						}
						
					}
					, abort: function ()
					{
					}
				}
				return external_ajax_transport;
			}
		}
	);
});