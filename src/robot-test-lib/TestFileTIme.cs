﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace AMA.Robot
{
    public class TestFileTimeProvider : ITimeProvider
    {
        string filename;
        public TestFileTimeProvider(string filename)
        {
            this.filename = filename;
        }

        const int milliseconds_to_wait_mutex = 100;
        static readonly Mutex mutex = new Mutex(false, "ama.Robot.file-test-time");

        public DateTime Now
        {
            get
            {
                return GetNowTime(add_second: true);
            }
        }

        public DateTime QuietNow
        {
            get
            {
                return GetNowTime(add_second: false);
            }
        }

        public void SetTime(DateTime dt)
        {
            if (!mutex.WaitOne(milliseconds_to_wait_mutex))
            {
                throw new Exception("can no wait for test file!");
            }
            else
            {
                try
                {
                    File.WriteAllText(filename, dt.ToString());
                }
                finally
                {
                    mutex.ReleaseMutex();
                }
            }
        }

        DateTime GetNowTime(bool add_second)
        {
            if (!mutex.WaitOne(milliseconds_to_wait_mutex))
            {
                throw new Exception("can no wait for test file!");
            }
            else
            {
                try
                {
                    string txt_current_time = File.ReadAllText(filename);
                    DateTime dt = DateTime.Parse(txt_current_time);
                    if (add_second)
                        File.WriteAllText(filename, dt.AddSeconds(1).ToString());
                    return dt;
                }
                finally
                {
                    mutex.ReleaseMutex();
                }
            }
        }
    }
}
