﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMA.Robot
{
    public class TestGuidProvider : IGuidProvider
    {
        static Guid[] guids = new Guid[]
        {
            new Guid("D571B33D-F21C-4859-A15B-2F510F649454")
            ,new Guid("D825C661-4A11-44AD-A6A2-42C00CCAD3B1")
            ,new Guid("81AD72C8-B70A-4E8C-8730-6A246B2A9AAD")
            ,new Guid("05B94733-B3E8-4575-8F69-FB2872CE8A75")
            ,new Guid("5C28C1AF-BC30-4783-A2D0-3505F8AE183F")
            ,new Guid("02E2554F-2C9E-43C9-BE42-6EE5B20D2032")
            ,new Guid("0818574D-8BCF-4648-A009-E3B1D962F75B")
            ,new Guid("B998A38A-66FB-4CBE-8A45-D3AB7A18B30B")
            ,new Guid("7B7747A8-8D4E-4C4E-B666-0C5CB26F7731")
            ,new Guid("C7DF9FD6-D675-4CCA-9DC3-FA1CD91C3FC0")
            ,new Guid("DD5ED12F-63CA-4781-A488-4EBCE1F3C614")
            ,new Guid("4E8B58A3-7DEE-4B50-B10E-C94D5BEF1EDC")
            ,new Guid("3205A5BD-4CCD-43B9-AFB8-B6569D970029")
            ,new Guid("697F349C-C297-402B-9D3D-CAE12A981635")
            ,new Guid("C17E9270-6DFD-4F07-8789-4CC01D01C565")
            ,new Guid("0861B181-7122-4DCD-8EC1-DF5389E088E1")
            ,new Guid("DF314A87-F431-47E0-8161-F707964B3C2A")
            ,new Guid("E86F3826-9651-4DAF-98B2-1AC5A3EEE0D9")
            ,new Guid("A8EB57FD-0EE1-4E19-BC38-B07B0947C035")
            ,new Guid("CCBA7FB8-44C8-471C-9466-1FB522E71D0F")
            ,new Guid("0F33FC27-211F-4FEE-B60D-98A08C70AC63")
            ,new Guid("C3AEE940-BF01-4520-921B-A5CD1CE78CE8")
            ,new Guid("373390D5-B1EF-4AE1-BB73-E8B925DEE4CE")
            ,new Guid("52D2EDA5-D3C9-4100-B223-5D70C254DACB")
            ,new Guid("8FEFAB1D-F5BD-4192-BB99-1074EBBE92B8")
            ,new Guid("BC0A59BA-B0E7-42F4-B5C8-FC8693245D84")
            ,new Guid("5E7E8818-CA62-426F-B746-1DAD5746B021")
            ,new Guid("3F71BC48-B556-4472-8BD4-DAEDF111C17B")
            ,new Guid("0080E598-FACE-415D-8F6C-A353675859A0")
            ,new Guid("C7E56256-B478-4B17-90A1-9B97EFBF274F")
            ,new Guid("944B0E31-7495-4703-A56F-929A3137F82F")
            ,new Guid("F0EF30BB-62B8-4A32-A101-502286151CDD")
            ,new Guid("7D3472E3-5C23-45E1-8A72-CF19F6488265")
            ,new Guid("3E945CE8-A681-40FF-B44D-132E701B5FC3")
            ,new Guid("AC9434B9-6215-45AC-9134-811E96CC20CB")
            ,new Guid("AA078E09-BE2C-40E1-B298-D57E1C3DA701")
            ,new Guid("16F70409-D109-474B-A183-A35160C65C85")
            ,new Guid("51807A7C-CFD6-4FD4-A0B4-BC8CCEAFA2AE")
            ,new Guid("2FAFBE17-B56A-4BBA-AA34-C65AE08D92D9")
            ,new Guid("6212FCEF-897C-4EA1-89E3-68BA7F5DC48B")
            ,new Guid("227A2F07-64D9-448F-A294-DD48A430E823")
            ,new Guid("ECD4436D-4087-479B-A203-8D8ADDCDCC96")
            ,new Guid("EA3DD75F-B8DC-4B77-9001-A6176DD3A4C3")

            ,new Guid("542D2093-CC28-4725-BF31-4D3A2718E19F")
            ,new Guid("330BB62D-8DC6-4ADD-A7D6-32D98C2E90E2")
            ,new Guid("CEF49BF0-82F4-42F5-8505-B118C916328D")
            ,new Guid("949E07FD-5536-4E60-B88F-3804AF724CC6")
            ,new Guid("2AE47020-7C61-4163-840E-F16729701399")
            ,new Guid("FD862E3A-BED5-46A0-AD46-3DB6BC2880DE")
            ,new Guid("398AB406-4171-41D5-8BD2-68DAF95D53BD")
            ,new Guid("14FCBAE1-2625-4656-BAA5-46AF16164248")
            ,new Guid("312D1AD7-7E15-4612-BF81-5AAF9B925CA8")
            ,new Guid("4D84DA29-61ED-44D0-8FA7-F0F9CB8AADE9")
            ,new Guid("AC56EBF9-F56E-40A9-A655-3C0B0704DD4F")
            ,new Guid("3D620F2C-CB6E-40E2-918E-4717E17FC4EB")
            ,new Guid("097A3FBE-C855-4996-9520-052263BBF2C2")
            ,new Guid("6982D24F-1EBE-4FC2-9E7C-2943F2DBD07E")
            ,new Guid("6A22FCD2-D6A8-4E35-A3FD-3AB7AA338F41")
            ,new Guid("005C9988-7219-4E83-88AF-06CC59D3B2D1")
            ,new Guid("AFB94395-584D-497D-90AC-AB6B85F9B772")
            ,new Guid("70BB6129-B505-438C-A233-2CA872C2395A")
            ,new Guid("AB1B905F-BDAD-426B-B557-367FF2638FE3")
            ,new Guid("712112D1-0A50-46CC-9980-51081C9E810E")
            ,new Guid("CC915ED3-C111-41F3-8E68-540A0D505CD7")
            ,new Guid("C0D99065-3FCF-4013-92AB-F8CBBF580D85")
            ,new Guid("286D587B-7C1E-4A0B-912F-BB7DB3F87039")
            ,new Guid("DDE2FC5F-7E0D-46A8-9740-6F8CC2A897C7")
            ,new Guid("C25FB3C1-7AE0-4B6F-BBC8-4185C58F9C24")
            ,new Guid("5F177F22-7007-4758-99A6-DBFD56CAA8E0")
            ,new Guid("6673EE68-2C7E-4CCD-9164-F28922E147EB")
            ,new Guid("8BFAD543-273C-4BD4-BFF2-2006715C1A00")
            ,new Guid("940216C4-85EC-4446-AA63-E3A348AF0A30")
            ,new Guid("BBF25AE5-D8C5-4522-ACC7-F09D2F04146B")
            ,new Guid("E2FBF164-F693-46C9-BF3A-C01B43DAACE6")
            ,new Guid("A1C88525-B11A-4F1A-B5DC-DB550D8A28D9")
            ,new Guid("87622759-3F6A-43A9-B568-57FBFAFE39F5")
            ,new Guid("C5E3F609-75C1-48CF-A8E5-FC6A5707BB3F")
            ,new Guid("EF6F2E10-7B04-4D1B-B66E-BAD6D0687FCB")
            ,new Guid("23C2AD0B-D9DC-4788-BA6E-EBE137840D1A")
            ,new Guid("FE6C2ECF-F219-42BE-AC5F-D555A6DFB031")
            ,new Guid("B3655D79-7009-4C5D-A0C8-D92A4EE06057")
            ,new Guid("569BB009-090F-494C-ACD1-BB73C51A30B2")
            ,new Guid("A029E145-B375-4AD9-9DDF-16539077DA5E")
            ,new Guid("8D11C210-BC2F-4D2E-BA47-9BA21DAD887B")
            ,new Guid("BC981025-6E82-4775-9B2F-1C86BE62875A")
            ,new Guid("C04E602A-1CC5-4F4E-BAEF-695D8B39A145")
            ,new Guid("6995088F-55B6-45C9-B1AC-8A7541A912B7")
            ,new Guid("39BDAA35-8F39-4694-9F27-7601793BB8F1")
            ,new Guid("47E7F77D-7A92-4D03-85BA-894524291256")
            ,new Guid("556CEA3B-7C56-48A7-AA15-3D1F2F6977E2")
            ,new Guid("AD065577-1FC4-4D84-B355-993641277524")
            ,new Guid("173248C9-26B8-4004-B786-B27F95DACCAE")
            ,new Guid("40C758CC-8C5F-4CAC-9E61-8AC347B757A4")
            ,new Guid("0FAD5DB0-8E4C-4873-8D99-1828C896FC0A")
            ,new Guid("F930F1DD-A7D0-4950-9FAB-4BF826805EE2")
        };



        private int count = 0;

        public Guid New
        {
            get
            {
                lock (this)
                {
                    return guids[count++];
                }
            }
        }

        public string unbeauty(string txt_g)
        {
            int i;
            string res= (!int.TryParse(txt_g, out i) || i<1 || i>guids.Length) ? txt_g : guids[i-1].ToString("D");
            return res;
        }

        public string beauty(string txt_g)
        {
            Guid guid = new Guid(txt_g);
            int i= 1;
            foreach (Guid g in guids)
            {
                if (guid == g)
                    return i.ToString();
                i++;
            }
            return txt_g;
        }
    }
}
