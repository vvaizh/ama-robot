﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace AMA.Robot
{
    public class TestTimeProvider : ITimeProvider
    {
        private DateTime startDate = new DateTime(1992, 4, 28);
        private int count = 0;      
  
        public DateTime Now
        {
            get
            {
                lock(this)
                {
                    return startDate.AddSeconds(count++);
                }
            }
        }

        public void SetDateTime(DateTime dt)
        {
            startDate = dt;
            count = 0;
        }
    }
}
