﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace AMA.Robot.cmd
{
    public class Executor
    {
        readonly Материалы материалы;
        string Datadir;
        public static List<object> ListResult = new List<object>(); 

        public Executor(Материалы материалы, string Datadir)
        {
            this.материалы = материалы;
            this.Datadir = Datadir;
        }

        public List<object> ExecuteCommand(string txt_command_line, TextWriter os)
        {
            List<object> result = new List<object>();
            if (txt_command_line == null || txt_command_line.Trim().Length == 0)
                return result;

            string [] args = txt_command_line.Split(' ');
            string txt_cmd = args[0];
            switch (txt_cmd)
            {
                case "help":
                case "h": os.WriteLine(ListCommands()); break;

                case "quit":
                case "exit":
                case "q": Environment.Exit(0); break;

                default:
                    result = ExecuteCommand(txt_cmd, args, os); 
                    break;
            }

            return result;
        }

        List<object> ExecuteCommand(string txt_cmd, string [] args, TextWriter os)
        {
            ICommand cmd = AMA.Robot.cmd.Factory.Get(txt_cmd);
            if (null != cmd)
            {
                cmd.Datadir = Datadir;
                //cmd.Execute(материалы, args, os);

                return cmd.Execute(материалы, args, os);
            }
            else
            {
                os.Write("Нераспознанная команда: ");
                os.WriteLine(args[0]);
                os.WriteLine(ListCommands());
                return new List<object>();
            }
        }

        public string ListCommands()
        {
            var sb = new StringBuilder("Доступные команды:\r\n");
            sb.AppendLine("help\t\t\tПоказать список доступных команд");
            sb.AppendLine("quit\t\t\tВыйти из программы");
            foreach (var d in cmd.Factory.Descriptions)
                sb.AppendLine(d);

            return sb.ToString();
        }
    }
}