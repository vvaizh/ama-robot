﻿using ama.efrsb;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;


namespace AMA.Robot.cmd
{
    class JsonSerrilization
    {
        public static JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    internal class CommandAttribute : Attribute
    {
        public CommandAttribute(string Abbreviation)
        {
            this.Abbreviation = Abbreviation;
        }

        public string Abbreviation { get; set; }
    }

    public interface ICommand
    {
        string Description { get; }
        string Datadir { get; set; }
        List<object> Execute(Материалы материалы, string[] arguments, TextWriter os);
    }

    internal abstract class ACommand : ICommand
    {
        public string Datadir { get; set; }

        public abstract string Description { get; }
        public abstract List<object> Execute(Материалы материалы, string[] arguments, TextWriter os);

        protected string ReadArgTextFile(string fname)
        {
            try
            {
                return File.ReadAllText(fname, Encoding.UTF8);
            }
            catch (System.IO.DirectoryNotFoundException)
            {
                return File.ReadAllText(Path.Combine(Datadir, fname), Encoding.UTF8);
            }
            catch (System.IO.FileNotFoundException)
            {
                return File.ReadAllText(Path.Combine(Datadir, fname), Encoding.UTF8);
            }
        }
    }

    [Command("commis")]
    internal class BankCommission : ACommand
    {
        public override string Description
        {
            get { return "creac\t<manproc_id>\t<account_group>\t<commission>\tЗарегистрировать коммиссию банка"; }
        }
        void Append_адрес(StringBuilder sb, Денежные_срества.Адрес адрес)
        {
            if (Денежные_срества.ТипУчетнойГруппы.Деньги_третьих_лиц != адрес.учётная_группа)
            {
                sb.Append(адрес.TypeToString());
            }
            else
            {
                sb.Append(адрес.контрагент.Наименование);
            }
        }
        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            string id_процедуры = материалы.guids.unbeauty(args[1]);
            Процедура proc = материалы.дела.Процедура(id_процедуры);
            Денежные_срества.ТипУчетнойГруппы тип = ПолучитьТипУчетнойГруппы(args[2]);

            Денежные_срества.Движение операция = new Денежные_срества.Движение();
            операция.Куда = new Денежные_срества.Адрес();
            операция.Куда.учётная_группа = Денежные_срества.ТипУчетнойГруппы.Деньги_третьих_лиц;
            операция.Куда.контрагент = new Контрагент();
            операция.Куда.контрагент.Наименование = "ПАО Сбербанк";
            операция.сумма = "-"+args[3];
            операция.Откуда = new Денежные_срества.Адрес();
            операция.Откуда.учётная_группа = тип;
            операция.дата = материалы.time.Now;
            операция.обоснование = "Комиссия банка";

            материалы.регистратор.ЗарегистрироватьДенежнуюОперацию(proc.ID, операция);

            StringBuilder sb = new StringBuilder();
            sb.Append("Зарегистрировано движение").AppendLine();
            sb.Append("   ").Append(Convert.ToDecimal(операция.сумма).ToString("#,#.00")).AppendLine(" руб.");
            sb.Append("   откуда:         ").Append(операция.Откуда.ToReadableString()).AppendLine();
            sb.Append("   куда:           ").Append(операция.Куда.ToReadableString()).AppendLine();
            sb.AppendFormat("   обоснование:    {0}", операция.обоснование).AppendLine();
            Console.WriteLine(sb.ToString());
            return new List<object>();
        }

        private Денежные_срества.ТипУчетнойГруппы ПолучитьТипУчетнойГруппы(string тип)
        {
            switch (тип.ToLower())
            {
                case "средствадолжника":
                    return Денежные_срества.ТипУчетнойГруппы.средстваДолжника;
                case "залог":
                    return Денежные_срества.ТипУчетнойГруппы.залог;
                case "задаток":
                    return Денежные_срества.ТипУчетнойГруппы.задаток;
                case "средстваау":
                    return Денежные_срества.ТипУчетнойГруппы.средстваАУ;
                default:
                    throw new Exception();
            }
        }
    }

    [Command("creac")]
    internal class CreateMainAccount : ACommand
    {
        public override string Description
        {
            get { return "creac\t<id>\tЗарегистрировать учетную группу"; }
        }
        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            string id_процедуры = материалы.guids.unbeauty(args[1]);
            Процедура proc = материалы.дела.Процедура(id_процедуры);
            
            Денежные_срества.Учётная_группа учетнаяГруппа = JsonSerrilization.jsSerializer.Deserialize<Денежные_срества.Учётная_группа>(ReadArgTextFile(args[2]));
            учетнаяГруппа.Дата_начального_сальдо = материалы.time.Now;
            материалы.регистратор.ЗарегистрироватьНачальноеСальдо(proc.ID, учетнаяГруппа);

            Console.WriteLine(String.Format("Зарегистрирована учетная группа: {0}.\r\n     начальное сальдо: {1}",
                учетнаяГруппа.TypeToString(), учетнаяГруппа.Начальное_сальдо.ToString("#,#.00")));
            return new List<object>();
        }
    }

    [Command("finop")]
    internal class FinancialOperation : ACommand
    {
        public override string Description
        {
            get { return "finop\t<manproc_id>\t<path>/to/json\tВыполнить финансовые операции"; }
        }
        void Append_адрес(StringBuilder sb, Денежные_срества.Адрес адрес)
        {
            if (Денежные_срества.ТипУчетнойГруппы.Деньги_третьих_лиц!=адрес.учётная_группа)
            {
                sb.Append(адрес.TypeToString());
            }
            else
            {
                sb.Append(адрес.контрагент.Наименование);
            }
        }
        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            string id_процедуры = материалы.guids.unbeauty(args[1]);
            Процедура proc = материалы.дела.Процедура(id_процедуры);
            List<Денежные_срества.Движение> операции = JsonSerrilization.jsSerializer.Deserialize<List<Денежные_срества.Движение>> (ReadArgTextFile(args[2]));
            foreach(Денежные_срества.Движение операция in операции)
            {
                операция.дата = материалы.time.Now;
                Контрагент контрагент = null; 
                if (операция.Откуда.контрагент != null)
                {
                    контрагент = материалы.дела.КонтрагентПоИНН(операция.Откуда.контрагент.ИНН);
                    if(контрагент == null)
                    {
                        контрагент = new Контрагент();
                        контрагент.Id = Guid.NewGuid().ToString();
                        контрагент.ИНН = операция.Откуда.контрагент.ИНН;
                        контрагент.Наименование = операция.Откуда.контрагент.Наименование;
                        материалы.регистратор.ЗарегистрироватьКонтрагента(контрагент);
                    }
                    операция.Откуда.контрагент = контрагент;
                }
                else if(операция.Куда.контрагент != null)
                {
                    контрагент = материалы.дела.КонтрагентПоИНН(операция.Куда.контрагент.ИНН);
                    if (контрагент == null)
                    {
                        контрагент = new Контрагент();
                        контрагент.Id = материалы.guids.New.ToString();
                        контрагент.ИНН = операция.Куда.контрагент.ИНН;
                        контрагент.Наименование = операция.Куда.контрагент.Наименование;
                        материалы.регистратор.ЗарегистрироватьКонтрагента(контрагент);
                    }
                    операция.Куда.контрагент = контрагент;
                }

                материалы.регистратор.ЗарегистрироватьДенежнуюОперацию(proc.ID, операция);

                StringBuilder sb = new StringBuilder();
                sb.Append("Зарегистрировано движение").AppendLine();
                sb.Append("   ").Append(Convert.ToDecimal(операция.сумма).ToString("#,#.00")).AppendLine(" руб.");
                sb.Append("   откуда:         ").Append(операция.Откуда.ToReadableString()).AppendLine();
                sb.Append("   куда:           ").Append(операция.Куда.ToReadableString()).AppendLine();
                sb.AppendFormat("   обоснование:    {0}", операция.обоснование).AppendLine();
                Console.WriteLine(sb.ToString());
            }
            return new List<object>();
        }
    }

    [Command("outop")]
    internal class OutFinancialOperation : ACommand
    {
        public override string Description
        {
            get { return "outop\t<manproc_id>\t<type>\tОтобразить финансовую операцию"; }
        }
        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            string id_процедуры = материалы.guids.unbeauty(args[1]);
            Процедура proc = материалы.дела.Процедура(id_процедуры);
            Денежные_срества.ТипУчетнойГруппы типУчетнойГруппы = ПолучитьТипУчетнойГруппы(args[2]);
            Денежные_срества.Учётная_группа учетнаяГруппа = материалы.дела.ВсеУчетныеГруппы(proc.ID).
                SingleOrDefault(t => t.учётная_группа == типУчетнойГруппы);
            IEnumerable<Денежные_срества.Движение> движения = материалы.дела.ДвиженияДенежныхСредств(proc.ID).
                Where(t=> t.Куда.учётная_группа== типУчетнойГруппы || t.Откуда.учётная_группа==типУчетнойГруппы).ToList();
            материалы.operations.ТекущееСальдо(материалы.дела, proc.ID, учетнаяГруппа);
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("|дата      |{0}|{1}\r\n", TypeToString(типУчетнойГруппы).PadRight(13), ("обоснование").PadRight(13 * 5));
            sb.Append("--------------------------------------------------------------------------------------------").AppendLine();
            foreach (Денежные_срества.Движение движение in движения)
            {
                sb.AppendFormat("|{0}|{1}|{2}\r\n", движение.дата.ToShortDateString().PadLeft(10),
                    Convert.ToDecimal(движение.сумма).ToString("#,#.00").PadLeft(13), движение.обоснование.PadRight(13 * 5));
            }
            sb.Append("--------------------------------------------------------------------------------------------").AppendLine();
            sb.AppendFormat("Конечное сальдо: {0}\r\n", учетнаяГруппа.Текущее_сальдо.ToString("#,#.00"));
            Console.Write(sb.ToString());
            return new List<object>();
        }

        public string TypeToString(Денежные_срества.ТипУчетнойГруппы тип)
        {
            switch (тип)
            {
                case Денежные_срества.ТипУчетнойГруппы.средстваДолжника:
                    return "Средства дол.";
                case Денежные_срества.ТипУчетнойГруппы.средстваАУ:
                    return "Собственные средства АУ";
                case Денежные_срества.ТипУчетнойГруппы.залог:
                    return "Залог";
                case Денежные_срества.ТипУчетнойГруппы.задаток:
                    return "Задаток";
                default:
                    throw new Exception();
            }
        }

        private Денежные_срества.ТипУчетнойГруппы ПолучитьТипУчетнойГруппы(string тип)
        {
            switch (тип.ToLower())
            {
                case "средствадолжника":
                    return Денежные_срества.ТипУчетнойГруппы.средстваДолжника;
                case "залог":
                    return Денежные_срества.ТипУчетнойГруппы.залог;
                case "задаток":
                    return Денежные_срества.ТипУчетнойГруппы.задаток;
                case "средстваау":
                    return Денежные_срества.ТипУчетнойГруппы.средстваАУ;
                default:
                    throw new Exception();
            }
        }
    }

    [Command("showops")]
    internal class ShowAllOperations : ACommand
    {
        public override string Description
        {
            get { return "showops\t<manproc_id>\t\t\tотобразить все денежные операции"; }
        }
        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            string id_процедуры = материалы.guids.unbeauty(args[1]);
            Процедура proc = материалы.дела.Процедура(id_процедуры);
            Денежные_срества.Учётная_группа основнойСчет = материалы.дела.ВсеУчетныеГруппы(proc.ID).
                SingleOrDefault(t => t.учётная_группа == Денежные_срества.ТипУчетнойГруппы.средстваДолжника);
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("сумма на основном счету: {0}\r\n", основнойСчет.Начальное_сальдо.ToString("#,#.00"));
            sb.Append("--------------------------------------------------------------------------------------------------------------------------------------\r\n");
            sb.Append("|дата      |Средства     |Средства АУ  |залог        |задаток      |Обоснование                                                       \r\n");
            sb.Append("|          |должника     |             |             |             |                                                                  \r\n");
            sb.Append("--------------------------------------------------------------------------------------------------------------------------------------\r\n");
            foreach(Денежные_срества.Движение движение in материалы.дела.ДвиженияДенежныхСредств(proc.ID))
            {
                sb.AppendFormat("{0}\r\n", ЗадатьКоличествоПробелов(движение));
            }
            sb.Append("--------------------------------------------------------------------------------------------------------------------------------------\r\n");
            sb.AppendFormat("Средств на основном счету: {0}\r\n", основнойСчет.Текущее_сальдо.ToString("#,#.00"));
            Console.Write(sb.ToString());
            return new List<object>();
        }
        private string space = "|             ";
        private string ЗадатьКоличествоПробелов(Денежные_срества.Движение op, int n = 13)
        {
            string result = ЗадатьКоличествоПробеловЯчейки(op.дата.ToShortDateString(), 10);
            string сумма = ЗадатьКоличествоПробеловЯчейки(Convert.ToDecimal(op.сумма).ToString("#,#.00"), n);
            for (int i = 1; i < 5; i++)
            {
                if (i == (int)op.Куда.учётная_группа)
                {
                    result = result + ЗадатьКоличествоПробеловЯчейки(Convert.ToDecimal(op.сумма).ToString("#,#.00"), n);
                }
                else if(i == (int)op.Откуда.учётная_группа)
                {
                    result = result + ЗадатьКоличествоПробеловЯчейки(Convert.ToDecimal(op.сумма).ToString("#,#.00"), n);
                }
                else
                {
                    result = result + space;
                }
            }
            result = result + "|" + op.обоснование;
            return result;
        }

        private string ЗадатьКоличествоПробеловЯчейки(string line, int n)
        {
            int l = n - line.Length;
            string result = "|" + line.PadLeft(n);
            return result;
        }
    }


    [Command("tproc")]
    internal class OutProcedures : ACommand
    {
        public override string Description
        {
            get { return "tproc\t\t\t\t\tВывести краткую информацию по текущим процедурам"; }
        }

        public override List<object> Execute(Материалы материалы, string[] arguments, TextWriter os)
        {
            try
            {
                List<object> result = new List<object>();
                int i = 0;
                if (null != os)
                    os.WriteLine(" Процедуры:");

                List<Процедура> procedures = материалы.дела.ВсеПроцедуры();
                foreach (Процедура процедура in procedures)
                {
                    result.Add(процедура);
                    string outText = String.Format("{0,3}) {1} {2} {3} над {4} ", ++i, процедура.ID, процедура.Должник.ИНН, процедура.типПроцедуры, процедура.Должник.Наименование);
                    if (null != os)
                        os.WriteLine((outText.Count() >= 100) ? (outText.Substring(0, 100) + "...") : outText);
                }
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<object>();
            }
        }  
    }
	

    [Command("time")]
    internal class Time : ACommand
    {
        public override string Description{ get {return "time\t<t>\t\t\t\tУстановить или отобразить текущее время"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            DateTime dt = new DateTime();
            
            TestTimeProvider provider = материалы.time as TestTimeProvider;
            if(null != provider)
            {
                if (2 != args.Length || !DateTime.TryParse(args[1], out dt))
                {
                    os.Write(материалы.time.Now.ToString());
                }
                else
                {
                    provider.SetDateTime(dt);
                    os.WriteLine("Наступило {0}", dt);
                }
            }
            else
            {
                TestFileTimeProvider fileProvider = материалы.time as TestFileTimeProvider;
                if (2 != args.Length || !DateTime.TryParse(args[1], out dt))
                {
                    os.Write(материалы.time.Now.ToString());
                }
                else
                {
                    os.WriteLine("Наступило {0}", dt);
                }
            }
            
            return new List<object>();
            
        }
    }

    [Command("sproc")]
    internal class StartProcedure : ACommand
    {
        public override string Description { get { return "sproc\t<path/to/json>\t\t\tНачать процедуру"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            string proc_type = args[1];
            string dstart = args[2];
            string dfinish = args[3];
            string filename = args[4];
            Процедура процедура = new Процедура();
            процедура.Должник = JsonSerrilization.jsSerializer.Deserialize<Контрагент>(ReadArgTextFile(filename));
            процедура.типПроцедуры = (Процедура.ТипПроцедуры)Enum.Parse(typeof(Процедура.ТипПроцедуры), proc_type);
            процедура.Чего_процедуры();
            процедура.Дата_начала= DateTime.Parse(dstart);
            процедура.Дата_окончания = DateTime.Parse(dfinish);
            процедура.ID = материалы.регистратор.ЗарегистрироватьПроцедуру(процедура).ToString();

            os.WriteLine("Зарегистрирована процедура (id:{0}) {1}", материалы.guids.beauty(процедура.ID), процедура.тип.Кратко);
            os.WriteLine("  над {0} (ИНН:{1})", процедура.Должник.Наименование, процедура.Должник.ИНН);
            os.WriteLine("  с {0:d} по {1}", процедура.Дата_начала, процедура.Дата_окончания.ToString("g"));
            return new List<object>();
        }
    }

    [Command("rreq")]
    internal class RegisterRequest : ACommand
    {
        public override string Description { get { return "rreq\t<manproc_id>\t<path/to/json>\tЗарегистрировать исходящий запрос"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            string id_Процедуры = материалы.guids.unbeauty(args[1]);
            string filename = args[2];
            Процедура процедура = материалы.дела.Процедура(id_Процедуры);
            string json = ReadArgTextFile(filename);
            foreach (Исходящее исходящее in JsonSerrilization.jsSerializer.Deserialize<List<Исходящее>>(json))
            {
                исходящее.ДатаОтправки = материалы.time.Now;
                исходящее.ПроцедураID = id_Процедуры;

                материалы.регистратор.ЗарегистрироватьИсходящее(исходящее);
                if (исходящее.По(Исходящее.Шаблон.В_банк))
                {
                    os.Write("Отправлено {0:d} \"Запрос в {1}\" " , исходящее.ДатаОтправки, исходящее.Получатель);
                    os.WriteLine("(id:{0}) в ходе пр:{1}", материалы.guids.beauty(исходящее.ID), материалы.guids.beauty(процедура.ID));
                }
                else if (исходящее.Тип == Исходящее.Шаблон.О_введении.Тип)
                {
                    os.Write("Отправлено {0:d} \"{1}\" ", исходящее.ДатаОтправки, исходящее.По_шаблону().Наименование);
                    os.WriteLine("для \"{0}\" (id:{1}) в ходе пр:{2}", исходящее.Получатель, материалы.guids.beauty(исходящее.ID), материалы.guids.beauty(процедура.ID));
                }
                else
                {
                    os.Write("Отправлено {0:d} \"{1}\" ", исходящее.ДатаОтправки, исходящее.По_шаблону().Наименование);
                    os.WriteLine("(id:{0}) в ходе пр:{1}", материалы.guids.beauty(исходящее.ID), материалы.guids.beauty(процедура.ID));
                }
            }
            return new List<object>();
        }
    }

    public class Ответ_с_доп_информацией : Ответ
    {
        public Счет[] Счета;
    }


    [Command("rans")]
    internal class RegisterAnswer : ACommand
    {
        public override string Description { get { return "rans\t<manproc_id>\t<path/to/json>\tЗарегистрировать ответ на запрос"; } }
        
        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            Процедура proc = материалы.дела.Процедура(материалы.guids.unbeauty(args[1]));
            string filename = args[2];
            var json = ReadArgTextFile(filename);
            List<Ответ_с_доп_информацией> входящиеСообщения = JsonSerrilization.jsSerializer.Deserialize<List<Ответ_с_доп_информацией>>(json);
            foreach (Ответ_с_доп_информацией obj in входящиеСообщения)
            {
                Ответ входящее = new Ответ();
                входящее.ПроцедураID = proc.ID;
                входящее.ДатаПолучения = материалы.time.Now;
                входящее.Отправитель = obj.Отправитель;
                входящее.Тип = obj.Тип;
                Инвентаризация инв = материалы.дела.ВсеИнвентаризации(proc.ID).LastOrDefault();
                if(null != инв)
                    инв.ДатаОценки = материалы.time.Now;

                Исходящее исходящее = материалы.дела.ВсеИсходящие(входящее.ПроцедураID, null).FirstOrDefault(исх => исх.Получатель==входящее.Отправитель);
                if (null != исходящее && null == входящее.Тип)
                {
                    входящее.ЗапросID = исходящее.ID;
                    входящее.Тип = исходящее.Тип;
                }

                материалы.регистратор.ЗарегистрироватьВходящее(входящее);

                Исходящее.Шаблон шаблон = Исходящее.Шаблон.Типа(входящее.Тип);
                os.Write("Получен");
                if (null != исходящее)
                    os.Write(" ответ на");
                os.Write(" \"{0}\" (id:{1}) ", шаблон.Наименование, материалы.guids.beauty(входящее.ID));
                if (шаблон==Исходящее.Шаблон.В_банк)
                    os.Write("от \"{0}\" ", входящее.Отправитель);
                os.WriteLine("{0:d} в ходе пр:{1}", входящее.ДатаПолучения, материалы.guids.beauty(proc.ID));

                if (шаблон == Исходящее.Шаблон.В_ИФНС)
                    Зарегистрировать_содержимое_ответа_ИФНС(материалы, входящее, os, obj);
                if (шаблон == Исходящее.Шаблон.В_банк)
                    Зарегистрировать_содержимое_ответа_Банка(материалы, входящее, os, obj);
            }
            
            return new List<object>();
        }

        void Зарегистрировать_содержимое_ответа_ИФНС(Материалы материалы, Ответ ответ, TextWriter os, Ответ_с_доп_информацией obj)
        {
            os.WriteLine("  Счета:");
            if (obj.Счета != null)
            {
                foreach (Счет a in obj.Счета)
                {
                    Счет account = new Счет();
                    account = a;
                    account.ПроцедураID = ответ.ПроцедураID;
                    материалы.регистратор.ЗарегистрироватьСчёт(account);
                    os.WriteLine("  - {0}: {1}", account.Банк, account.Счёт);
                }
            }
        }

        void Зарегистрировать_содержимое_ответа_Банка(Материалы материалы, Ответ ответ, TextWriter os, Ответ_с_доп_информацией obj)
        {
            os.WriteLine("  Состояние счетов:");
            if (obj.Счета != null)
            {
                foreach (Счет a in obj.Счета)
                {
                    Счет account = new Счет();
                    account = a;
                    материалы.регистратор.ОбновитьСчёт(account, account.Остаток);
                    os.WriteLine("  - {0}: {1} руб.", account.Счёт, account.Остаток);
                }
            }
        }
    }

    [Command("chefrsb")]
    internal class CheckEfrsbPublication : ACommand
    {
        public override string Description { get { return "efrsb\t<manproc_id>\t<id_task>\t\tПроверить публикацию сообщения на ЕФРСБ"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            try
            {
                string id_процедуры = материалы.guids.unbeauty(args[1]);
                string формулировка = args[2].Replace("_", " ");
                Процедура процедура = материалы.дела.Процедура(id_процедуры);
				//Загрузка новых сообщений с сервера
				Client client = new Client();
				List<PlainMessage> newmsg = client.GetNewMessages(процедура.Должник.ИНН, "", "", материалы.дела.ПолучитьПоследнююРевизииюЕфрсбСообщения(процедура.Должник.Id));
				foreach (PlainMessage pm in newmsg)
				{
					Efrsb_message_record message_record = new Efrsb_message_record();
					MessageData md = MessageData.ReadFromXmlText(pm.Body);
					message_record.Revision = pm.Revision;
					message_record.Body = pm.Body;
					message_record.PublishDate = md.PublishDate;
					message_record.MessageType = md.MessageInfo.MessageType;
					message_record.MessageNumber = md.Number;
					материалы.регистратор.ДобавитьНовоеОбъявлениеЕФРСБ(процедура.Должник.Id, message_record);
				}

				IEnumerable<Efrsb_message_record> публикации = материалы.дела.ВсеЕфрсбСообщения(процедура.Должник.Id);
                List<object> pubs = new List<object>();
                foreach(Efrsb_message_record пуб in публикации)
                {
                    if (формулировка.Equals(Efrsb.ПолучитьПолнуюФормулировкуОбъявления(пуб.MessageType, процедура)))
                    {
                        pubs.Add(пуб);
                    }
                }
                return pubs;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<object>();
            }
        }
    }

    [Command("efrsb")]
    internal class RegisterEFRSBPublication : ACommand
    {
        public override string Description { get { return "efrsb\t<manproc_id>\t<messageType>\t<messageNumber>\tОпубликовать сообщение в ЕФРСБ"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            try
            {
                Efrsb_message_record message = new Efrsb_message_record();
                string id_процедуры = материалы.guids.unbeauty(args[1]);
                Процедура процедура = материалы.дела.Процедура(id_процедуры);
                message.PublishDate = материалы.time.Now;
                message.DebtorId = процедура.Должник.Id;
                
                message.Body = args.Count() > 3? ReadArgTextFile(args[3]):"";
                message.MessageType = Efrsb.ПолучитьНазваниеОбъявления(args[2]);
                message.Revision = материалы.дела.ПолучитьПоследнююРевизииюЕфрсбСообщения(процедура.Должник.Id);
                message.MessageNumber = Efrsb.MessageNumber.New;

                материалы.регистратор.ДобавитьНовоеОбъявлениеЕФРСБ(процедура.Должник.Id, message);
                string ss = message.MessageType;
                os.WriteLine("Опубликовано {0:d} сообщение в ЕФРСБ {1} в ходе пр:{2}",
                     message.PublishDate, Efrsb.ПолучитьФормулировкуОбъявления(message.MessageType), материалы.guids.beauty(процедура.ID));

                return new List<object>();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<object>();
            }
        }
    }

    [Command("aefrsb")]
    internal class GetAllEfrsb : ACommand
    {
        public override string Description { get { return "aefrsb\t<manproc_id>\t\t\tПолучить все сообщения ЕФРСБ"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            try
            {
                string id_процедуры = материалы.guids.unbeauty(args[1]);
                Процедура процедура = материалы.дела.Процедура(id_процедуры);
                IEnumerable<Efrsb_message_record> message = материалы.дела.ВсеЕфрсбСообщения(процедура.Должник.Id);
                List<object> objectMessage = new List<object>();
                foreach(Efrsb_message_record msg in message)
                {
                    objectMessage.Add(msg);
                }
                return objectMessage;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<object>();
            }
        }
    }

    [Command("rpub")]
    internal class RegisterPublication : ACommand
    {
        public override string Description { get { return "rpub\t<manproc_id>\t<path/to/json>\tЗарегистрировать публикацию в СМИ"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            Публикация publication = new Публикация();
            string id_процедуры = материалы.guids.unbeauty(args[1]);
            publication.ПроцедураID = материалы.guids.unbeauty(id_процедуры);
            publication.Дата = материалы.time.Now;
            publication.СМИ = "Коммерсантъ";
            материалы.регистратор.ЗарегистрироватьПубликацию(publication);
            os.Write("Зарегистрирована публикация в СМИ \"{0}\" ", publication.СМИ);
            os.WriteLine("от {0:d} в ходе пр:{1}", publication.Дата, материалы.guids.beauty(publication.ПроцедураID));

            return new List<object>();
        }
    }

    public class Требование_кредитора
    {
        public Кредитор кредитор;
        public decimal сумма;
    }

    [Command("rtk")]
    internal class RegisterCreditorsClaim : ACommand
    {
        public override string Description { get { return "rtk\t<manproc_id>\t<path/to/json>\tЗарегистрировать требование кредиторов"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            Процедура процедура= материалы.дела.Процедура(материалы.guids.unbeauty(args[1]));
            string filename = args[2];
            var json = ReadArgTextFile(filename);
            List<Требование_кредитора> требование_кредитора = JsonSerrilization.jsSerializer.Deserialize<List<Требование_кредитора>>(json);
            foreach (Требование_кредитора obj in требование_кредитора)
            {
                Кредитор creditor = new Кредитор();
                creditor = obj.кредитор;
                creditor.процедураId = процедура.ID;
                материалы.регистратор.ЗарегистрироватьКредитора(creditor);

                Требование claim = new Требование();
                claim.Сумма = obj.сумма;
                claim.ПроцедураID = процедура.ID;
                claim.ДатаПолучения = claim.ДатаВключения = материалы.time.Now;
                claim.КредиторID = creditor.ID;
                claim.ID = материалы.регистратор.ЗарегистрироватьТребованиеКредитора(claim);

                os.Write("Получено требование (id:{0}) от \"{1}\" ", материалы.guids.beauty(claim.ID), creditor.Наименование);
                os.Write("на сумму {0} руб., ", claim.Сумма.ToString("N2"));
                os.WriteLine("{0:d} в ходе пр:{1}", claim.ДатаПолучения, материалы.guids.beauty(процедура.ID));
            }
            return new List<object>();
        }
    }

    [Command("itk")]
    internal class IncludeCreditorsClaim : ACommand
    {
        public override string Description { get { return "itk\t<manproc_id>\t<path/to/json>\tВключить требование кредитора в реестр"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            Процедура процедура = материалы.дела.Процедура(материалы.guids.unbeauty(args[1]));
            var json = ReadArgTextFile(args[2]);
            List<Требование> требования = JsonSerrilization.jsSerializer.Deserialize<List<Требование>>(json);
            foreach (Требование inc in требования)
            {
                Требование требование = материалы.дела.Требование(материалы.guids.unbeauty(inc.ID));
                требование.Включено = inc.Включено;
                требование.ДатаВключения = inc.ДатаВключения;
                материалы.регистратор.ОбновитьТребования(материалы.guids.unbeauty(требование.КредиторID), inc.ДатаВключения);
                if (inc.Сумма != 0)
                {
                    требование.Сумма = inc.Сумма;
                    материалы.регистратор.ОбновитьТребования(материалы.guids.unbeauty(требование.КредиторID), inc.Сумма);
                }
                материалы.регистратор.IncludeClaim(требование.ID);
                Кредитор кредитор = материалы.дела.Кредитор(требование.КредиторID);
                os.Write("Требование (id:{0}) от {1} ", материалы.guids.beauty(требование.ID), кредитор.Наименование);
                os.Write("на сумму {0} руб. включено в реестр {1:d} ", требование.Сумма, требование.ДатаВключения);
                os.WriteLine("в ходе пр:{0}", материалы.guids.beauty(процедура.ID));
            }

            return new List<object>();
        }
    }

    [Command("rsk")]
    internal class AppointCreditorsMeeting : ACommand
    {
        public override string Description { get { return "rsk\t<manproc_id>\t<path/to/json>\tНазначить собрание"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            List<string> result = new List<string>();
            Процедура процедура = материалы.дела.Процедура(материалы.guids.unbeauty(args[1]));
            Собрание собрание = new Собрание();
            собрание.ПроцедураID = процедура.ID;
            собрание.Дата = DateTime.Parse(args[2]);
            собрание.Вопросы = JsonSerrilization.jsSerializer.Deserialize<List<Вопрос>>(ReadArgTextFile(args[3]));
            собрание.ДатаНазначения = материалы.time.Now;
            материалы.регистратор.ЗарегистрироватьСобрание(собрание);
            os.WriteLine("Назначено СК на {0} (id:{1}) в ходе пр:{2}", собрание.Дата, материалы.guids.beauty(собрание.ID), материалы.guids.beauty(процедура.ID));
            return new List<object>();
        }
    }

    [Command("uinv")]
    internal class UpdateInventariztion : ACommand
    {
        public override string Description { get { return "uinv\t<manproc_id>\t<path/to/json>\tПровести инвентаризацию"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            Процедура процедура = материалы.дела.Процедура(материалы.guids.unbeauty(args[1]));
            Инвентаризация инвентаризация = new Инвентаризация();
            инвентаризация.ДатаОценки = материалы.time.Now;
            инвентаризация.ПроцедураID = процедура.ID;
            материалы.регистратор.ЗарегистрироватьОценкуСчетов(инвентаризация);
            os.WriteLine("Проведена оценка {0} в ходе пр:{1}", инвентаризация.ДатаОценки, материалы.guids.beauty(процедура.ID));
            return new List<object>();
        }
    }

    [Command("rnsk")]
    internal class RegisterMeetingNotification : ACommand
    {
        public override string Description { get { return "rnsk\t<manproc_id>\t<path/to/json>\tЗарегистрировать исходящее уведомление о собрании"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            Процедура процедура = материалы.дела.Процедура(материалы.guids.unbeauty(args[1]));
            var json = ReadArgTextFile(args[2]);
            foreach (Исходящее message in JsonSerrilization.jsSerializer.Deserialize<List<Исходящее>>(json))
            {
                message.ПроцедураID = процедура.ID;
                message.Тип = Исходящее.Шаблон.Уведомление_о_СК.Тип;
                message.ДатаОтправки = материалы.time.Now;
                материалы.регистратор.ЗарегистрироватьИсходящее(message);

                os.Write("Отправлено {0} \"{1}\" ", message.ДатаОтправки.ToString("d"), message.По_шаблону().Наименование);
                os.WriteLine(" для {0} (id:{1}), в ходе пр:{2}", message.Получатель, материалы.guids.beauty(message.ID), материалы.guids.beauty(процедура.ID));
            }
            return new List<object>();
        }
    }

    [Command("rrsk")]
    internal class RegisterMeetingResults : ACommand
    {
        public override string Description { get { return "rrsk\t<manproc_id>\t<path/to/json>\tЗарегистрировать результаты собрания"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            Процедура процедура = материалы.дела.Процедура(материалы.guids.unbeauty(args[1]));
            List<Вопрос> вопросы = JsonSerrilization.jsSerializer.Deserialize<List<Вопрос>>(ReadArgTextFile(args[2]));
            Собрание собрание= материалы.дела.ПоследнееСобрание(процедура.ID);

            os.Write("Результаты собрания кредиторов в рамках {0} над {1}:\r\n", процедура.Чего_процедуры(), процедура.Должник.Наименование);

            int i = 1;
            foreach (Вопрос вопрос in вопросы)
                os.WriteLine("{0,3}) {1}: {2}", i++, вопрос.Формулировка, вопрос.Голоса.First().Решение);

            материалы.регистратор.ПровестиСобрание(собрание);
            return new List<object>();
        }
    }

    [Command("rfa")]
    internal class RegisterFinancialAnalysisResults : ACommand
    {
        public override string Description { get { return "rfa\t<manproc_id>\t\t\tЗарегистрировать результаты финанализа"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            Анализ анализ = new Анализ();
            анализ.Дата = материалы.time.Now;
            анализ.ПроцедураID = материалы.guids.unbeauty(args[1]);
            материалы.регистратор.ЗарегистрироватьФинанализ(анализ);
            os.WriteLine("Зарегистрированы результаты финанализа в ходе пр:{0}", материалы.guids.beauty(анализ.ПроцедураID));
            return new List<object>();
        }
    }

    [Command("rinv")]
    internal class RegisterInventory : ACommand
    {
        public override string Description { get { return "rinv\t<manproc_id>\t<path/to/json>\tЗарегистрировать результаты инвентаризации"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            Процедура процедура = материалы.дела.Процедура(материалы.guids.unbeauty(args[1]));
            Инвентаризация инвентаризация = new Инвентаризация();
            инвентаризация.Дата = материалы.time.Now;
            инвентаризация.ПроцедураID = процедура.ID;
            материалы.регистратор.ЗарегистрироватьИнвентаризацию(инвентаризация);

            os.WriteLine("Зарегистрированы результаты инвентаризации в ходе пр:{0}", 
                материалы.guids.beauty(инвентаризация.ПроцедураID));
            return new List<object>();
        } 
    }

    [Command("osale")]
    internal class OpenSale : ACommand
    {
        public override string Description { get { return "osale\t<manproc_id>\t<path/to/json>\tЗарегистрировать открытие торгов"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            Процедура процедура = материалы.дела.Процедура(материалы.guids.unbeauty(args[1]));
            Торги торги = new Торги();
            торги.ПроцедураID = процедура.ID;
            торги.ДатаОткрытия = DateTime.Parse(args[2]);
            материалы.регистратор.ЗарегистрироватьТорги(торги);
            os.WriteLine("Зарегистрировано открытие торгов в ходе пр:{0}", материалы.guids.beauty(торги.ПроцедураID));
            return new List<object>();
        }
    }

    [Command("csale")]
    internal class CloseSale : ACommand
    {
        public override string Description { get { return "csale\t<manproc_id>\t<path/to/json>\tЗарегистрировать завершение торгов"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            Процедура процедура = материалы.дела.Процедура(материалы.guids.unbeauty(args[1]));
            Торги торги = new Торги();
            торги.ПроцедураID = процедура.ID;
            торги.ID = args[2];
            торги.ДатаЗавершения = DateTime.Parse(args[3]);
            материалы.регистратор.ЗакрытьТорги(торги);
            os.WriteLine("Зарегистрировано завершение торгов в ходе пр:{0}", материалы.guids.beauty(процедура.ID));
            return new List<object>();
        }
    }

    [Command("atasks")]
    internal class AllTasks : ACommand
    {
        public override string Description { get { return "atasks\t\t\t\t\tВывести задачи по всем процедурам"; } }

        public override List<object> Execute(Материалы материалы, string[] arguments, TextWriter os)
        {
            List<object> result = new List<object>();
            List<IЗадача> всеЗадачи = new List<IЗадача>();
            всеЗадачи = материалы.постановщик.Запланировать_фронт_задач(null);
            foreach (IЗадача задача in всеЗадачи)
            {
                result.Add(задача);
            }
            return result;
        }
    }
    [Command("tasks")]
    internal class ListTasks : ACommand
    {
        public override string Description { get { return "tasks\t<manproc_id>\t\t\tОтобразить текущие задачи"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            try
            {
                string id_процедуры = материалы.guids.unbeauty(args[1]);
                Процедура процедура = материалы.дела.Процедура(id_процедуры);
                List<IЗадача> задачи = материалы.постановщик.Запланировать_фронт_задач(процедура);
                if (null == задачи || 0 == задачи.Count)
                {
                    os.WriteLine("Задачи отсутствуют");
                    return new List<object>();
                }
                else
                {
                    List<object> result = new List<object>();
                    os.WriteLine("Текущие задачи в рамках {0} над {1}:", процедура.Чего_процедуры(), процедура.Должник.Наименование);
                    int i = 1;
                    foreach (IЗадача задача in задачи)
                    {
                        result.Add(задача);
                        if (task.Статус.Открыта == задача.Статус && материалы.дела.Задача_решена(задача) == null)
                            os.WriteLine("{0,3}) {1} до {2:d}", i++, задача.Формулировка, задача.К_сроку);
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<object>();
            }
            
        }
    }

    [Command("treturn")]
    internal class ReturnTask : ACommand
    {
        public override string Description
        {
            get { return "treturn\t\t\t\t\tВосстановить предыдущую задачу"; }
        }

        public override List<object> Execute(Материалы материалы, string[] arguments, TextWriter os)
        {
            материалы.регистратор.Восстановить_задачу();
            return new List<object>();
        }
    }

    [Command("rtask")]
    internal class ResolveTask : ACommand
    {
        public override string Description { get { return "" +
                    "rtask\t<manproc_id>\t\t\tОтметить задачу как решенную"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            try
            {
                string id_процедуры = материалы.guids.unbeauty(args[1]);
                Процедура процедура = материалы.дела.Процедура(id_процедуры);
                string формулировка_задачи = args[2].Replace("_", " ");
                string TaskResult = "2";
                if (args.Count() > 3)
                    TaskResult = args[3];

                List<IЗадача> задачи = материалы.постановщик.Запланировать_фронт_задач(процедура);
                List<IЗадача> sckipTasks = материалы.дела.ВсеПропущенныеЗадачи(процедура.ID);
                int numberTask;
                if (Int32.TryParse(формулировка_задачи, out numberTask))
                {
                    int index = 1;
                    foreach (IЗадача задача in задачи)
                    {
                        if (task.Статус.Открыта == задача.Статус && материалы.дела.Задача_решена(задача) == null &&
                            sckipTasks.SingleOrDefault(t => t.Формулировка == задача.Формулировка) == null)
                        {
                            if (numberTask == index)
                            {
                                материалы.регистратор.Зарегистрировать_решение(задача, TaskResult);
                                os.WriteLine("Задача \"{0}\" помечена как решенная", задача.Формулировка);
                                break;
                            }
                            index++;
                        }
                    }
                }
                else
                {
                    IЗадача задача = задачи.FirstOrDefault(_задача => _задача.Формулировка.Equals(формулировка_задачи));
                    if (null != задача)
                    { 
                        материалы.регистратор.Зарегистрировать_решение(задача, TaskResult);
						os.WriteLine("Задача \"{0}\" помечена как решенная", задача.Формулировка);
					}
                }
                
                return new List<object>();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<object>();
            }

        }
    }

    [Command("t")]
    internal class Timeline : ACommand
    {
        public override string Description { get { return "t\t<id>\t\t\t\tПоказать линию времени для процедуры <id>"; } }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            os.Write(материалы.постановщик.Build_timeline(материалы.guids.unbeauty(args[1])));
            return new List<object>();
        }
    }

    [Command("show")]
    internal class ShowAll : ACommand
    {
        public override string Description { get { return "show\t<id>\t\t\t\tПоказать состояние дела для процедуры <id>"; } }

        private string PrintProcedure(AMA.Robot.Процедура proc)
        {
            return String.Format("Процедура {0} (id:{1}) над \"{2}\" (ИНН:{3}) с {4:d} по {5:g}",
                proc.Чего_процедуры(), proc.ID, proc.Должник.Наименование, proc.Должник.ИНН, proc.Дата_начала, proc.Дата_окончания);
        }

        private string PrintOutcoming(AMA.Robot.Исходящее msg)
        {
            return String.Format("Исходящее (id:{0}) \"{1}\" для \"{2}\", отправлено {3:d}",
                msg.ID, msg.По_шаблону().Наименование, msg.Получатель, msg.ДатаОтправки);
        }

        private string PrintIncoming(AMA.Robot.Ответ msg, Исходящее imsg = null)
        {
            return String.Format("Входящее (id:{0}) \"{1}\" от \"{2}\", получено {3:d}",
                msg.ID, imsg.По_шаблону().Наименование, msg.Отправитель, msg.ДатаПолучения);
        }

        private string PrintPublication(Публикация msg)
        {
            return String.Format("Публикация (id:{0}) в \"{1}\" от {2:d}",
                msg.ID, msg.СМИ, msg.Дата);
        }

        private string PrintEFRSB(IEfrsb_message_record msg)
        {
            return String.Format("Сообщение на ЕФРСБ (id:{0}) от {1:d}",
                msg.Revision, msg.PublishDate);
        }

        private string PrintCreditorsClaim(Требование claim, Кредитор creditor)
        {
            return String.Format("Требование (id:{0}) кредитора \"{1}\" (ИНН:{2}) на сумму {3} от {4:d}, {5}",
                claim.ID, creditor.Наименование, creditor.ИНН, claim.Сумма, claim.ДатаПолучения,
                claim.Включено ? String.Format("включено в РТК {0:d}", claim.ДатаВключения) : "не включено в РТК");
        }

        private string PrintMeeting(Собрание meeting)
        {
            return String.Format("Собрание (id:{0}), назначено {1:d}, проведено {2:g}",
                meeting.ID, meeting.ДатаНазначения, meeting.Дата);
        }

        public override List<object> Execute(Материалы материалы, string[] args, TextWriter os)
        {
            var proc = материалы.дела.Процедура(материалы.guids.unbeauty(args[1]));
            var outcoming = материалы.дела.ВсеИсходящие(proc.ID);
            var incoming = материалы.дела.ВсеВходящие(proc.ID);
            var pubs = материалы.дела.ВсеПубликации(proc.ID);
            var efrsbs = материалы.дела.ВсеЕфрсбСообщения(proc.Должник.Id);
            var claims = материалы.дела.ВсеТребования(proc.ID);
            var meetings = материалы.дела.ВсеСобрания(proc.ID);

            os.WriteLine("Состояние дела:");
            os.WriteLine(PrintProcedure(proc));
            foreach (var msg in outcoming) 
                os.WriteLine(PrintOutcoming(msg));
            foreach (var msg in incoming) 
                os.WriteLine(PrintIncoming(msg,!String.IsNullOrEmpty(msg.ЗапросID) ? материалы.дела.Исходящее(msg.ЗапросID) : null));
            foreach (var msg in pubs) 
                os.WriteLine(PrintPublication(msg));
            foreach (var msg in efrsbs) 
                os.WriteLine(PrintEFRSB(msg));
            foreach (var claim in claims) 
                os.WriteLine(PrintCreditorsClaim(claim, материалы.дела.Кредитор(claim.КредиторID)));
            foreach (var meeting in meetings) 
                os.WriteLine(PrintMeeting(meeting));

            return new List<object>();
        }
    }

    public static class Factory
    {
        private static readonly Dictionary<string, ICommand> commands = null;

        static Factory()
        {
            commands = new Dictionary<string, ICommand>();
            var types = typeof(ICommand).Assembly.GetTypes()
                .Where(t => typeof(ICommand).IsAssignableFrom(t))
                .ToList();
            foreach (var type in types)
            {
                foreach (var attr in type.GetCustomAttributes(typeof(CommandAttribute), false))
                {
                    CommandAttribute ca= attr as CommandAttribute;
                    if (null != ca)
                    {
                        commands[ca.Abbreviation] = Activator.CreateInstance(type) as ICommand;
                    }
                }
            }
        }

        public static ICommand Get(string abbreviation)
        {
            ICommand res = null;
            commands.TryGetValue(abbreviation, out res);
            return res;
        }

        public static IEnumerable<string> Descriptions
        {
            get
            {
                foreach (var command in commands)
                    yield return command.Value.Description;
            }
        }
    }
}
