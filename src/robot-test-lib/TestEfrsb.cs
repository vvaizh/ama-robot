﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMA.Robot
{
    public class Efrsb
    {
        public static string ПолучитьНазваниеОбъявления(string s)
        {
            switch (s)
            {
                case "ВведениеПроцедуры": return "BeginExecutoryProcess";
                case "СобраниеКредиторов": return "Meeting";
                case "РезультатСК": return "MeetingResult";
                case "ПолучениеТК": return "ReceivingCreditorDemand";
                case "Инвентаризация": return "PropertyInventoryResult";
                case "ДоговорКуплиПродажи": return "SaleContractResult";
                case "Оценка": return "PropertyEvaluationReport";
                case "ПризнакиБанкротства": return "DeliberateBankruptcy";
                case "ПланРеструктуризации": return "ViewDraftRestructuringPlan";
                default:
                    throw new Exception();
            }
        }

        public static string ПолучитьПолнуюФормулировкуОбъявления(string s, Процедура процедура)
        {
            switch (s)
            {
                case "BeginExecutoryProcess": return String.Format("Разместить на ЕФРСБ \"Сообщение о введении {0}\"", процедура.Чего_процедуры());
                case "Meeting": return "Разместить сообщение об СК на ЕФРСБ";
                case "MeetingResult": return "Опубликовать результаты СК на ЕФРСБ";
                case "ReceivingCreditorDemand": return "Разместить на ЕФРСБ Уведомление о получении ТК";
                case "PropertyInventoryResult": return "Опубликовать на ЕФРСБ отчёт об инвентаризации";
                case "SaleContractResult": return "Разместить на ЕФРСБ сведения о заключении договора купли-продажи"; ;
                case "PropertyEvaluationReport": return "Опубликовать отчёт об оценке имущества на ЕФРСБ";
                case "DeliberateBankruptcy": return "Опубликовать на ЕФРСБ сообщение о наличии или об отсутствии признаков фиктивного банкротства";
                case "ViewDraftRestructuringPlan": return "Опубликовать на ЕФРСБ сведения о порядке и месте ознакомления с проектом плана реструктуризации";
                default:
                    return "";
            }
        }

        public static string ПолучитьФормулировкуОбъявления(string s)
        {
            switch(s)
            {
                case "BeginExecutoryProcess": return "о введении процедуры";
                case "Meeting": return "о собрании кредиторов";
                case "MeetingResult": return "о результате проведения СК";
                case "ReceivingCreditorDemand": return "о получении требований кредитора";
                case "PropertyInventoryResult": return "об инвентаризации";
                case "SaleContractResult": return "о заключении договора купли-продажи";
                case "PropertyEvaluationReport": return "об оценке имущества";
                case "DeliberateBankruptcy": return "о наличии или об отсутствии признаков фиктивного банкротства";
                case "ViewDraftRestructuringPlan": return "о порядке и месте ознакомления с проектом плана реструктуризации";
                default:
                    throw new Exception();
            }
        }

        public class MessageNumber
        {
            private static string[] numbers = new string[] 
            {
                "0000001",
                "0000002",
                "0000003",
                "0000004",
                "0000005",
                "0000006",
                "0000007",
                "0000008",
                "0000009"
            };

            private static int count = 0;

            public static string New
            {
                get
                {
                    return numbers[count++];
                }
            }
        }
    }


}
